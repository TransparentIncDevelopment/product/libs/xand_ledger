# XAND Ledger

This repo contains the code defining the processing and validation logic of our chain and the
transactions that can be performed on it.

It is intentionally light on dependencies, and makes an effort to be reasonably agnostic of the
underlying chain implementation (IE: Substrate). The crate should never depend on substrate crates
directly, with the exception of the crate required for SCALE encoding, which is technically not
a part of substrate, but is used by it.

## Contributing

### Add to changelog

When making changes to this repo, please make updates to the [CHANGELOG](CHANGELOG.md).

### Tagging
This repo uses tags to identify release versions at a repository level consistent with the version specified in [Cargo.toml](./Cargo.toml).

After the release's successful completed merge to `master`, make the corresponding tag and push it.

For more info on tagging, see [Git docs](https://git-scm.com/book/en/v2/Git-Basics-Tagging).

> Version 0.1.0: tag "0.1.0"

> Note: Git tags are global across branches

Fetch tags from remote:
```bash
git fetch --tags
```

List all tags:
```bash
git tag -l --sort=-creatordate
```

Add and push a tag matching the bumped version in [Cargo.toml](./Cargo.toml):
```bash
git tag -a "0.8.1" -m "Some info about this release"
git push origin --tags
```
>This pushes any un-pushed tags to remote - to push a specific tag, `git push origin <tagname>`

## Usage
Please check out the rust docs! You can view them like so:

`cargo doc --open`

## Structure
The `model` module contains struct definitions representing the transactions and their
various constituent parts as well as traits defining how chain data is accessed.

The logic for building and validating transactions can be found in `transactions`

The core crypto logic is in `zkplmt` (Zero knowledge proof of linear member tuple).

Our actual business logic that defines how a transaction is processed is located in
`financial_impl` -- this is likely to be broken up / changed soon.

## Protected legal text

Text in the `src/legal_text/official_text/` directory is protected from changes without the MR approval of at least one member of the legal team.

This is done using the [Codeowners feature](https://docs.gitlab.com/ee/user/project/code_owners.html) of Gitlab.

See permissions in the [CODEOWNERS](./CODEOWNERS) file.

### Preventing unauthorized changes
To prevent this text from being accidentally edited without the legal team's approval, the following safeguard is in place:

* The folder containing the text has legal team code-owners indicated in the `CODEOWNERS` file.

>Note: In order for this safeguard to work as intended, this repo's Gitlab settings must: 1) indicate that the protected branch cannot be updated without at least one code-owner's approval and, 2) have the `CODEOWNERS` setting turned on for MRs/protected branches.

### Making updates to legal text

When creating an MR with changes to protected legal text, it must be done via MR to merge to the protected branch.

1. **Notify legal:** Add legal team code-owner(s) as "Reviewers" in the MR (or @ mention them) so that they will be email-notified of the MR. 
2. **Get legal approval:** At least one of these code-owners will need to approve the MR in order for it to be merged to the protected branch.

>Note: Code-owners are not automatically notified of an MR requiring their review.
> Gitlab is aware of the [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/12855) but has de-prioritized it.

### Adding protected legal text

Add a new subdirectory to `src/legal_text/official_text` and place the legal text files there.

### Adding code-owners

To add code-owner reviewers:

1. Add the individual's Gitlab user handle to the appropriate definition in the CODEOWNERS file.
2. Ensure that they have been added to our Transparent Inc. Gitlab project with at least "Developer" privileges.

## References:

[TxO Flow diagram](https://miro.com/app/board/uXjVOHoKSlI=/)