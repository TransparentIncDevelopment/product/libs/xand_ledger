use criterion::{black_box, criterion_group, Criterion};
use rand::rngs::OsRng;
use xand_ledger::{
    create_banned_members_list, crypto::DefaultCryptoImpl, verify_send_claims_transaction,
    TestSendClaimsBuilder,
};

criterion_group!(
    send_claims,
    send_claims_construction,
    send_claims_construction_with_banned,
    send_claims_verification,
    send_claims_verification_with_banned
);

fn send_claims_construction(c: &mut Criterion) {
    c.bench_function("send claims construction", |b| {
        b.iter(|| TestSendClaimsBuilder::default().build(black_box(&mut OsRng::default())))
    });
}

fn send_claims_construction_with_banned(c: &mut Criterion) {
    let banned_count = 5;
    let description = format!("send claims construction with {} banned", banned_count);
    let rng = OsRng::default();
    let banned_members = create_banned_members_list(rng, banned_count);
    c.bench_function(&description, |b| {
        b.iter(|| {
            TestSendClaimsBuilder::default()
                .with_banned_members(banned_members.clone())
                .build(black_box(OsRng::default()))
        })
    });
}

fn send_claims_verification(c: &mut Criterion) {
    let send_claims_transaction = TestSendClaimsBuilder::default().build(OsRng::default());
    c.bench_function("send claims verification", |b| {
        b.iter(|| {
            verify_send_claims_transaction::<DefaultCryptoImpl>(
                black_box(&send_claims_transaction),
                Vec::new(),
            )
        })
    });
}

fn send_claims_verification_with_banned(c: &mut Criterion) {
    let banned_count = 5;
    let description = format!("send claims verification with {} banned", banned_count);
    let rng = OsRng::default();
    let banned_members = create_banned_members_list(rng, banned_count);
    let send_claims_transaction = TestSendClaimsBuilder::default()
        .with_banned_members(banned_members.clone())
        .build(OsRng::default());
    c.bench_function(&description, |b| {
        b.iter(|| {
            verify_send_claims_transaction::<DefaultCryptoImpl>(
                black_box(&send_claims_transaction),
                banned_members.clone(),
            )
        })
    });
}
