use criterion::{black_box, criterion_group, Criterion};
use rand::rngs::OsRng;
use xand_ledger::{
    create_banned_members_list, crypto::DefaultCryptoImpl, verify_redeem_request_transaction,
    TestRedeemRequestBuilder,
};

criterion_group!(
    redeem,
    redeem_request_construction,
    redeem_request_construction_with_banned,
    redeem_request_verification,
    redeem_request_verification_with_banned
);

fn redeem_request_construction(c: &mut Criterion) {
    c.bench_function("redeem request construction", |b| {
        b.iter(|| TestRedeemRequestBuilder::default().build(black_box(&mut OsRng::default())))
    });
}

fn redeem_request_construction_with_banned(c: &mut Criterion) {
    let banned_count = 5;
    let description = format!("redeem request construction with {} banned", banned_count);
    let rng = OsRng::default();
    let banned_members = create_banned_members_list(rng, banned_count);
    c.bench_function(&description, |b| {
        b.iter(|| {
            TestRedeemRequestBuilder::default()
                .with_banned_members(banned_members.clone())
                .build(black_box(&mut OsRng::default()))
        })
    });
}

fn redeem_request_verification(c: &mut Criterion) {
    let redeem_request_response = TestRedeemRequestBuilder::default().build(OsRng::default());
    c.bench_function("redeem request verification", |b| {
        b.iter(|| {
            verify_redeem_request_transaction::<DefaultCryptoImpl>(
                black_box(&redeem_request_response.redeem_request),
                &redeem_request_response.trust_pub_key,
                &[],
            )
        })
    });
}

fn redeem_request_verification_with_banned(c: &mut Criterion) {
    let banned_count = 5;
    let description = format!("redeem request verification with {} banned", banned_count);
    let rng = OsRng::default();
    let banned_members = create_banned_members_list(rng, banned_count);
    let redeem_request_response = TestRedeemRequestBuilder::default()
        .with_banned_members(banned_members.clone())
        .build(OsRng::default());
    c.bench_function(&description, |b| {
        b.iter(|| {
            verify_redeem_request_transaction::<DefaultCryptoImpl>(
                black_box(&redeem_request_response.redeem_request),
                &redeem_request_response.trust_pub_key,
                &banned_members.clone(),
            )
        })
    });
}
