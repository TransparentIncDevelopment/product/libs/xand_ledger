use std::{env, fs, path::Path};
use zkplmt::core::CryptoSystemParameters;

fn main() {
    // precompute points
    let points = CryptoSystemParameters::generate();
    let out_dir = env::var("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join("crypto_params");
    fs::write(
        &dest_path,
        serde_scale::to_vec(&points).expect("failed to serialize crypto parameters"),
    )
    .expect("failed to save precomputed crypto parameters during build.");
    println!("cargo:rerun-if-changed=build.rs");
}
