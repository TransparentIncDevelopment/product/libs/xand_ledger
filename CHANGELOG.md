# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.60.0] 2022-03-28
### Changed
* Updated terminology according to our [Taxonomy Summary Document](https://transparentfinancialsystems.sharepoint.com/:x:/s/TransparentSystems/EZHcEa1fu6lMpenk8TvLFWsBae1sKHVHcf9WIy6yA1tb8A?e=AbeEwp)

## [0.59.0] 2022-03-14
### Removed
* `InsufficientFunds` variant of `RedemptionCancelReason`

## [0.58.0] 2022-03-03
### Changed
* `ValidatorRedemption` renamed to `ClearRedemption`
* `RedemptionProcessor` now handles `ClearRedemptionTransaction` requests

### Removed
* `ClearRedemptionFulfillmentTransaction`, `ClearRedemptionCancellationTransaction` and their respective handler traits
* `ClearRedemptionProcessor` (logic now handled by `RedemptionProcessor`)

## [0.57.0] 2022-02-24
### Added
* `utxo()` getter function to `RewardTransaction`

## [0.56.0] 2022-02-09
### Added
* `to_hash` impl for `ClearTransactionOutput`

### Changed
* Function `is_participating_validator` on the `ValidatorRepository` trait renamed to `is_validator`

## [0.55.0] 2022-02-03
### Added
* `IUtxo` exposed publicly
* `TestValidatorRedemptionBuilder` exposed publicly under the `test-helpers` feature
* Functions to access fields on `ValidatorRedemptionTransaction`

## [0.54.0] - 2022-02-02
> Note - these additions were made as v.0.53.0 by accident, but were not published as such. This version publishes these changes.
### Added
* CODEOWNERS file, setting legal team members as codeowners of legal text folder
* Legal text folder

### Changed
* Moved UETA text from e-sig implementation folder to legal text folder

## [0.53.0] - 2022-01-21
### Added
* Validator Redemption feature added

## [0.52.0] - 2022-01-20
### Changed
* `ExitingRdemptionRecord` and `RedemptionRecord` consolidated into `RedemptionRecord`
* Change module style to Rust 2018

## [0.51.0] - 2022-01-13
### Changed
* Clear UTXOs are now interfaced over the `IUtxo` trait.
* `ClearTransactionOutputRepository` has an associated type for the UTXO it is to store.
* `RewardProcessor` is generic over the type of `ClearTransactionOutputRepository` it uses to process a reward.
* `RewardTransaction` and `RewardTransactionHandler` are generic over the type of UTXO they contain/handle.

## [0.50.0] - 2022-01-10
### Fixed
* UETA legalese reflects latest edits from legal in the [Product Requirements Document](https://transparentfinancialsystems.sharepoint.com/:w:/s/TransparentSystems/EaCBXdLJvfhIlOAsVG8CbdQBWWhMvqosk4bMRFWQ4AlTbw?e=vVUgHn).

## [0.49.0] - 2022-01-06
### Added
* `BannedState` now generic to allow more flexibility for the implementer of our interfaces

## [0.48.0] - 2022-01-04
### Added
* `esig_payload` module now public

## [0.47.0] - 2022-01-03
### Added
* `TransactionOutput` and `ClearTransactionOutput` now implement `Copy`

## [0.46.0] - 2021-12-23
### Added
* Custom serialization and deserialization handling for `ESigMessage`
* E-signature payload `ESigMessage` now implements `Copy`
* Reference UETA payload text as a separate text file, to facilitate audits

### Changed
* Replaced `UetaPayload` with non-exhaustive `ESigMessage` enum. This enables us to add other E-Signature schemes in the future while preserving backward compatibility.

## [0.45.0] - 2021-12-16
### Removed
* Ability to check if a network is UETA ESignature-compliant or not (and associated behavior)

## [0.44.0] - 2021-12-06

### Added
* Add validation to `RewardsProcessor` to reject reward processing transactions in non-UETA compliant networks where `ClearTransactionOutput` contains any UETA payload.

## [0.43.0] - 2021-11-30
### Added
* Add `NetworkMetadata` trait to `CreationProcessor`, `TransfersProcessor`, `ExitingRedemptionProcessor`, `RedemptionProcessor`, `RewardProcessor` for purposes of checking network status (e.g. if UETA compliant network).
* Add UETA verification for `ClearTransactionOutput`
* Add ability for non-UETA networks to bypass UETA verification for rewards processing

## [0.42.0] - 2021-11-23

* Add UETA payload to the `TransactionOutput` type and verify its validity on all transfers and creation requests

## [0.33.0] - 2021-08-02

* Add variant to `CorrelationIdUsedBy` for redeems

## [0.31.0] - 2021-07-22

* Added redemption construction and verification

## [0.30.0] - 2021-07-19

* Split out total created claims counters into dedicated `TotalIssuanceRepository`

## [0.29.1] - 2021-07-12

* Implement `Hash` for `PublicKey`.

## [0.29.0] - 2021-07-07

* Fix nostd regression that prevented 0.28.0 from building in thermite.

## [0.26.0] - 2021-06-23

* Provide a port for getting banned members.

## [0.25.2] - 2021-06-22

* Generate new random id tag in creation request.

## [0.25.0] - 2021-06-17

* Add member repository to transfers processor.
* Fix validation on identity tags for transfers so member's public keys are also accepted.

## [0.23.2] - 2021-05-27

* Extracted zkplmt module to a different repo. Moved time-consuming bulletproof setup to
  compile time via build script to avoid a cold-start issue at runtime.

## [0.22.0] - 2021-05-17
* Replace existing uses of `PrivateTransactionOutput` with the structurally equivalent `OpenedTransactionOutput` type, and remove `PrivateTransactionOutput`.

## [0.21.0] - 2021-05-11
* Export `TransfersProcessor` for external use
* Export `TestCreationRequestBuilder` and `TestTransferBuilder` using `test-helpers` feature flag to prevent accidental usage in production

## [0.20.0] - 2021-05-4
* Added `TransfersProcessor` to chain logic to enable on-chain validation of transfers

## [0.19.0] - 2021-04-16
* Added `encrypted_sender` field to `CreationRecord`
* Added `decrypt` method to the new type wrapper `VerifiableEncryptionOfSignerKey`

## [0.18.0] - 2021-04-16
* Changed return type of `get_trust_key()` to be fallible.

## [0.17.0] - 2021-04-16
### Changed
* `trust_pub_key` added to `CreateTxnInputs`
* Verifiable sender encryption is now applied to creation request handling

## [0.16.0] - 2021-04-13
### Removed
* `create_redeem_transaction()` as it was incomplete

## [0.13.0] - 2021-03-25 --

### Changed
* `create_create_transaction()` is now appropriately fallible
* `check_public_key_ownership()` renamed to
  `check_ownership()`; function signature changed

### Removed
* Unused code: `IDTagDataProvider::{my_identity_tag, masking_id_tags}`, builder impl for `CreateTxnParams`, test exercising these
* Unused `std` annotations (per the above removed code)
* In `unchecked_create_create_transaction()`, removed `assert!` panic (FYI - the `expect` was maintained for a verifiably impossible error case)


## [0.12.0] - 2021-03-25 --

### Changed
* Bumped version representing latest published version after reverting work from `v.0.11.0` for ADO [6390](https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6390) that was rolled back due to concerns about `no_std` handling)

### Added
* Changelog for repo
* README instructions on how to use the changelog

### Removed
* FYI - See reverted commit on `master` branch: `95e77366d8d88e7bf08f06d7e46381795da4a9d1`

## [0.11.0] - 2021-03-24 --

### Added
* Work for ADO [6390](https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6390) to bring in xand-financial-client to this crate

## [Unreleased]
<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->



<!-- When a new release is made, add it here
e.g. ## [0.8.2] - 2020-11-09 -->
[Unreleased]: https://gitlab.com/TransparentIncDevelopment/product/libs/xand_ledger/-/compare/0.25.0...master
[0.25.0]: https://gitlab.com/TransparentIncDevelopment/product/libs/xand_ledger/-/compare/0.24.2...0.25.0
[0.23.2]: https://gitlab.com/TransparentIncDevelopment/product/libs/xand_ledger/-/compare/0.22.0...0.23.2
