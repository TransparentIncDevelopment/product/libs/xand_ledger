use crate::{crypto::get_G, PrivateKey, PublicKey};
use alloc::vec::Vec;
use curve25519_dalek::scalar::Scalar;
use rand::{CryptoRng, RngCore};

pub fn random_private_key<R>(rng: &mut R) -> PrivateKey
where
    R: RngCore + CryptoRng,
{
    Scalar::random(rng)
}

pub fn random_public_key<R>(rng: &mut R) -> PublicKey
where
    R: RngCore + CryptoRng,
{
    PublicKey::from(random_private_key(rng))
}

pub fn create_banned_members_list<R: RngCore + CryptoRng>(mut rng: R, count: u8) -> Vec<PublicKey> {
    let mut banned_members = Vec::new();
    let g = get_G();
    for _ in 0..count {
        let private_key = random_private_key(&mut rng);
        let public_key = (private_key * g).into();
        banned_members.push(public_key);
    }
    banned_members
}

const EMPTY_UETA_TEXT: &str = "";

/// Creates an empty, type-valid `ESigPayload`. It is empty to satisfy type correctness,
/// to get tests to compile. This can be used to create an invalid
/// `ESigPayload::Ueta(_)`, because anything other than the explicit payload itself is invalid.
/// If in the future we want to allow ESig messages for a single network to vary, we will need to update the core impl.
pub fn empty_esig_payload() -> &'static str {
    EMPTY_UETA_TEXT
}
