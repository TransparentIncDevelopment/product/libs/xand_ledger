// Avoid clippy warning in CreateRequestProcessor constructor hidden behind derive_more::Constructor macro
#![allow(clippy::too_many_arguments)]

#[cfg(test)]
mod tests;

use crate::{
    crypto::LedgerCrypto, errors::FailureReason,
    esig_payload::opened_transaction_output_contains_valid_ueta, model::*,
    transactions::verify_create_request_transaction,
};
use core::marker::PhantomData;

/// A collection of repositories that when combined are capable of processing create requests,
/// and implement [CreateRequestHandler](trait.CreateRequestHandler.html)
#[derive(derive_more::Constructor)]
pub struct CreateRequestProcessor<'a, CreateRequestRecordIter, Crypto, BlockNumber: PartialOrd> {
    create_request_repo: &'a dyn CreateRequestRepository<Iter = CreateRequestRecordIter>,
    total_issuance_repo: &'a dyn TotalIssuanceRepository,
    correlation_id_repo: &'a dyn CorrelationIdRepository,
    identity_tag_repo: &'a dyn IdentityTagRepository,
    txo_repo: &'a dyn TransactionDataRepository<BlockNumber>,
    member_repo: &'a dyn MemberRepository<BlockNumber>,
    pending_create_req_exp_time: &'a dyn PendingCreateRequestExpireTime,
    trust_metadata: &'a dyn TrustMetadata,
    _crypto: PhantomData<Crypto>,
}

impl<'a, CI, Crypto, BlockNumber> CreateRequestProcessor<'a, CI, Crypto, BlockNumber>
where
    CI: Iterator<Item = (CorrelationId, CreateRequestRecord)>,
    Crypto: LedgerCrypto,
    BlockNumber: PartialOrd,
{
    /// If you have one type which implements all needed repositories, you can use this constructor
    /// instead.
    pub fn new_unified<T>(implements_all: &'a T) -> Self
    where
        T: CreateRequestRepository<Iter = CI>
            + TotalIssuanceRepository
            + CorrelationIdRepository
            + IdentityTagRepository
            + TransactionDataRepository<BlockNumber>
            + PendingCreateRequestExpireTime
            + MemberRepository<BlockNumber>
            + TrustMetadata,
    {
        CreateRequestProcessor::new(
            implements_all,
            implements_all,
            implements_all,
            implements_all,
            implements_all,
            implements_all,
            implements_all,
            implements_all,
            PhantomData,
        )
    }
}

fn verify_create_request_record_does_not_already_exist<CI>(
    repo: &dyn CreateRequestRepository<Iter = CI>,
    txn: &CreateRequestTransaction,
) -> Result<(), FailureReason>
where
    CI: Iterator<Item = (CorrelationId, CreateRequestRecord)>,
{
    if repo
        .get_create_request(txn.core_transaction.correlation_id)
        .is_some()
    {
        return Err(FailureReason::PendingCreateRequestIdAlreadyExists);
    }
    Ok(())
}

fn verify_correlation_id_has_not_been_used(
    repo: &dyn CorrelationIdRepository,
    txn: &CreateRequestTransaction,
) -> Result<(), FailureReason> {
    if repo.is_used(txn.core_transaction.correlation_id).is_some() {
        return Err(FailureReason::CorrelationIdCannotBeReused);
    }
    Ok(())
}

fn verify_identity_tags_are_valid<BlockNumber>(
    id_repo: &dyn IdentityTagRepository,
    member_repo: &dyn MemberRepository<BlockNumber>,
    txn: &CreateRequestTransaction,
) -> Result<(), FailureReason> {
    if txn
        .core_transaction
        .identity_sources
        .iter()
        .all(|t| id_repo.exists(t) || member_repo.is_valid_member(t))
    {
        return Ok(());
    }
    Err(FailureReason::UnknownIdentity)
}

fn verify_outputs_are_being_created(txn: &CreateRequestTransaction) -> Result<(), FailureReason> {
    if txn.core_transaction.outputs.is_empty() {
        return Err(FailureReason::CreateRequestMustProduceOutputs);
    }
    Ok(())
}

fn verify_none_of_the_outputs_have_zero_value(
    txn: &CreateRequestTransaction,
) -> Result<(), FailureReason> {
    if txn
        .core_transaction
        .outputs
        .iter()
        .any(|txo| txo.value == 0)
    {
        return Err(FailureReason::CreateRequestAmountMustBeMoreThanZero);
    }
    Ok(())
}

fn verify_none_of_the_outputs_already_exist<BlockNumber>(
    repo: &dyn TransactionDataRepository<BlockNumber>,
    txn: &CreateRequestTransaction,
) -> Result<(), FailureReason> {
    if !txn
        .core_transaction
        .outputs
        .iter()
        .all(|txo| repo.lookup_status(&txo.transaction_output).0 == TxoStatus::Nonexistent)
    {
        return Err(FailureReason::OutputTxoAlreadyExists);
    }
    Ok(())
}

fn verify_proofs_are_valid<T: LedgerCrypto, BlockNumber>(
    trust_metadata: &dyn TrustMetadata,
    member_repo: &dyn MemberRepository<BlockNumber>,
    txn: &CreateRequestTransaction,
) -> Result<(), FailureReason> {
    let trust_key = trust_metadata
        .get_trust_key()
        .ok_or(FailureReason::InvalidTrustAccountId)?;
    let banned_members = member_repo
        .get_banned_members()
        .into_iter()
        .map(|(p, _)| p)
        .collect();
    verify_create_request_transaction::<T>(txn, &trust_key, banned_members)?;
    Ok(())
}

fn verify_outputs_are_ueta_compliant(txn: &CreateRequestTransaction) -> Result<(), FailureReason> {
    if !txn
        .core_transaction
        .outputs
        .iter()
        .all(opened_transaction_output_contains_valid_ueta)
    {
        return Err(FailureReason::TxoNotUETACompliant);
    }
    Ok(())
}

impl<CI, T, BlockNumber> CreateRequestHandler for CreateRequestProcessor<'_, CI, T, BlockNumber>
where
    CI: Iterator<Item = (CorrelationId, CreateRequestRecord)>,
    T: LedgerCrypto,
    BlockNumber: PartialOrd,
{
    fn create_request(&self, txn: CreateRequestTransaction) -> Result<(), FailureReason> {
        verify_create_request_record_does_not_already_exist(self.create_request_repo, &txn)?;
        verify_correlation_id_has_not_been_used(self.correlation_id_repo, &txn)?;
        verify_identity_tags_are_valid(self.identity_tag_repo, self.member_repo, &txn)?;
        verify_outputs_are_being_created(&txn)?;
        verify_none_of_the_outputs_have_zero_value(&txn)?;
        verify_none_of_the_outputs_already_exist(self.txo_repo, &txn)?;
        verify_outputs_are_ueta_compliant(&txn)?;

        verify_proofs_are_valid::<T, BlockNumber>(self.trust_metadata, self.member_repo, &txn)?;

        //Transaction is valid at this point. Apply expected changes
        // Store txos as pending
        for txo in &txn.core_transaction.outputs {
            self.txo_repo
                .set_output_status(txo.transaction_output, TxoStatus::Pending);
        }
        self.correlation_id_repo
            .store_id(txn.core_transaction.correlation_id);

        let expires_at = self
            .pending_create_req_exp_time
            .calc_expire_time_for_new_create_request();

        self.create_request_repo.store_create_request(
            txn.core_transaction.correlation_id,
            CreateRequestRecord::from_request(txn, expires_at),
        );

        Ok(())
    }
}

impl<CI, T, BlockNumber> CashConfirmationHandler for CreateRequestProcessor<'_, CI, T, BlockNumber>
where
    CI: Iterator<Item = (CorrelationId, CreateRequestRecord)>,
    BlockNumber: PartialOrd,
{
    fn cash_confirmation(&self, transaction: CashConfirmationTransaction) -> TransactionResult {
        // TODO: Should we attempt to verify trust sig in domain?

        // Check there is a matching pending create
        let maybe_create_request = self
            .create_request_repo
            .get_create_request(transaction.correlation_id);
        match maybe_create_request {
            None => return Err(FailureReason::NoMatchingCreateRequestToFulfill),
            Some(pending_create) => {
                // Delete the create request
                self.create_request_repo
                    .remove_create_request(transaction.correlation_id);
                // Mark claims as confirmed and therefore spendable
                for txo in pending_create.outputs {
                    self.txo_repo
                        .set_output_status(txo.transaction_output, TxoStatus::Confirmed);
                    // Update total created claims amount
                    self.total_issuance_repo
                        .add_to_total_cash_confirmations(txo.value);
                }
                // Make identity tag output available for use
                self.identity_tag_repo
                    .store_id_tag(pending_create.identity_output);
            }
        }
        Ok(())
    }
}

impl<CI, T, BlockNumber> CreateCancellationHandler
    for CreateRequestProcessor<'_, CI, T, BlockNumber>
where
    CI: Iterator<Item = (CorrelationId, CreateRequestRecord)>,
    BlockNumber: PartialOrd,
{
    fn create_cancellation(&self, transaction: CreateCancellationTransaction) -> TransactionResult {
        // Check there is a matching pending create
        match self
            .create_request_repo
            .get_create_request(transaction.correlation_id)
        {
            None => Err(FailureReason::NoMatchingCreateRequestToCancel),
            Some(c) => {
                // Delete the create request
                self.create_request_repo
                    .remove_create_request(transaction.correlation_id);
                Ok(())
            }
        }
    }

    fn cancel_expired_create_requests(&self, dispatcher: &dyn Dispatcher) -> TransactionResult {
        let current_timestamp = self.pending_create_req_exp_time.current_block_timestamp();
        for (id, pcreate) in self.create_request_repo.all_pending_create_requests() {
            let expires_at = pcreate.expires_at;
            if expires_at <= current_timestamp {
                // It's expired. Build appropriate cancellation transaction
                let cancellation =
                    CreateCancellationTransaction::new(id, CreateCancellationReason::Expired);
                dispatcher.dispatch(cancellation.into())?;
            }
        }
        Ok(())
    }
}
