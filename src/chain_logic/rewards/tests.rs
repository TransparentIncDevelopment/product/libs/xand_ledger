#![allow(non_snake_case)]

use crate::{
    chain_logic::rewards::RewardProcessor,
    esig_payload::errors::ESigError,
    model::reward_transactions::{RewardTransaction, RewardTransactionHandler},
    test::fakes::{
        fake_clear_transaction_data_repository::FakeClearTransactionDataRepository,
        FakeTotalIssuanceRepository,
    },
    ClearTransactionOutput, ClearTransactionOutputRepository, FailureReason, MonetaryValue,
    TotalIssuanceRepository,
};
use core::convert::TryFrom;

struct TestContext {
    utxo_repo: FakeClearTransactionDataRepository,
    total_issuance_repo: FakeTotalIssuanceRepository,
}

impl TestContext {
    fn new() -> Self {
        Self {
            utxo_repo: FakeClearTransactionDataRepository::new(),
            total_issuance_repo: FakeTotalIssuanceRepository::new(),
        }
    }

    fn reward_processor(&self) -> RewardProcessor<'_, FakeClearTransactionDataRepository> {
        RewardProcessor {
            utxo_repo: &self.utxo_repo,
            total_issuance_repo: &self.total_issuance_repo,
        }
    }
}

fn ueta_compliant_clear_txn_with_amt(amt: u64) -> ClearTransactionOutput {
    ClearTransactionOutput::with_compliant_esig_payload(
        "A".into(),
        MonetaryValue::try_from(amt).unwrap(),
        0,
    )
}

fn clear_txn_with_custom_esig_payload(
    esig_text: &'static str,
) -> Result<ClearTransactionOutput, ESigError> {
    ClearTransactionOutput::with_custom_esig_payload(
        "A".into(),
        MonetaryValue::try_from(1_u64).unwrap(),
        0,
        esig_text,
    )
}

#[test]
fn process_reward_transaction__happy_path_utxo_reward_gets_added_to_storage() {
    // Given
    let ctx = TestContext::new();
    let utxo = ueta_compliant_clear_txn_with_amt(1_u64);
    let txn = RewardTransaction::new(utxo);
    let processor = ctx.reward_processor();

    // When
    processor.process_reward_transaction(txn).unwrap();

    // Then
    assert!(ctx.utxo_repo.current_txos().contains(&utxo));
    assert_eq!(ctx.total_issuance_repo.get_total_cash_confirmations(), 1);
}

#[test]
fn process_reward_transaction__utxo_reward_can_increase_total_cash_confirmations_by_more_than_one_claim(
) {
    // Given
    let ctx = TestContext::new();
    let utxo = ueta_compliant_clear_txn_with_amt(17_u64);
    let txn = RewardTransaction::new(utxo);
    let processor = ctx.reward_processor();

    // When
    processor.process_reward_transaction(txn).unwrap();

    // Then
    assert_eq!(ctx.total_issuance_repo.get_total_cash_confirmations(), 17);
}

#[test]
fn process_reward_transaction__fails_if_utxo_reward_already_exists() {
    // Given
    let ctx = TestContext::new();
    let utxo = ueta_compliant_clear_txn_with_amt(1_u64);
    ctx.utxo_repo.add(utxo);
    let txn = RewardTransaction::new(utxo);
    let processor = ctx.reward_processor();

    // When
    let error = processor.process_reward_transaction(txn).unwrap_err();

    // Then
    assert_eq!(error, FailureReason::OutputTxoAlreadyExists);
    assert_eq!(ctx.total_issuance_repo.get_total_cash_confirmations(), 0);
}

mod esig_payload {
    use crate::{
        chain_logic::rewards::tests::{
            clear_txn_with_custom_esig_payload, ueta_compliant_clear_txn_with_amt, TestContext,
        },
        clear_txo::IUtxo,
        esig_payload::{errors::ESigError, ESigPayload, UETA_TEXT},
        model::reward_transactions::{RewardTransaction, RewardTransactionHandler},
        test_helpers::empty_esig_payload,
    };
    use insta::assert_display_snapshot;

    #[test]
    fn process_reward_transaction__processed_reward_contains_clear_utxo_with_valid_ueta_payload() {
        // Given
        let ctx = TestContext::new();
        let utxo = ueta_compliant_clear_txn_with_amt(1_u64);
        let txn = RewardTransaction::new(utxo);
        let processor = ctx.reward_processor();

        // When
        processor.process_reward_transaction(txn).unwrap();
        let maybe_utxo = ctx.utxo_repo.current_txos().get(&utxo).cloned();
        let resulting_payload = ctx.utxo_repo.get_ueta_payload_for_utxo(utxo);

        // Then
        assert_eq!(resulting_payload.ueta_text(), Some(String::from(UETA_TEXT)));
        assert!(
            matches!(maybe_utxo.unwrap(), ClearTransactionOutput),
            "Expected ClearTransactionOutput clear UTXO but got {:?}",
            maybe_utxo
        );
    }

    #[test]
    fn constructing_clear_utxo_for_reward_fails_if_invalid_ueta_payload() {
        // Given an invalid UETA payload
        // When we attempt to construct a clear TXO for a rewards transaction
        let res = clear_txn_with_custom_esig_payload(empty_esig_payload()).unwrap_err();
        // Then
        assert!(matches!(res, ESigError::InvalidStringContent));
    }

    #[test]
    fn process_reward_transaction__successfully_constructed_reward_txn_has_valid_ueta_output_in_txos(
    ) {
        // Given valid reward that contains a UTXO with a UETA compliant payload
        let ctx = TestContext::new();
        let utxo = ueta_compliant_clear_txn_with_amt(1_u64);

        // When we construct and submit a reward transaction
        let txn = RewardTransaction::new(utxo);

        // Then it has the correct payload
        assert_display_snapshot!(UETA_TEXT);
        assert!(matches!(txn.utxo.esig_payload(), ESigPayload::Ueta(_)));
        assert_eq!(
            txn.utxo.esig_payload().ueta_text(),
            Some(UETA_TEXT.to_string())
        );
    }
}
