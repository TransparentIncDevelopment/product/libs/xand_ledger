use super::*;
use crate::{
    crypto::DefaultCryptoImpl,
    model::{MockDispatcher, MockPendingCreateRequestExpireTime},
    test::{
        arb_rng,
        fakes::{
            FakeCorrelationIdRepository, FakeCreateRequestRepository, FakeCsprng,
            FakeIdentityTagRepo, FakeMemberRepository, FakeTotalIssuanceRepository,
            FakeTransactionDataRepository, FakeTrustMetadata,
        },
    },
    transactions::{TestCreateRequestBuilder, TransactionValidationError},
};
use mockall::predicate::eq;
use proptest::prelude::*;
use rand::{rngs::OsRng, CryptoRng};
use std::collections::HashMap;

fn default_pending_create_request_expire_time_repo() -> MockPendingCreateRequestExpireTime {
    let mut pending_create_request_expire_time_repo = MockPendingCreateRequestExpireTime::new();
    // existing pending create
    pending_create_request_expire_time_repo
        .expect_calc_expire_time_for_new_create_request()
        .return_once(|| 100.into());
    pending_create_request_expire_time_repo
}

struct TestContext {
    create_request_repo: FakeCreateRequestRepository,
    total_issuance_repo: FakeTotalIssuanceRepository,
    correlation_id_repo: FakeCorrelationIdRepository,
    identity_tag_repo: FakeIdentityTagRepo,
    txo_repo: FakeTransactionDataRepository,
    member_repo: FakeMemberRepository,
    trust_metadata: FakeTrustMetadata,
    pending_create_req_exp_time: MockPendingCreateRequestExpireTime,
}

impl TestContext {
    fn new() -> Self {
        Self {
            create_request_repo: FakeCreateRequestRepository::new(),
            total_issuance_repo: FakeTotalIssuanceRepository::new(),
            correlation_id_repo: FakeCorrelationIdRepository::new(),
            identity_tag_repo: FakeIdentityTagRepo::new(),
            txo_repo: FakeTransactionDataRepository::new(),
            trust_metadata: FakeTrustMetadata::new(),
            member_repo: FakeMemberRepository::new(),
            pending_create_req_exp_time: default_pending_create_request_expire_time_repo(),
        }
    }

    fn from_identity_tags_and_trust_key(tags: &[IdentityTag], trust_key: PublicKey) -> Self {
        Self {
            identity_tag_repo: FakeIdentityTagRepo::seeded(tags),
            trust_metadata: FakeTrustMetadata::from_trust_key(trust_key),
            ..Self::new()
        }
    }

    fn from_identity_tags(tags: &[IdentityTag]) -> Self {
        Self {
            identity_tag_repo: FakeIdentityTagRepo::seeded(tags),
            ..Self::new()
        }
    }

    fn create_request_processor(
        &self,
    ) -> CreateRequestProcessor<
        '_,
        impl Iterator<Item = (CorrelationId, CreateRequestRecord)>,
        DefaultCryptoImpl,
        u64,
    > {
        CreateRequestProcessor {
            create_request_repo: &self.create_request_repo,
            total_issuance_repo: &self.total_issuance_repo,
            correlation_id_repo: &self.correlation_id_repo,
            identity_tag_repo: &self.identity_tag_repo,
            txo_repo: &self.txo_repo,
            member_repo: &self.member_repo,
            trust_metadata: &self.trust_metadata,
            pending_create_req_exp_time: &self.pending_create_req_exp_time,
            _crypto: PhantomData,
        }
    }
}

mod create_request {
    use super::*;
    use crate::{verifiable_encryption::EncryptionVerificationError, Scalar, ZkPlmtError};

    #[test]
    fn processing_create_request_txn_works() {
        happy_path(&mut OsRng::default());
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(100))]
        #[test]
        fn proptest_working_create_request_txns(rng_seed: [u8; 32]) {
            let mut rngref = FakeCsprng::from_seed(rng_seed);
            happy_path(&mut rngref);
        }
    }

    fn happy_path<Rng>(rng: &mut Rng) -> CreateRequestTransaction
    where
        Rng: CryptoRng + RngCore,
    {
        //Given
        let txn = TestCreateRequestBuilder::default().build(&mut OsRng::default());
        let correlation_id = txn.create_request.core_transaction.correlation_id;
        let expected_create_request_record =
            CreateRequestRecord::from_request(txn.create_request.clone(), 100.into());

        let ctx = TestContext::from_identity_tags_and_trust_key(
            &txn.create_request.core_transaction.identity_sources,
            txn.trust_pub_key,
        );

        //When
        let fth = ctx.create_request_processor();

        fth.create_request(txn.create_request.clone()).unwrap();

        //Then
        assert_eq!(ctx.total_issuance_repo.get_total_cash_confirmations(), 0);
        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Some(expected_create_request_record)
        );

        assert_eq!(ctx.correlation_id_repo.all_used_ids(), vec!(correlation_id));
        //Identity tag is not stored until confirmation
        assert!(!ctx
            .identity_tag_repo
            .exists(&txn.create_request.core_transaction.identity_output));
        let expected_outputs = txn
            .create_request
            .core_transaction
            .outputs
            .iter()
            .map(|txo| (txo.transaction_output, (TxoStatus::Pending, Some(0))))
            .collect::<HashMap<_, _>>();
        assert_eq!(ctx.txo_repo.current_txos(), expected_outputs);
        txn.create_request
    }

    #[test]
    fn create_request_succeeds_with_valid_member_address_as_decoy() {
        let csprng = &mut OsRng::default();

        let masking_identity =
            IdentityTag::from_key_with_generator_base(Scalar::random(csprng).into());
        let masking_identities = vec![
            IdentityTag::random(csprng),
            IdentityTag::random(csprng),
            IdentityTag::random(csprng),
            masking_identity,
        ];
        let txn = TestCreateRequestBuilder::default()
            .masking_identity_sources(masking_identities)
            .build(csprng);

        let transaction_identities = &txn
            .create_request
            .core_transaction
            .identity_sources
            .clone()
            .into_iter()
            .filter(|s| s != &masking_identity)
            .collect::<Vec<IdentityTag>>();

        let ctx = TestContext {
            identity_tag_repo: FakeIdentityTagRepo::seeded(transaction_identities),
            member_repo: FakeMemberRepository::new().with_members(vec![masking_identity]),
            trust_metadata: FakeTrustMetadata::from_trust_key(txn.trust_pub_key),
            ..TestContext::new()
        };

        //When
        let processor = ctx.create_request_processor();

        let response = processor.create_request(txn.create_request);
        assert!(response.is_ok())
    }

    #[test]
    fn create_request_succeeds_with_valid_member_address_as_true_source() {
        let csprng = &mut OsRng::default();

        let private_key = Scalar::random(csprng);
        let identity_source = IdentityTag::from_key_with_generator_base(private_key.into());

        let txn = TestCreateRequestBuilder::default()
            .identity_source(identity_source)
            .private_key(private_key)
            .build(csprng);

        let transaction_identities = &txn
            .create_request
            .core_transaction
            .identity_sources
            .clone()
            .into_iter()
            .filter(|s| s != &identity_source)
            .collect::<Vec<IdentityTag>>();

        let ctx = TestContext {
            identity_tag_repo: FakeIdentityTagRepo::seeded(transaction_identities),
            member_repo: FakeMemberRepository::new().with_members(vec![identity_source]),
            trust_metadata: FakeTrustMetadata::from_trust_key(txn.trust_pub_key),
            ..TestContext::new()
        };

        //When
        let processor = ctx.create_request_processor();

        let response = processor.create_request(txn.create_request);
        assert!(response.is_ok())
    }

    #[test]
    fn create_request_fails_with_invalid_member_address() {
        let csprng = &mut OsRng::default();

        let masking_identity =
            IdentityTag::from_key_with_generator_base(Scalar::random(csprng).into());
        let masking_identities = vec![
            IdentityTag::random(csprng),
            IdentityTag::random(csprng),
            IdentityTag::random(csprng),
            masking_identity,
        ];
        let txn = TestCreateRequestBuilder::default()
            .masking_identity_sources(masking_identities)
            .build(csprng);

        let transaction_identities = &txn
            .create_request
            .core_transaction
            .identity_sources
            .clone()
            .into_iter()
            .filter(|s| s != &masking_identity)
            .collect::<Vec<IdentityTag>>();

        let ctx = TestContext {
            identity_tag_repo: FakeIdentityTagRepo::seeded(transaction_identities),
            member_repo: FakeMemberRepository::new(),
            ..TestContext::new()
        };

        //When
        let processor = ctx.create_request_processor();

        let response = processor.create_request(txn.create_request);
        assert!(response.is_err())
    }

    #[test]
    fn rejected_when_called_with_duplicate_id() {
        // Given
        let expected_result = FailureReason::PendingCreateRequestIdAlreadyExists;
        let txn = TestCreateRequestBuilder::default().build(&mut OsRng::default());
        let correlation_id = txn.create_request.core_transaction.correlation_id;
        let existing_record =
            CreateRequestRecord::from_request(txn.create_request.clone(), 1000.into());

        let ctx =
            TestContext::from_identity_tags(&txn.create_request.core_transaction.identity_sources);
        ctx.create_request_repo
            .store_create_request(correlation_id, existing_record.clone());
        let sut = ctx.create_request_processor();

        // When
        let result = sut.create_request(txn.create_request.clone()).unwrap_err();

        // Then
        assert_eq!(result, expected_result);
        //Existing record was not overwritten
        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Some(existing_record)
        );
        assert!(ctx.correlation_id_repo.all_used_ids().is_empty());
        assert!(!ctx
            .identity_tag_repo
            .exists(&txn.create_request.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }

    #[test]
    fn rejected_when_called_with_previously_used_correlation_id() {
        let expected_res = FailureReason::CorrelationIdCannotBeReused;
        let txn = TestCreateRequestBuilder::default().build(&mut OsRng::default());
        let correlation_id = txn.create_request.core_transaction.correlation_id;

        let ctx =
            TestContext::from_identity_tags(&txn.create_request.core_transaction.identity_sources);
        ctx.correlation_id_repo.store_id(correlation_id);
        let fth = ctx.create_request_processor();

        assert_eq!(
            fth.create_request(txn.create_request.clone()).unwrap_err(),
            expected_res
        );
        assert!(ctx
            .create_request_repo
            .get_create_request(correlation_id)
            .is_none());
        assert_eq!(ctx.correlation_id_repo.all_used_ids(), vec!(correlation_id));
        assert!(!ctx
            .identity_tag_repo
            .exists(&txn.create_request.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }

    #[test]
    fn rejected_when_identity_tags_do_not_exist() {
        let expected_res = FailureReason::UnknownIdentity;

        let txn = TestCreateRequestBuilder::default().build(&mut OsRng::default());
        let correlation_id = txn.create_request.core_transaction.correlation_id;

        let ctx = TestContext::new();
        let fth = ctx.create_request_processor();

        assert_eq!(
            fth.create_request(txn.create_request.clone()).unwrap_err(),
            expected_res
        );
        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert!(ctx.correlation_id_repo.all_used_ids().is_empty());
        assert!(!ctx
            .identity_tag_repo
            .exists(&txn.create_request.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }

    #[test]
    fn rejects_zero_value_outputs() {
        let mut csprng = OsRng::default();

        let txn = TestCreateRequestBuilder::default()
            .values(vec![0, 1, 2, 3])
            .build(&mut csprng);
        let correlation_id = txn.create_request.core_transaction.correlation_id;

        let ctx =
            TestContext::from_identity_tags(&txn.create_request.core_transaction.identity_sources);
        let fth = ctx.create_request_processor();

        assert_eq!(
            fth.create_request(txn.create_request.clone()).unwrap_err(),
            FailureReason::CreateRequestAmountMustBeMoreThanZero
        );
        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert!(ctx.correlation_id_repo.all_used_ids().is_empty());
        assert!(!ctx
            .identity_tag_repo
            .exists(&txn.create_request.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }

    #[test]
    fn rejected_when_creating_outputs_that_already_exist() {
        let expected_res = FailureReason::OutputTxoAlreadyExists;
        let txn = TestCreateRequestBuilder::default().build(&mut OsRng::default());
        let correlation_id = txn.create_request.core_transaction.correlation_id;
        let expected: Vec<TransactionOutput> = txn
            .create_request
            .core_transaction
            .outputs
            .iter()
            .map(|x| x.transaction_output)
            .collect();

        let mut ctx =
            TestContext::from_identity_tags(&txn.create_request.core_transaction.identity_sources);
        ctx.txo_repo = FakeTransactionDataRepository::seeded(&expected, TxoStatus::Confirmed, 0);
        let fth = ctx.create_request_processor();

        assert_eq!(
            fth.create_request(txn.create_request.clone()).unwrap_err(),
            expected_res
        );
        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert!(ctx.correlation_id_repo.all_used_ids().is_empty());
        assert!(!ctx
            .identity_tag_repo
            .exists(&txn.create_request.core_transaction.identity_output));
        //Existing Outputs should still exist
        let expected_outputs = txn
            .create_request
            .core_transaction
            .outputs
            .iter()
            .map(|txo| (txo.transaction_output, (TxoStatus::Confirmed, Some(0))))
            .collect::<HashMap<_, _>>();
        assert_eq!(ctx.txo_repo.current_txos(), expected_outputs);
    }

    #[test]
    fn rejected_when_transaction_cannot_be_verified() {
        let expected_res = FailureReason::TransactionCouldNotBeVerified(
            TransactionValidationError::OpenedCommitmentMismatch,
        );
        let txn = {
            let mut txn = TestCreateRequestBuilder::default()
                .build(&mut OsRng::default())
                .create_request;
            //trick chain into giving us so much money
            txn.core_transaction.outputs[0].value = u64::MAX - 1;
            txn
        };
        let correlation_id = txn.core_transaction.correlation_id;

        let ctx = TestContext::from_identity_tags(&txn.core_transaction.identity_sources);
        let fth = ctx.create_request_processor();

        assert_eq!(fth.create_request(txn.clone()).unwrap_err(), expected_res);
        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert!(ctx.correlation_id_repo.all_used_ids().is_empty());
        assert!(!ctx
            .identity_tag_repo
            .exists(&txn.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }

    #[test]
    fn rejected_when_altered() {
        let expected_res = FailureReason::TransactionCouldNotBeVerified(
            TransactionValidationError::InvalidEncryption(
                EncryptionVerificationError::FailedZkplmtError(ZkPlmtError::InvalidProof),
            ),
        );
        let txn = {
            let mut txn = TestCreateRequestBuilder::default()
                .build(&mut OsRng::default())
                .create_request;
            txn.core_transaction.extra = b"That wasn't the right signature".to_vec();
            txn
        };
        let correlation_id = txn.core_transaction.correlation_id;

        let ctx = TestContext::from_identity_tags(&txn.core_transaction.identity_sources);
        let fth = ctx.create_request_processor();

        assert_eq!(fth.create_request(txn.clone()).unwrap_err(), expected_res);
        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert!(ctx.correlation_id_repo.all_used_ids().is_empty());
        assert!(!ctx
            .identity_tag_repo
            .exists(&txn.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }

    #[test]
    fn rejected_when_no_outputs() {
        let txn = {
            let mut txn = TestCreateRequestBuilder::default()
                .build(&mut OsRng::default())
                .create_request;
            txn.core_transaction.outputs = vec![];
            txn
        };
        let correlation_id = txn.core_transaction.correlation_id;

        let ctx = TestContext::from_identity_tags(&txn.core_transaction.identity_sources);
        let fth = ctx.create_request_processor();

        assert_eq!(
            fth.create_request(txn.clone()).unwrap_err(),
            FailureReason::CreateRequestMustProduceOutputs
        );
        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert!(ctx.correlation_id_repo.all_used_ids().is_empty());
        assert!(!ctx
            .identity_tag_repo
            .exists(&txn.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }
}

mod cash_confirmation {
    use super::*;

    #[test]
    fn happy_path() {
        let mut csprng = OsRng::default();

        // Make an imaginary create request
        let mut req = TestCreateRequestBuilder::default()
            .build(&mut csprng)
            .create_request;
        let id_tag = IdentityTag::random(&mut csprng);
        req.core_transaction.identity_output = id_tag;
        let correlation_id = req.core_transaction.correlation_id;
        let as_record = CreateRequestRecord::from_request(req.clone(), 1.into());

        let ctx = TestContext::new();
        ctx.create_request_repo
            .store_create_request(correlation_id, as_record);
        let fth = ctx.create_request_processor();

        // Issue a confirmation for the create
        let confirm_txn = CashConfirmationTransaction::new(req.core_transaction.correlation_id);
        fth.cash_confirmation(confirm_txn).unwrap();

        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert!(ctx.total_issuance_repo.get_total_cash_confirmations() > 0);

        assert!(ctx
            .identity_tag_repo
            .exists(&req.core_transaction.identity_output));
        let expected_outputs = req
            .core_transaction
            .outputs
            .iter()
            .map(|txo| (txo.transaction_output, (TxoStatus::Confirmed, Some(0))))
            .collect::<HashMap<_, _>>();
        assert_eq!(ctx.txo_repo.current_txos(), expected_outputs);
    }

    #[test]
    fn no_matching_create() {
        let mut csprng = OsRng::default();

        let req = TestCreateRequestBuilder::default()
            .build(&mut csprng)
            .create_request;
        let correlation_id = req.core_transaction.correlation_id;
        let id_tag = IdentityTag::random(&mut csprng);

        let ctx = TestContext::new();
        let fth = ctx.create_request_processor();

        let confirm_txn = CashConfirmationTransaction::new(req.core_transaction.correlation_id);
        assert_eq!(
            fth.cash_confirmation(confirm_txn).unwrap_err(),
            FailureReason::NoMatchingCreateRequestToFulfill
        );
        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert_eq!(ctx.total_issuance_repo.get_total_cash_confirmations(), 0);

        assert!(!ctx
            .identity_tag_repo
            .exists(&req.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }
}

mod create_cancellation {
    use super::*;

    #[test]
    fn happy_path() {
        let mut csprng = OsRng::default();

        // Make an imaginary create request
        let req = TestCreateRequestBuilder::default()
            .build(&mut csprng)
            .create_request;
        let correlation_id = req.core_transaction.correlation_id;
        let as_record = CreateRequestRecord::from_request(req.clone(), 1.into());

        let ctx = TestContext::new();
        ctx.create_request_repo
            .store_create_request(correlation_id, as_record);
        let fth = ctx.create_request_processor();

        // Issue a cancel for the create
        let cancel_txn = CreateCancellationTransaction::new(
            req.core_transaction.correlation_id,
            CreateCancellationReason::Expired,
        );
        fth.create_cancellation(cancel_txn).unwrap();

        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert_eq!(ctx.total_issuance_repo.get_total_cash_confirmations(), 0);

        assert!(!ctx
            .identity_tag_repo
            .exists(&req.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }

    #[test]
    fn no_matching_create() {
        let mut csprng = OsRng::default();

        // Make an imaginary create request
        let req = TestCreateRequestBuilder::default()
            .build(&mut csprng)
            .create_request;
        let correlation_id = req.core_transaction.correlation_id;
        let as_record = CreateRequestRecord::from_request(req.clone(), 1.into());

        let ctx = TestContext::new();
        let fth = ctx.create_request_processor();

        // Issue a cancel for the create
        let cancel_txn = CreateCancellationTransaction::new(
            req.core_transaction.correlation_id,
            CreateCancellationReason::Expired,
        );
        assert_eq!(
            fth.create_cancellation(cancel_txn).unwrap_err(),
            FailureReason::NoMatchingCreateRequestToCancel
        );

        assert_eq!(
            ctx.create_request_repo.get_create_request(correlation_id),
            Option::None
        );
        assert_eq!(ctx.total_issuance_repo.get_total_cash_confirmations(), 0);

        assert!(!ctx
            .identity_tag_repo
            .exists(&req.core_transaction.identity_output));
        assert_eq!(ctx.txo_repo.current_txos().len(), 0);
    }

    #[test]
    fn expired_creates_cancelled_properly() {
        let mut csprng = OsRng::default();

        let am_expired = TestCreateRequestBuilder::default()
            .build(&mut csprng)
            .create_request;
        let not_expired = TestCreateRequestBuilder::default()
            .build(&mut csprng)
            .create_request;
        let expired_correlation_id = am_expired.core_transaction.correlation_id;
        let unexpired_correlation_id = not_expired.core_transaction.correlation_id;
        let expired_record = CreateRequestRecord::from_request(am_expired.clone(), 1.into());
        let unexpired_record = CreateRequestRecord::from_request(not_expired, 999.into());
        let expected_cancel_txn: LedgerTransaction = CreateCancellationTransaction::new(
            am_expired.core_transaction.correlation_id,
            CreateCancellationReason::Expired,
        )
        .into();

        let mut ctx = TestContext::new();
        ctx.create_request_repo
            .store_create_request(expired_correlation_id, expired_record);
        ctx.create_request_repo
            .store_create_request(unexpired_correlation_id, unexpired_record);
        ctx.pending_create_req_exp_time = {
            let mut pending_create_req_exp_time = MockPendingCreateRequestExpireTime::new();
            // Current block ts is 100
            pending_create_req_exp_time
                .expect_current_block_timestamp()
                .return_once(|| 100.into());
            pending_create_req_exp_time
        };
        let mut mock_dispatcher = MockDispatcher::new();
        mock_dispatcher
            .expect_dispatch()
            // There should only be on cancel call since the other request is not expired
            .times(1)
            .with(eq(expected_cancel_txn))
            .return_const(Ok(()));

        let fth = ctx.create_request_processor();

        fth.cancel_expired_create_requests(&mock_dispatcher)
            .unwrap();
    }
}

mod esig_payload {
    use super::*;
    use crate::{
        esig_payload::{errors::ESigError, ESigPayload, UETA_TEXT},
        test_helpers::empty_esig_payload,
    };
    use insta::assert_display_snapshot;
    use zkplmt::core::RistrettoPoint;

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn create_request_fails_without_ueta_payload_on_output(
            mut rng in arb_rng()
        ) {
            // Given a create request that contains a TXO with a non-UETA compliant payload
            let txn = TestCreateRequestBuilder::default().build(&mut rng);
            let correlation_id = txn.create_request.core_transaction.correlation_id;

            // When
            let res = TransactionOutput::with_custom_esig_payload(
                RistrettoPoint::default(),OneTimeKey::random(&mut rng), empty_esig_payload()
            ).unwrap_err();

            // Then
            assert!(matches!(res, ESigError::InvalidStringContent));
        }
    }

    #[allow(unused_must_use)]
    #[test]
    fn successfully_constructed_create_request_has_valid_ueta_output_in_txos() {
        // Given a valid create request with a UETA compliant payload
        let mut csprng = OsRng::default();
        let mut txn = TestCreateRequestBuilder::default().build(&mut csprng);

        let txo = TransactionOutput::with_compliant_esig_payload(
            Default::default(),
            OneTimeKey::random(&mut csprng),
        );

        // When we construct a create request
        txn.create_request
            .core_transaction
            .outputs
            .push(OpenedTransactionOutput {
                transaction_output: txo,
                blinding_factor: Default::default(),
                value: 100,
            });
        let create_request_txn = txn.create_request;

        // Then its txos have the correct payload
        assert_display_snapshot!(UETA_TEXT);
        create_request_txn
            .core_transaction
            .outputs
            .into_iter()
            .map(|txo| {
                assert!(matches!(
                    txo.transaction_output.esig_payload(),
                    ESigPayload::Ueta(_)
                ));
                assert_eq!(
                    txo.transaction_output.esig_payload().ueta_text(),
                    Some(UETA_TEXT.to_string())
                );
            });
    }
}
