use super::*;
use crate::test::fakes::fake_clear_transaction_data_repository::FakeClearTransactionDataRepository;
use crate::{
    chain_logic::redeems::RedeemFulfillmentHandler,
    crypto::DefaultCryptoImpl,
    test::fakes::{
        FakeCorrelationIdRepository, FakeKeyImageRepo, FakeMemberRepository,
        FakeRedeemRequestRepository, FakeTotalIssuanceRepository, FakeTransactionDataRepository,
        FakeTrustMetadata,
    },
    transactions::{TestExitingRedeemRequestBuildResponse, TransactionValidationError},
    BannedState, ClearTransactionOutput, IdentityTag, KeyImage, PublicKey, TransactionOutput,
    TxoStatus,
};

struct TestContext {
    clear_txo_repo: FakeClearTransactionDataRepository,
    txo_repo: FakeTransactionDataRepository,
    key_image_repo: FakeKeyImageRepo,
    member_repo: FakeMemberRepository,
    trust_metadata: FakeTrustMetadata,
    correlation_id_repo: FakeCorrelationIdRepository,
    redeem_request_repo: FakeRedeemRequestRepository,
    total_issuance_repo: FakeTotalIssuanceRepository,
}

impl TestContext {
    fn new() -> Self {
        Self {
            correlation_id_repo: FakeCorrelationIdRepository::new(),
            clear_txo_repo: FakeClearTransactionDataRepository::new(),
            txo_repo: FakeTransactionDataRepository::new(),
            trust_metadata: FakeTrustMetadata::new(),
            member_repo: FakeMemberRepository::new(),
            key_image_repo: FakeKeyImageRepo::new(),
            redeem_request_repo: FakeRedeemRequestRepository::new(),
            total_issuance_repo: FakeTotalIssuanceRepository::new(),
        }
    }

    fn valid_for(
        self,
        exiting_redeem_request_data: &TestExitingRedeemRequestBuildResponse,
    ) -> Self {
        let txos = get_input_txos_from_redeem_request(&exiting_redeem_request_data.redeem_request);
        let sender = exiting_redeem_request_data
            .redeem_request
            .core_transaction
            .sender;
        self.with_txos(&txos)
            .with_banned(vec![(sender, BannedState::Exiting(0))])
            .with_trust(exiting_redeem_request_data.trust_pub_key)
    }

    fn with_claims_in_network(self, amount: u64) -> Self {
        self.total_issuance_repo
            .add_to_total_cash_confirmations(amount);
        self
    }

    fn with_trust(self, trust: PublicKey) -> Self {
        Self {
            trust_metadata: FakeTrustMetadata::from_trust_key(trust),
            ..self
        }
    }

    fn with_members(self, tags: &[IdentityTag]) -> Self {
        let member_repo = self.member_repo.with_members(tags.into());
        Self {
            member_repo,
            ..self
        }
    }

    fn with_banned(self, banned: Vec<(PublicKey, BannedState<u64>)>) -> Self {
        let member_repo = self.member_repo.with_banned(banned);
        Self {
            member_repo,
            ..self
        }
    }

    fn with_txos(self, txos: &[TransactionOutput]) -> Self {
        Self {
            txo_repo: FakeTransactionDataRepository::seeded(txos, TxoStatus::Confirmed, 0),
            ..self
        }
    }

    fn with_txos_status(self, txos: &[TransactionOutput], status: TxoStatus) -> Self {
        Self {
            txo_repo: FakeTransactionDataRepository::seeded(txos, status, 0),
            ..self
        }
    }

    fn with_txos_block(self, txos: &[TransactionOutput], block: u64) -> Self {
        Self {
            txo_repo: FakeTransactionDataRepository::seeded(txos, TxoStatus::Confirmed, block),
            ..self
        }
    }

    fn with_key_images(self, key_images: &[KeyImage]) -> Self {
        Self {
            key_image_repo: FakeKeyImageRepo::seeded(key_images),
            ..self
        }
    }

    fn exiting_redeem_request_processor(
        &self,
    ) -> ExitingRedeemRequestProcessor<
        impl Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTransactionOutput>)>,
        DefaultCryptoImpl,
        u64,
        FakeClearTransactionDataRepository,
    > {
        ExitingRedeemRequestProcessor {
            clear_txo_repo: &self.clear_txo_repo,
            correlation_id_repo: &self.correlation_id_repo,
            txo_repo: &self.txo_repo,
            trust_metadata: &self.trust_metadata,
            member_repo: &self.member_repo,
            key_image_repo: &self.key_image_repo,
            redeem_request_repo: &self.redeem_request_repo,
            total_issuance_repo: &self.total_issuance_repo,
            _crypto: PhantomData,
        }
    }
}

fn get_input_txos_from_redeem_request(
    transaction: &ExitingRedeemRequestTransaction,
) -> Vec<TransactionOutput> {
    transaction
        .core_transaction
        .input
        .iter()
        .flat_map(|txos| txos.components.clone())
        .collect()
}

mod exiting_redeem {
    use super::*;
    use crate::{
        chain_logic::redeems::tests::TestContext as RedeemTestContext,
        model::RedeemCancellationHandler, transactions::TestExitingRedeemRequestBuilder,
        ConfidentialRedeemRequestRecord, FailureReason, RedeemCancellationReason,
        RedeemCancellationTransaction, RedeemFulfillmentTransaction,
    };
    use rand::rngs::OsRng;
    use std::cell::RefCell;

    #[test]
    fn happy_path() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let ctx = TestContext::new().valid_for(&exiting_redeem_request_response);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(result, Ok(_)),
            "Expected Success but got {:?}",
            result
        );
    }

    // Prevent overwriting other redeem request records with the same correlation id.
    // If a redeem request record was replaced by anothers', the original redeem output would be lost
    // and that amount would not be recoverable on-chain by trust cancellation or fulfillment.
    #[test]
    fn exiting_redeem_request_fails_if_duplicate_correlation_id() {
        let csprng = &mut OsRng::default();

        let exiting_redeem_request = TestExitingRedeemRequestBuilder::default().build(csprng);
        let correlation_id = exiting_redeem_request
            .redeem_request
            .core_transaction
            .correlation_id;

        // alter the redeem request record to be different than the redeem request
        let mut existing_record =
            ConfidentialRedeemRequestRecord::from(exiting_redeem_request.redeem_request.clone());
        existing_record.amount = 0xBEEFu64;
        existing_record.account_data = vec![];
        // setup context using modified redeem request record
        let ctx = TestContext::new().valid_for(&exiting_redeem_request);
        ctx.redeem_request_repo.store_redeem_request(
            correlation_id,
            RedeemRequestRecord::Confidential(Box::new(existing_record.clone())),
        );

        // attempt to process redeem request when record already exists with same correlation id
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request.redeem_request)
            .unwrap_err();

        // Check error type
        assert_eq!(result, FailureReason::RedeemRequestIdAlreadyExists);

        // Ensure redeem request wasn't partially applied
        assert_eq!(
            ctx.redeem_request_repo
                .get_unfulfilled_redeem_request(correlation_id),
            Some(RedeemRequestRecord::Confidential(Box::new(existing_record)))
        );
    }

    // This test ensures that the redeem request doesn't match
    // a previous bank transaction made by the trust
    #[test]
    fn exiting_redeem_request_fails_if_reused_correlation_id() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let correlation_id = exiting_redeem_request_response
            .redeem_request
            .core_transaction
            .correlation_id;
        let ctx = TestContext::new().valid_for(&exiting_redeem_request_response);
        ctx.correlation_id_repo.store_id(correlation_id);

        // Attempt to process redeem request
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request)
            .unwrap_err();

        // Expect reused id failure
        assert_eq!(result, FailureReason::CorrelationIdCannotBeReused);
        // Ensure no state changes are applied from the redeem request
        assert_eq!(
            ctx.redeem_request_repo
                .get_unfulfilled_redeem_request(correlation_id),
            None
        );
        assert_eq!(ctx.correlation_id_repo.all_used_ids(), vec![correlation_id]);
    }

    #[test]
    fn exiting_redeem_request_fails_if_output_txo_already_exist_in_repository() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let mut txos =
            get_input_txos_from_redeem_request(&exiting_redeem_request_response.redeem_request);
        txos.push(
            exiting_redeem_request_response
                .redeem_request
                .core_transaction
                .output
                .redeem_output
                .transaction_output,
        );
        let ctx = TestContext::new()
            .valid_for(&exiting_redeem_request_response)
            .with_txos(&txos);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(result, Err(FailureReason::OutputTxoAlreadyExists)),
            "Expected OutputTxoAlreadyExists but got {:?}",
            result
        );
    }

    #[test]
    fn exiting_redeem_request_fails_if_txo_inputs_do_not_exist_in_repository() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let ctx = TestContext::new()
            .valid_for(&exiting_redeem_request_response)
            .with_txos(&[]);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(result, Err(FailureReason::TxoNotSpendable(_))),
            "Expected TxoNotSpendable but got {:?}",
            result
        );
    }

    #[test]
    fn exiting_redeem_request_fails_if_txo_inputs_exist_but_not_confirmed() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let txos =
            get_input_txos_from_redeem_request(&exiting_redeem_request_response.redeem_request);
        let ctx = TestContext::new()
            .valid_for(&exiting_redeem_request_response)
            .with_txos_status(&txos, TxoStatus::Pending);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(result, Err(FailureReason::TxoNotSpendable(_))),
            "Expected TxoNotSpendable but got {:?}",
            result
        );
    }

    #[test]
    fn exiting_redeem_request_fails_if_key_images_already_exist_in_repository() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let key_image = vec![*exiting_redeem_request_response
            .redeem_request
            .core_transaction
            .key_images
            .get(0)
            .unwrap()];
        let ctx = TestContext::new()
            .valid_for(&exiting_redeem_request_response)
            .with_key_images(&key_image);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(result, Err(FailureReason::KeyImageAlreadySpent)),
            "Expected KeyImageAlreadySpent but got {:?}",
            result
        );
    }

    #[test]
    fn exiting_redeem_request_fails_with_corrupted_signature_proof() {
        let csprng = &mut OsRng::default();
        let mut exiting_redeem_request_response_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        // alter the proof of the redeem request after it was constructed
        exiting_redeem_request_response_response
            .redeem_request
            .pi
            .corrupt_proof();
        let ctx = TestContext::new().valid_for(&exiting_redeem_request_response_response);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response_response.redeem_request);
        assert!(
            matches!(
                result,
                Err(FailureReason::TransactionCouldNotBeVerified(
                    TransactionValidationError::InvalidSignature
                ))
            ),
            "Expected InvalidSignature but got {:?}",
            result
        );
    }

    #[test]
    fn exiting_redeem_request_fails_with_corrupted_alpha_proof() {
        let csprng = &mut OsRng::default();
        let mut exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        // alter the proof of the redeem request after it was constructed
        exiting_redeem_request_response
            .redeem_request
            .core_transaction
            .alpha
            .corrupt_proof();
        let ctx = TestContext::new().valid_for(&exiting_redeem_request_response);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(
                result,
                Err(FailureReason::TransactionCouldNotBeVerified(
                    TransactionValidationError::InvalidAlpha
                ))
            ),
            "Expected InvalidAlpha but got {:?}",
            result
        );
    }

    #[test]
    fn successful_exiting_redeem_request_adds_key_images_to_repository() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let ctx = TestContext::new().valid_for(&exiting_redeem_request_response_response);
        let expected_key_images = exiting_redeem_request_response_response
            .redeem_request
            .core_transaction
            .key_images
            .clone();
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response_response.redeem_request);
        assert!(
            matches!(result, Ok(_)),
            "Expected Success but got {:?}",
            result
        );
        expected_key_images
            .iter()
            .for_each(|image| assert!(ctx.key_image_repo.is_used(image)));
    }

    #[test]
    fn successful_exiting_redeem_request_stores_correlation_id() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let ctx = TestContext::new().valid_for(&exiting_redeem_request_response);
        let expected_record = ConfidentialRedeemRequestRecord::from(
            exiting_redeem_request_response.redeem_request.clone(),
        );
        let expected_id = exiting_redeem_request_response
            .redeem_request
            .core_transaction
            .correlation_id;
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(result, Ok(_)),
            "Expected Success but got {:?}",
            result
        );

        assert!(ctx.correlation_id_repo.is_used(expected_id).is_some());
    }

    #[test]
    fn successful_exiting_redeem_request_stores_an_unfulfilled_redeem_request_record() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let ctx = TestContext::new().valid_for(&exiting_redeem_request_response);
        let expected_record = ConfidentialRedeemRequestRecord::from(
            exiting_redeem_request_response.redeem_request.clone(),
        );
        let expected_id = exiting_redeem_request_response
            .redeem_request
            .core_transaction
            .correlation_id;
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(result, Ok(_)),
            "Expected Success but got {:?}",
            result
        );

        assert_eq!(
            ctx.redeem_request_repo
                .get_unfulfilled_redeem_request(expected_id)
                .unwrap(),
            RedeemRequestRecord::Confidential(Box::new(expected_record))
        );
    }

    #[test]
    fn successful_exiting_redeem_request_reduces_total_claims() {
        let starting_claims = 100;
        let redeem_amount = 100;
        let expected_remaining = 0;
        let input_values = vec![25, 50, 25];
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response = TestExitingRedeemRequestBuilder::default()
            .with_redeem_amount(redeem_amount)
            .with_input_values(input_values)
            .build(csprng);
        let ctx = TestContext::new()
            .valid_for(&exiting_redeem_request_response)
            .with_claims_in_network(starting_claims);
        let expected_record = ConfidentialRedeemRequestRecord::from(
            exiting_redeem_request_response.redeem_request.clone(),
        );
        let expected_id = exiting_redeem_request_response
            .redeem_request
            .core_transaction
            .correlation_id;
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(result, Ok(_)),
            "Expected Success but got {:?}",
            result
        );

        assert_eq!(
            ctx.total_issuance_repo.get_total_claims(),
            expected_remaining
        );
    }

    #[test]
    fn fails_if_not_banned() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let mut ctx = TestContext::new().valid_for(&exiting_redeem_request_response);
        // Remove the banned members
        ctx.member_repo = ctx.member_repo.with_banned(vec![]);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(
                result,
                Err(FailureReason::ExitingRedeemRequestMustBeByExitingMember)
            ),
            "Expected ExitingRedeemRequestMustBeByExitingMember but got {:?}",
            result
        );
    }

    #[test]
    fn fails_if_already_exited() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let mut ctx = TestContext::new().valid_for(&exiting_redeem_request_response);
        // Remove the banned members
        let sender = exiting_redeem_request_response
            .redeem_request
            .core_transaction
            .sender;
        let banned = vec![(sender, BannedState::Exited)];
        ctx.member_repo = ctx.member_repo.with_banned(banned);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(
                result,
                Err(FailureReason::ExitingRedeemRequestMustBeByExitingMember)
            ),
            "Expected ExitingRedeemRequestMustBeByExitingMember but got {:?}",
            result
        );
    }

    #[test]
    fn fails_if_txos_newer_than_banned_date() {
        let csprng = &mut OsRng::default();
        let exiting_redeem_request_response =
            TestExitingRedeemRequestBuilder::default().build(csprng);
        let mut ctx = TestContext::new().valid_for(&exiting_redeem_request_response);
        let old_txos = ctx.txo_repo.transaction_outputs.borrow().clone();
        let new_txos = old_txos
            .into_iter()
            .map(|(txo, (s, _))| (txo, (s, Some(10000))))
            .collect();
        ctx.txo_repo.transaction_outputs = RefCell::new(new_txos);
        let result = ctx
            .exiting_redeem_request_processor()
            .exiting_redeem_request(exiting_redeem_request_response.redeem_request);
        assert!(
            matches!(
                result,
                Err(FailureReason::ExitingRedeemRequestTxONewerThanBan)
            ),
            "Expected ExitingRedeemRequestTxONewerThanBan but got {:?}",
            result
        );
    }

    #[test]
    fn cancelled_exiting_redeem_request_records_are_removed_from_storage() {
        // Given a pending redeem request
        let csprng = &mut OsRng::default();
        let req = TestExitingRedeemRequestBuilder::default()
            .build(csprng)
            .redeem_request;

        let correlation_id = req.core_transaction.correlation_id;
        let record = ConfidentialRedeemRequestRecord::from(req.clone());

        let ctx = RedeemTestContext::new();
        ctx.with_confidential_redeem_request(correlation_id, record);
        let fth = ctx.redeem_request_processor();

        // When the redeem request is cancelled
        let cancel_txn = RedeemCancellationTransaction::new(
            correlation_id,
            RedeemCancellationReason::InvalidData,
        );
        fth.redeem_cancellation(cancel_txn).unwrap();

        // Then the pending redeem request is removed from storage
        assert_eq!(
            fth.redeem_request_repo
                .get_unfulfilled_redeem_request(correlation_id),
            None
        );

        // Then the total redeem request amount is not altered
        assert_eq!(ctx.total_redeem_amount(), 0);

        // Then the redeem output is still spendable by the owner
        assert_eq!(
            ctx.transaction_status(&req.core_transaction.output.redeem_output.transaction_output),
            TxoStatus::Confirmed
        )
    }

    #[test]
    fn fulfilled_exiting_redeem_request_records_are_removed_from_storage() {
        // Given a pending redeem request
        let csprng = &mut OsRng::default();
        let exiting_redeem_request = TestExitingRedeemRequestBuilder::default()
            .build(csprng)
            .redeem_request;

        let correlation_id = exiting_redeem_request.core_transaction.correlation_id;
        let record = ConfidentialRedeemRequestRecord::from(exiting_redeem_request.clone());

        let ctx = RedeemTestContext::new();
        ctx.with_confidential_redeem_request(correlation_id, record);
        let fth = ctx.redeem_request_processor();

        // When the redeem request is fulfilled
        let confirm_txn = RedeemFulfillmentTransaction::new(
            exiting_redeem_request.core_transaction.correlation_id,
        );
        fth.redeem_fulfillment(confirm_txn).unwrap();

        // Then the pending redeem request is removed from storage
        assert_eq!(
            fth.redeem_request_repo
                .get_unfulfilled_redeem_request(correlation_id),
            Option::None
        );

        // Then the redeem output is unusable
        assert_eq!(
            ctx.transaction_status(
                &exiting_redeem_request
                    .core_transaction
                    .output
                    .redeem_output
                    .transaction_output
            ),
            TxoStatus::Nonexistent
        );
    }
}
