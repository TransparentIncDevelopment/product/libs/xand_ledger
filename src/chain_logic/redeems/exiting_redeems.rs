#![allow(clippy::too_many_arguments)]

#[cfg(test)]
mod tests;

use crate::{
    crypto::LedgerCrypto, redeems::RedeemRequestRecord,
    transactions::verify_exiting_redeem_request_transaction, BannedState,
    ClearTransactionOutputRepository, ConfidentialRedeemRequestRecord, CorrelationId,
    CorrelationIdRepository, ExitingRedeemRequestHandler, ExitingRedeemRequestTransaction,
    FailureReason, KeyImageRepository, MemberRepository, RedeemRequestRepository,
    TotalIssuanceRepository, TransactionDataRepository, TransactionOutput, TransactionResult,
    TrustMetadata, TxoStatus,
};
use alloc::boxed::Box;
use core::marker::PhantomData;

#[derive(derive_more::Constructor)]
pub struct ExitingRedeemRequestProcessor<
    'a,
    ExitingRedeemRequestRecordIter,
    C,
    BlockNumber: PartialOrd,
    ClearTxoStore: ClearTransactionOutputRepository,
> {
    clear_txo_repo: &'a ClearTxoStore,
    txo_repo: &'a dyn TransactionDataRepository<BlockNumber>,
    key_image_repo: &'a dyn KeyImageRepository,
    member_repo: &'a dyn MemberRepository<BlockNumber>,
    trust_metadata: &'a dyn TrustMetadata,
    correlation_id_repo: &'a dyn CorrelationIdRepository,
    redeem_request_repo: &'a dyn RedeemRequestRepository<
        Iter = ExitingRedeemRequestRecordIter,
        Utxo = ClearTxoStore::Utxo,
    >,
    total_issuance_repo: &'a dyn TotalIssuanceRepository,
    _crypto: PhantomData<C>,
}

impl<RI, C, BlockNumber, ClearTxoStore> ExitingRedeemRequestHandler
    for ExitingRedeemRequestProcessor<'_, RI, C, BlockNumber, ClearTxoStore>
where
    RI: Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTxoStore::Utxo>)>,
    C: LedgerCrypto,
    BlockNumber: PartialOrd + Copy,
    ClearTxoStore: ClearTransactionOutputRepository,
{
    fn exiting_redeem_request(
        &self,
        transaction: ExitingRedeemRequestTransaction,
    ) -> TransactionResult {
        verify_correlation_id_has_not_been_used(self.correlation_id_repo, &transaction)?;
        verify_does_not_already_exist::<RI, ClearTxoStore>(self.redeem_request_repo, &transaction)?;
        verify_txos_are_new(self.txo_repo, &transaction)?;
        verify_key_images_do_not_already_exist(self.key_image_repo, &transaction)?;
        verify_sender_is_banned(self.member_repo, &transaction)?;
        verify_txos_are_spendable_for_exiting_redeem_request(
            self.member_repo,
            self.txo_repo,
            &transaction,
        )?;
        verify_proofs_are_valid::<C>(&transaction)?;

        //Transaction is now confirmed to be valid. Apply changes
        save_key_images(self.key_image_repo, &transaction);
        save_correlation_id(self.correlation_id_repo, &transaction);
        save_redeem_request_record::<RI, ClearTxoStore>(
            self.redeem_request_repo,
            self.total_issuance_repo,
            transaction,
        );
        Ok(())
    }
}

fn verify_txos_are_spendable_for_exiting_redeem_request<BlockNumber: PartialOrd + Copy>(
    member_repo: &dyn MemberRepository<BlockNumber>,
    txo_repo: &dyn TransactionDataRepository<BlockNumber>,
    txn: &ExitingRedeemRequestTransaction,
) -> TransactionResult {
    let banned_block = member_repo
        .get_banned_members()
        .iter()
        .find(|(member, state)| member == &txn.core_transaction.sender)
        .and_then(|(_, state)| match state {
            BannedState::Exiting(block) => Some(*block),
            BannedState::Exited => None,
        })
        .ok_or(FailureReason::ExitingRedeemRequestMustBeByExitingMember)?;
    txn.core_transaction
        .input
        .iter()
        .flat_map(|set| &set.components)
        .try_for_each(|txo| {
            verify_is_spendable_and_valid_exiting_redeem_request_txo(txo_repo, txo, banned_block)
        })
}

fn verify_is_spendable_and_valid_exiting_redeem_request_txo<BlockNumber: PartialOrd>(
    txo_repo: &dyn TransactionDataRepository<BlockNumber>,
    txo: &TransactionOutput,
    banned_block: BlockNumber,
) -> TransactionResult {
    let status = txo_repo.lookup_status(txo);
    match status {
        (TxoStatus::Confirmed, Some(block)) => {
            if block <= banned_block {
                Ok(())
            } else {
                Err(FailureReason::ExitingRedeemRequestTxONewerThanBan)
            }
        }
        _ => Err(FailureReason::TxoNotSpendable(*txo)),
    }
}

fn verify_sender_is_banned<BlockNumber>(
    repo: &dyn MemberRepository<BlockNumber>,
    txn: &ExitingRedeemRequestTransaction,
) -> TransactionResult {
    repo.get_banned_members()
        .iter()
        .any(|(member, state)| {
            member == &txn.core_transaction.sender && matches!(state, BannedState::Exiting(_))
        })
        .then_some(())
        .ok_or(FailureReason::ExitingRedeemRequestMustBeByExitingMember)
}

fn verify_correlation_id_has_not_been_used(
    repo: &dyn CorrelationIdRepository,
    txn: &ExitingRedeemRequestTransaction,
) -> TransactionResult {
    if repo.is_used(txn.core_transaction.correlation_id).is_some() {
        return Err(FailureReason::CorrelationIdCannotBeReused);
    }
    Ok(())
}

fn verify_does_not_already_exist<RI, ClearTxoStore: ClearTransactionOutputRepository>(
    repo: &dyn RedeemRequestRepository<Iter = RI, Utxo = ClearTxoStore::Utxo>,
    txn: &ExitingRedeemRequestTransaction,
) -> TransactionResult
where
    RI: Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTxoStore::Utxo>)>,
{
    if repo
        .get_unfulfilled_redeem_request(txn.core_transaction.correlation_id)
        .is_some()
    {
        return Err(FailureReason::RedeemRequestIdAlreadyExists);
    }
    Ok(())
}

fn verify_txos_are_new<BlockNumber: PartialOrd>(
    txo_repo: &dyn TransactionDataRepository<BlockNumber>,
    transaction: &ExitingRedeemRequestTransaction,
) -> TransactionResult {
    if txo_repo
        .lookup_status(
            &transaction
                .core_transaction
                .output
                .redeem_output
                .transaction_output,
        )
        .0
        == TxoStatus::Nonexistent
    {
        Ok(())
    } else {
        Err(FailureReason::OutputTxoAlreadyExists)
    }
}

fn verify_key_images_do_not_already_exist(
    key_image_repo: &dyn KeyImageRepository,
    transaction: &ExitingRedeemRequestTransaction,
) -> TransactionResult {
    let new_key_images = &transaction.core_transaction.key_images;
    new_key_images
        .iter()
        .all(|image| !key_image_repo.is_used(image))
        .then_some(())
        .ok_or(FailureReason::KeyImageAlreadySpent)
}

fn verify_proofs_are_valid<T: LedgerCrypto>(
    txn: &ExitingRedeemRequestTransaction,
) -> Result<(), FailureReason> {
    verify_exiting_redeem_request_transaction::<T>(txn)?;
    Ok(())
}

fn save_key_images(
    image_repository: &dyn KeyImageRepository,
    transaction: &ExitingRedeemRequestTransaction,
) {
    transaction
        .core_transaction
        .key_images
        .iter()
        .for_each(|image| image_repository.insert_key_image(*image))
}

fn save_correlation_id(
    correlation_id_repo: &dyn CorrelationIdRepository,
    transaction: &ExitingRedeemRequestTransaction,
) {
    correlation_id_repo.store_id(transaction.core_transaction.correlation_id);
}

fn save_redeem_request_record<RI, ClearTxoStore: ClearTransactionOutputRepository>(
    exiting_redeem_request_repo: &dyn RedeemRequestRepository<
        Iter = RI,
        Utxo = ClearTxoStore::Utxo,
    >,
    total_issuance_repo: &dyn TotalIssuanceRepository,
    txn: ExitingRedeemRequestTransaction,
) where
    RI: Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTxoStore::Utxo>)>,
{
    total_issuance_repo.add_to_total_redeemed(txn.core_transaction.output.redeem_output.value);
    exiting_redeem_request_repo.store_redeem_request(
        txn.core_transaction.correlation_id,
        RedeemRequestRecord::Confidential(Box::new(ConfidentialRedeemRequestRecord::from(txn))),
    );
}
