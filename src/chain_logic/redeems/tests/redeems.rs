use crate::{
    chain_logic::redeems::tests::{
        get_identity_tags_from_redeem_request, get_input_txos_from_redeem_request, TestContext,
    },
    errors::FailureReason,
    transactions::{TestRedeemRequestBuilder, TransactionValidationError},
    ConfidentialRedeemRequestRecord, CorrelationIdRepository, IdentityTag, IdentityTagRepository,
    KeyImageRepository, RedeemRequestHandler, RedeemRequestRecord, RedeemRequestRepository,
    TotalIssuanceRepository, TxoStatus,
};
use curve25519_dalek::scalar::Scalar;
use rand::rngs::OsRng;

#[test]
fn happy_path() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Ok(_)),
        "Expected Success but got {:?}",
        result
    );
}

// Prevent overwriting other redeem request records with the same correlation id.
// If a redeem record was replaced by anothers', the original redeem output would be lost
// and that amount would not be recoverable on-chain by trust cancellation or fulfillment.
#[test]
fn redeem_request_fails_if_duplicate_correlation_id() {
    let csprng = &mut OsRng::default();

    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let correlation_id = redeem_request_response
        .redeem_request
        .core_transaction
        .correlation_id;

    // alter the redeem request record to be different than the redeem request
    let mut existing_record =
        ConfidentialRedeemRequestRecord::from(redeem_request_response.redeem_request.clone());
    existing_record.amount = 0xBEEFu64;
    existing_record.account_data = vec![];
    // setup context using modified redeem request record
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    ctx.redeem_request_repo.store_redeem_request(
        correlation_id,
        RedeemRequestRecord::Confidential(Box::new(existing_record.clone())),
    );

    // attempt to process redeem request when record already exists with same correlation id
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request.clone())
        .unwrap_err();

    // Check error type
    assert_eq!(result, FailureReason::RedeemRequestIdAlreadyExists);

    // Ensure redeem request wasn't partially applied
    assert_eq!(
        ctx.redeem_request_repo
            .get_unfulfilled_redeem_request(correlation_id),
        Some(RedeemRequestRecord::Confidential(Box::new(existing_record)))
    );
    assert!(!ctx.identity_tag_repo.exists(
        &redeem_request_response
            .redeem_request
            .core_transaction
            .output_identity
    ));
    assert_eq!(
        ctx.transaction_status(
            &redeem_request_response
                .redeem_request
                .core_transaction
                .output
                .change_output
                .txo
        ),
        TxoStatus::Nonexistent
    );
}

// This test ensures that the redeem request doesn't match
// a previous bank transaction made by the trust
#[test]
fn redeem_request_fails_if_reused_correlation_id() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let correlation_id = redeem_request_response
        .redeem_request
        .core_transaction
        .correlation_id;
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    ctx.correlation_id_repo.store_id(correlation_id);

    // Attempt to process redeem request
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request.clone())
        .unwrap_err();

    // Expect reused id failure
    assert_eq!(result, FailureReason::CorrelationIdCannotBeReused);
    // Ensure no state changes are applied from the redeem request
    assert_eq!(
        ctx.redeem_request_repo
            .get_unfulfilled_redeem_request(correlation_id),
        None
    );
    assert_eq!(ctx.correlation_id_repo.all_used_ids(), vec![correlation_id]);
    assert!(!ctx.identity_tag_repo.exists(
        &redeem_request_response
            .redeem_request
            .core_transaction
            .output_identity
    ));
    assert_eq!(
        ctx.transaction_status(
            &redeem_request_response
                .redeem_request
                .core_transaction
                .output
                .change_output
                .txo
        ),
        TxoStatus::Nonexistent
    );
}

#[test]
fn redeem_request_fails_with_invalid_member_address() {
    let csprng = &mut OsRng::default();
    let invalid_member = Scalar::random(csprng);
    let identity_source = IdentityTag::from_key_with_generator_base(invalid_member.into());
    let redeem_request_response = TestRedeemRequestBuilder::default()
        .with_identity_source(identity_source)
        .build(csprng);
    let redeem_request = redeem_request_response.redeem_request;
    let txos = get_input_txos_from_redeem_request(&redeem_request);
    let identity_tags: Vec<IdentityTag> = get_identity_tags_from_redeem_request(&redeem_request)
        .into_iter()
        .filter(|id| id != &identity_source)
        .collect();

    let ctx = TestContext::new()
        .with_identity_tags(&identity_tags)
        .with_txos(txos.as_slice())
        .with_trust(redeem_request_response.trust_pub_key);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request);
    assert!(
        matches!(result, Err(FailureReason::UnknownIdentity)),
        "Expected UnknownIdentity but got {:?}",
        result
    );
}

#[test]
fn redeem_request_succeeds_with_valid_member_address_as_true_source() {
    let csprng = &mut OsRng::default();
    let private_key = Scalar::random(csprng);
    let identity_source = IdentityTag::from_key_with_generator_base(private_key.into());
    let redeem_request_response = TestRedeemRequestBuilder::default()
        .with_identity_source(identity_source)
        .with_private_key(private_key)
        .build(csprng);
    let redeem_request = redeem_request_response.redeem_request;
    let txos = get_input_txos_from_redeem_request(&redeem_request);
    let identity_tags: Vec<IdentityTag> = get_identity_tags_from_redeem_request(&redeem_request)
        .into_iter()
        .filter(|id| id != &identity_source)
        .collect();

    let ctx = TestContext::new()
        .with_identity_tags(&identity_tags)
        .with_members(&vec![identity_source])
        .with_txos(txos.as_slice())
        .with_trust(redeem_request_response.trust_pub_key);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request);
    assert!(
        matches!(result, Ok(_)),
        "Expected success but got error {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_if_identity_tags_do_not_exist_in_repository() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let ctx = TestContext::new()
        .valid_for_confidential_redeem(&redeem_request_response)
        .with_identity_tags(&[]);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Err(FailureReason::UnknownIdentity)),
        "Expected UnknownIdentity but got {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_if_txo_inputs_do_not_exist_in_repository() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let ctx = TestContext::new()
        .valid_for_confidential_redeem(&redeem_request_response)
        .with_txos(&[]);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Err(FailureReason::TxoNotSpendable(_))),
        "Expected TxoNotSpendable but got {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_if_txo_inputs_exist_but_not_confirmed() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let txos = get_input_txos_from_redeem_request(&redeem_request_response.redeem_request);
    let ctx = TestContext::new()
        .valid_for_confidential_redeem(&redeem_request_response)
        .with_txos_status(&txos, TxoStatus::Pending);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Err(FailureReason::TxoNotSpendable(_))),
        "Expected TxoNotSpendable but got {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_if_key_images_already_exist_in_repository() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let key_image = vec![*redeem_request_response
        .redeem_request
        .core_transaction
        .key_images
        .get(0)
        .unwrap()];
    let ctx = TestContext::new()
        .valid_for_confidential_redeem(&redeem_request_response)
        .with_key_images(&key_image);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Err(FailureReason::KeyImageAlreadySpent)),
        "Expected KeyImageAlreadySpent but got {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_if_change_output_txo_already_exist_in_repository() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let mut txos = get_input_txos_from_redeem_request(&redeem_request_response.redeem_request);
    txos.push(
        redeem_request_response
            .redeem_request
            .core_transaction
            .output
            .change_output
            .txo,
    );
    let ctx = TestContext::new()
        .valid_for_confidential_redeem(&redeem_request_response)
        .with_txos(&txos);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Err(FailureReason::OutputTxoAlreadyExists)),
        "Expected OutputTxoAlreadyExists but got {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_if_redeem_output_txo_already_exist_in_repository() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let mut txos = get_input_txos_from_redeem_request(&redeem_request_response.redeem_request);
    txos.push(
        redeem_request_response
            .redeem_request
            .core_transaction
            .output
            .redeem_output
            .transaction_output,
    );
    let ctx = TestContext::new()
        .valid_for_confidential_redeem(&redeem_request_response)
        .with_txos(&txos);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Err(FailureReason::OutputTxoAlreadyExists)),
        "Expected OutputTxoAlreadyExists but got {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_with_corrupted_signature_proof() {
    let csprng = &mut OsRng::default();
    let mut redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    // alter the proof of the redeem request after it was constructed
    redeem_request_response.redeem_request.pi.corrupt_proof();
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(
                TransactionValidationError::InvalidSignature
            ))
        ),
        "Expected InvalidSignature but got {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_with_corrupted_bullet_proof() {
    let csprng = &mut OsRng::default();
    let mut redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    // alter the proof of the redeem request after it was constructed
    redeem_request_response
        .redeem_request
        .core_transaction
        .range_proof
        .corrupt_proof();
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(
                TransactionValidationError::InvalidBulletProof
            ))
        ),
        "Expected InvalidSignature but got {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_with_corrupted_alpha_proof() {
    let csprng = &mut OsRng::default();
    let mut redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    // alter the proof of the redeem request after it was constructed
    redeem_request_response
        .redeem_request
        .core_transaction
        .alpha
        .corrupt_proof();
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(
                TransactionValidationError::InvalidAlpha
            ))
        ),
        "Expected InvalidSignature but got {:?}",
        result
    );
}

#[test]
fn redeem_request_fails_with_corrupted_encrypted_sender() {
    let csprng = &mut OsRng::default();
    let mut redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    // alter the proof of the redeem request after it was constructed
    redeem_request_response
        .redeem_request
        .core_transaction
        .encrypted_sender
        .corrupt_proof();
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(
                TransactionValidationError::InvalidEncryption(_)
            ))
        ),
        "Expected InvalidSignature but got {:?}",
        result
    );
}

#[test]
fn successful_redeem_request_adds_identity_output_to_repository() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    let expected_tag = redeem_request_response
        .redeem_request
        .core_transaction
        .output_identity;
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Ok(_)),
        "Expected Success but got {:?}",
        result
    );
    assert!(ctx.identity_tag_repo.exists(&expected_tag));
}

#[test]
fn successful_redeem_request_adds_key_images_to_repository() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    let expected_key_images = redeem_request_response
        .redeem_request
        .core_transaction
        .key_images
        .clone();
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Ok(_)),
        "Expected Success but got {:?}",
        result
    );
    expected_key_images
        .iter()
        .for_each(|image| assert!(ctx.key_image_repo.is_used(image)));
}

#[test]
fn successful_redeem_request_adds_change_output_as_confirmed_to_repository() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    let expected_txo = redeem_request_response
        .redeem_request
        .core_transaction
        .output
        .change_output
        .txo;
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Ok(_)),
        "Expected Success but got {:?}",
        result
    );
    assert_eq!(ctx.transaction_status(&expected_txo), TxoStatus::Confirmed);
}

#[test]
fn successful_redeem_request_stores_an_unfulfilled_redeem_request() {
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default().build(csprng);
    let ctx = TestContext::new().valid_for_confidential_redeem(&redeem_request_response);
    let expected_record =
        ConfidentialRedeemRequestRecord::from(redeem_request_response.redeem_request.clone());
    let expected_id = redeem_request_response
        .redeem_request
        .core_transaction
        .correlation_id;
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Ok(_)),
        "Expected Success but got {:?}",
        result
    );

    assert_eq!(
        ctx.redeem_request_repo
            .get_unfulfilled_redeem_request(expected_id)
            .unwrap(),
        RedeemRequestRecord::Confidential(Box::new(expected_record))
    );
}

#[test]
fn successful_redeem_request_reduces_total_claims() {
    let starting_claims = 100;
    let redeem_amount = 10;
    let expected_remaining = 90;
    let csprng = &mut OsRng::default();
    let redeem_request_response = TestRedeemRequestBuilder::default()
        .with_redeem_amount(redeem_amount)
        .build(csprng);
    let ctx = TestContext::new()
        .valid_for_confidential_redeem(&redeem_request_response)
        .with_claims_in_network(starting_claims);
    let expected_record =
        ConfidentialRedeemRequestRecord::from(redeem_request_response.redeem_request.clone());
    let expected_id = redeem_request_response
        .redeem_request
        .core_transaction
        .correlation_id;
    let result = ctx
        .redeem_request_processor()
        .redeem_request(redeem_request_response.redeem_request);
    assert!(
        matches!(result, Ok(_)),
        "Expected Success but got {:?}",
        result
    );

    assert_eq!(
        ctx.total_issuance_repo.get_total_claims(),
        expected_remaining
    );
}

mod redeem_fulfillment {
    use crate::{
        chain_logic::redeems::tests::TestContext, errors::FailureReason,
        transactions::TestRedeemRequestBuilder, ConfidentialRedeemRequestRecord, CorrelationId,
        RedeemFulfillmentHandler, RedeemFulfillmentTransaction, RedeemRequestRecord,
        RedeemRequestRepository, TxoStatus,
    };
    use rand::rngs::OsRng;

    #[test]
    fn happy_path() {
        let csprng = OsRng::default();

        // Make an imaginary redeem request
        let req = TestRedeemRequestBuilder::default()
            .build(csprng)
            .redeem_request;

        let correlation_id = req.core_transaction.correlation_id;
        let as_record = ConfidentialRedeemRequestRecord::from(req.clone());

        let ctx = TestContext::new();
        ctx.redeem_request_repo.store_redeem_request(
            correlation_id,
            RedeemRequestRecord::Confidential(Box::new(as_record)),
        );
        let fth = ctx.redeem_request_processor();

        // Issue a confirmation for the redeem
        let confirm_txn = RedeemFulfillmentTransaction::new(req.core_transaction.correlation_id);
        fth.redeem_fulfillment(confirm_txn).unwrap();

        assert_eq!(
            ctx.redeem_request_repo
                .get_unfulfilled_redeem_request(correlation_id),
            Option::None
        );

        // ensure redeem output is unusable
        assert_eq!(
            ctx.transaction_status(&req.core_transaction.output.redeem_output.transaction_output),
            TxoStatus::Nonexistent
        );
    }

    #[test]
    fn cant_fulfill_non_existent_redeem() {
        let ctx = TestContext::new();
        let fth = ctx.redeem_request_processor();
        // Issue a confirmation for non-existant redeem
        let confirm_txn = RedeemFulfillmentTransaction::new(CorrelationId::default());
        let result = fth.redeem_fulfillment(confirm_txn);
        assert!(
            matches!(result, Err(FailureReason::NoMatchingRedeemRequestToFulfill)),
            "Expected NoMatchingRedeemRequestToFulfill, got {:?}",
            result
        )
    }
}

#[cfg(test)]
mod redeem_cancellation {
    use crate::{
        chain_logic::redeems::tests::TestContext, errors::FailureReason,
        transactions::TestRedeemRequestBuilder, ConfidentialRedeemRequestRecord, CorrelationId,
        RedeemCancellationHandler, RedeemCancellationReason, RedeemCancellationTransaction,
        RedeemRequestRecord, RedeemRequestRepository, TotalIssuanceRepository, TxoStatus,
    };
    use rand::rngs::OsRng;

    #[test]
    fn happy_path() {
        let csprng = OsRng::default();

        // Make an imaginary redeem request
        let req = TestRedeemRequestBuilder::default()
            .build(csprng)
            .redeem_request;

        let correlation_id = req.core_transaction.correlation_id;
        let as_record = ConfidentialRedeemRequestRecord::from(req.clone());

        let ctx = TestContext::new();
        ctx.redeem_request_repo.store_redeem_request(
            correlation_id,
            RedeemRequestRecord::Confidential(Box::new(as_record)),
        );
        let fth = ctx.redeem_request_processor();

        // Issue a cancellation for the redeem
        let cancel_txn = RedeemCancellationTransaction::new(
            correlation_id,
            RedeemCancellationReason::InvalidData,
        );
        fth.redeem_cancellation(cancel_txn).unwrap();

        // Assert that the redeem request record is removed from storage
        assert_eq!(
            fth.redeem_request_repo
                .get_unfulfilled_redeem_request(correlation_id),
            None
        );

        // Assert total redeem amount is not altered
        assert_eq!(ctx.total_issuance_repo.get_total_redeemed(), 0);

        // Assert that the redeem output is spendable by the owner
        assert_eq!(
            ctx.transaction_status(&req.core_transaction.output.redeem_output.transaction_output),
            TxoStatus::Confirmed
        )
    }

    #[test]
    fn cant_cancel_nonexistent_redeem() {
        let ctx = TestContext::new();
        let fth = ctx.redeem_request_processor();
        // Issue a confirmation for non-existent redeem
        let cancel_tx = RedeemCancellationTransaction::new(
            CorrelationId::default(),
            RedeemCancellationReason::InvalidData,
        );
        let result = fth.redeem_cancellation(cancel_tx);
        assert!(
            matches!(result, Err(FailureReason::NoMatchingRedeemRequestToCancel)),
            "Expected NoMatchingRedeemRequestToCancel, got {:?}",
            result
        )
    }

    #[test]
    fn cancellation_reduces_redeemed_claims() {
        let starting_amount = 100;
        let redeem_amount = 10;
        let expected_amount = 100;
        let csprng = OsRng::default();

        // Make an imaginary redeem request
        let req = TestRedeemRequestBuilder::default()
            .with_redeem_amount(redeem_amount)
            .build(csprng)
            .redeem_request;

        let correlation_id = req.core_transaction.correlation_id;
        let as_record = ConfidentialRedeemRequestRecord::from(req);

        let ctx = TestContext::new().with_claims_in_network(starting_amount);
        ctx.redeem_request_repo.store_redeem_request(
            correlation_id,
            RedeemRequestRecord::Confidential(Box::new(as_record)),
        );
        let fth = ctx.redeem_request_processor();

        // Issue a cancellation for the redeem
        let cancel_txn = RedeemCancellationTransaction::new(
            correlation_id,
            RedeemCancellationReason::InvalidData,
        );
        fth.redeem_cancellation(cancel_txn).unwrap();

        // Assert total claims is restored to the original amount
        assert_eq!(ctx.total_issuance_repo.get_total_claims(), expected_amount);
    }
}

mod esig_payload {
    use crate::{
        esig_payload::{ESigPayload, UETA_TEXT},
        transactions::TestRedeemRequestBuilder,
        OneTimeKey, OpenedTransactionOutput, TransactionOutput,
    };
    use insta::assert_display_snapshot;
    use rand::rngs::OsRng;

    #[test]
    fn successfully_constructed_redeem_request_has_valid_ueta_output_in_txos() {
        // Given a valid redeem with a UETA compliant payload
        let mut csprng = OsRng::default();
        let mut txn = TestRedeemRequestBuilder::default().build(csprng);

        let txo = TransactionOutput::with_compliant_esig_payload(
            Default::default(),
            OneTimeKey::random(&mut csprng),
        );

        // When we construct a redeem
        txn.redeem_request.core_transaction.output.redeem_output = OpenedTransactionOutput {
            transaction_output: txo,
            blinding_factor: Default::default(),
            value: 100,
        };

        let redeem_request_transaction = txn.redeem_request;

        // Then its txos have the correct payload
        assert_display_snapshot!(UETA_TEXT);
        let redeem_txo = redeem_request_transaction
            .core_transaction
            .output
            .redeem_output
            .transaction_output;

        assert!(matches!(redeem_txo.esig_payload(), ESigPayload::Ueta(_)));
        assert_eq!(
            redeem_txo.esig_payload().ueta_text(),
            Some(UETA_TEXT.to_string())
        );
    }
}
