#![allow(non_snake_case)]

use crate::{
    chain_logic::redeems::tests::{get_input_txos_from_clear_redeem_request, TestContext},
    esig_payload::ESigPayload,
    transactions::clear_redeem::TestClearRedeemRequestBuilder,
    ClearRedeemOutput, ClearRedeemRequestHandler, ClearRedeemRequestRecord,
    ClearRedeemRequestTransaction, ClearTransactionOutput, ClearTransactionOutputRepository,
    CorrelationId, CorrelationIdRepository, FailureReason, MonetaryValue, PublicKey,
    RedeemRequestRecord, RedeemRequestRepository, TotalIssuanceRepository,
};
use assert_matches::assert_matches;
use rand::{rngs::OsRng, Rng};
use std::convert::TryFrom;

#[test]
fn clear_redeem_request__succeeds_the_happy_path() {
    // Given a clear redeem request
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .build(csprng)
        .unwrap();
    let correlation_id = clear_redeem_request.correlation_id;
    // And a test context that supports the transaction
    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request);

    // When a redeem is requested
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request.clone());

    // Then it was successful
    assert!(result.is_ok());
    // And the redeem record is added to storage
    assert_eq!(
        ctx.redeem_request_repo
            .get_unfulfilled_redeem_request(correlation_id),
        Some(RedeemRequestRecord::Clear(clear_redeem_request.into()))
    );
}

#[test]
fn clear_redeem_request__errors_when_same_transaction_is_submitted_twice() {
    // Given a redeem request
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .build(csprng)
        .unwrap();
    let correlation_id = clear_redeem_request.correlation_id;
    // And a test context that supports the transaction
    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request);

    // When a redeem is requested twice
    let result_1 = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request.clone());
    let result_2 = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request.clone());

    // Then the first was successful
    result_1.unwrap();
    // And the second throws an error
    assert_matches!(result_2, Err(FailureReason::RedeemRequestIdAlreadyExists));
    // And only one transaction was recorded
    assert_eq!(
        ctx.redeem_request_repo
            .all_unfulfilled_redeem_requests()
            .len(),
        1
    );
    assert_eq!(
        ctx.redeem_request_repo
            .get_unfulfilled_redeem_request(correlation_id),
        Some(RedeemRequestRecord::Clear(clear_redeem_request.into()))
    );
}

// Prevent overwriting other redeem request records with the same correlation id.
// If a redeem request record was replaced by anothers', the original redeem output would be lost
// and that amount would not be recoverable on-chain by trust cancellation or fulfillment.
#[test]
fn clear_redeem_request__errors_if_duplicate_correlation_id() {
    // Given a redeem request
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .build(csprng)
        .unwrap();
    let correlation_id = clear_redeem_request.correlation_id;

    // And a redeem request record to be different than the redeem request with the same correlation id
    let mut existing_record = ClearRedeemRequestRecord::from(clear_redeem_request.clone());
    existing_record.amount = MonetaryValue::try_from(100u64).unwrap();
    existing_record.account_data = vec![];

    // And a test context using the modified redeem request record
    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request);
    ctx.redeem_request_repo.store_redeem_request(
        correlation_id,
        RedeemRequestRecord::Clear(existing_record.clone()),
    );

    // When processing a redeem request record with the same correlation id is attempted
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request.clone());

    // Then an error is returned
    assert_matches!(result, Err(FailureReason::RedeemRequestIdAlreadyExists));

    // And the redeem request wasn't partially applied
    assert_eq!(
        ctx.redeem_request_repo
            .get_unfulfilled_redeem_request(correlation_id),
        Some(RedeemRequestRecord::Clear(existing_record))
    );
    assert!(!ctx
        .clear_txo_repo
        .contains(&clear_redeem_request.output.change_output.unwrap()));
}

// This test ensures that the redeem request doesn't match
// a previous bank transaction made by the trust
#[test]
fn clear_redeem_request_fails_if_reused_correlation_id() {
    // Given a redeem request
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .build(csprng)
        .unwrap();
    let correlation_id = clear_redeem_request.correlation_id;
    // And a test context that already has the correlation id in storage
    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request);
    ctx.correlation_id_repo.store_id(correlation_id);

    // When the clear redeem request is requested to be processed
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request.clone());

    // Then a reused id error is returned
    assert_matches!(result, Err(FailureReason::CorrelationIdCannotBeReused));
    // And no state changes are applied from the clear redeem request
    assert_eq!(
        ctx.redeem_request_repo
            .get_unfulfilled_redeem_request(correlation_id),
        None
    );
    assert_eq!(ctx.correlation_id_repo.all_used_ids(), vec![correlation_id]);
    assert!(!ctx
        .clear_txo_repo
        .contains(&clear_redeem_request.output.change_output.unwrap()));
}

#[test]
fn clear_redeem_request__errors_with_invalid_validator_address() {
    // Given a redeem request issued by a public key that is not associated with a validator
    let csprng = &mut OsRng::default();
    let invalid_validator = PublicKey::from("invalidator");
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .with_issuer(invalid_validator)
        .build(csprng)
        .unwrap();
    let txos = get_input_txos_from_clear_redeem_request(&clear_redeem_request);
    // And a corresponding test context
    let ctx = TestContext::new().with_clear_txos(txos.as_slice());

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then
    assert_matches!(result, Err(FailureReason::UnknownIdentity));
}

#[test]
fn clear_redeem_request__succeeds_with_valid_validator_address_as_true_source() {
    // Given a redeem request issued by a public key that is associated with a validator
    let csprng = &mut OsRng::default();
    let public_key = PublicKey::from("VALIDator");
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .with_issuer(public_key)
        .build(csprng)
        .unwrap();
    let txos = get_input_txos_from_clear_redeem_request(&clear_redeem_request);
    // And a corresponding test context
    let ctx = TestContext::new()
        .with_validators(&[public_key])
        .with_clear_txos(txos.as_slice());

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then
    assert!(result.is_ok());
}

#[test]
fn clear_redeem_request__errors_if_clear_txo_inputs_do_not_exist_in_repository() {
    // Given a redeem request
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .build(csprng)
        .unwrap();
    // And a test context that doesn't have the necessary clear txos in storage
    let ctx = TestContext::new()
        .valid_for_clear_redeem(&clear_redeem_request)
        .with_clear_txos(&[]);

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then
    assert_matches!(result, Err(FailureReason::ClearTxoNotRedeemable));
}

#[test]
fn clear_redeem_request__errors_if_change_output_clear_txo_already_exist_in_repository() {
    // Given a redeem request
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .build(csprng)
        .unwrap();
    // And a test context that already has the change output txo in storage
    let mut txos = get_input_txos_from_clear_redeem_request(&clear_redeem_request);
    txos.push(clear_redeem_request.output.change_output.unwrap());
    let ctx = TestContext::new()
        .valid_for_clear_redeem(&clear_redeem_request)
        .with_clear_txos(&txos);

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then
    assert_matches!(result, Err(FailureReason::OutputTxoAlreadyExists));
}

#[test]
fn clear_redeem_request__errors_if_redeem_output_clear_txo_already_exist_in_repository() {
    // Given a redeem request
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .build(csprng)
        .unwrap();
    // And a test context that already has the redeem output txo in storage
    let mut txos = get_input_txos_from_clear_redeem_request(&clear_redeem_request);
    txos.push(clear_redeem_request.output.redeem_output);
    let ctx = TestContext::new()
        .valid_for_clear_redeem(&clear_redeem_request)
        .with_clear_txos(&txos);

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then
    assert_matches!(result, Err(FailureReason::OutputTxoAlreadyExists));
}

#[test]
fn clear_redeem_request__adds_change_output_as_confirmed_to_repository_when_successful() {
    // Given a redeem request and a valid test context
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .build(csprng)
        .unwrap();
    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request);
    let expected_txo = clear_redeem_request.output.change_output;

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then the redeem request succeeds
    assert!(result.is_ok());
    // And the change output was saved to storage
    assert!(ctx.clear_txo_repo.contains(&expected_txo.unwrap()));
}

#[test]
fn clear_redeem_request__stores_an_unfulfilled_redeem_request_when_successful() {
    // Given a redeem request and a valid test context
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .build(csprng)
        .unwrap();
    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request);
    let expected_record = ClearRedeemRequestRecord::from(clear_redeem_request.clone());
    let expected_id = clear_redeem_request.correlation_id;

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then the redeem request succeeds
    assert!(result.is_ok());
    // And the redeem request record was saved to storage
    assert_eq!(
        ctx.redeem_request_repo
            .get_unfulfilled_redeem_request(expected_id)
            .unwrap(),
        RedeemRequestRecord::Clear(expected_record)
    );
}

#[test]
fn clear_redeem_request__reduces_total_claims_when_successful() {
    // Given a redeem request and a test context with a given number of issued claims
    let starting_claims = 100;
    let redeem_amount = 10;
    let expected_remaining = 90;
    let csprng = &mut OsRng::default();
    let clear_redeem_request = TestClearRedeemRequestBuilder::default()
        .with_redeem_amount(redeem_amount)
        .build(csprng)
        .unwrap();
    let ctx = TestContext::new()
        .valid_for_clear_redeem(&clear_redeem_request)
        .with_claims_in_network(starting_claims);
    let expected_record = ClearRedeemRequestRecord::from(clear_redeem_request.clone());
    let expected_id = clear_redeem_request.correlation_id;

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then the redeem request succeeds
    assert!(result.is_ok());
    // And the claims has been adjusted accordingly
    assert_eq!(
        ctx.total_issuance_repo.get_total_claims(),
        expected_remaining
    );
}

#[test]
fn clear_redeem_request__errors_when_clear_redeem_request_is_constructed_with_unowned_txos() {
    // Given a maliciously constructed redeem request with different owners between inputs and outputs
    let csprng = &mut OsRng::default();
    let public_key_1 = PublicKey::from("1");
    let public_key_2 = PublicKey::from("2");
    let clear_redeem_request = ClearRedeemRequestTransaction {
        input: vec![ClearTransactionOutput::new(
            public_key_1,
            MonetaryValue::try_from(100u64).unwrap(),
            0,
            ESigPayload::new_with_ueta(),
        )],
        output: ClearRedeemOutput {
            redeem_output: ClearTransactionOutput::new(
                public_key_2,
                MonetaryValue::try_from(90u64).unwrap(),
                1,
                ESigPayload::new_with_ueta(),
            ),
            change_output: Some(ClearTransactionOutput::new(
                public_key_2,
                MonetaryValue::try_from(10u64).unwrap(),
                2,
                ESigPayload::new_with_ueta(),
            )),
        },
        correlation_id: CorrelationId::default(),
        extra: vec![],
    };
    let correlation_id = clear_redeem_request.correlation_id;
    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request);

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then
    assert_matches!(
        result,
        Err(FailureReason::ClearTxoDoesNotBelongToRedeemingValidator)
    );
}

#[test]
fn clear_redeem_request__errors_when_clear_redeem_request_is_constructed_with_input_less_than_output(
) {
    // Given a maliciously constructed redeem request with inputs that are less than the outputs
    let csprng = &mut OsRng::default();
    let public_key = PublicKey::from("public key");
    let clear_redeem_request = ClearRedeemRequestTransaction {
        input: vec![ClearTransactionOutput::new(
            public_key,
            MonetaryValue::try_from(50u64).unwrap(),
            0,
            ESigPayload::new_with_ueta(),
        )],
        output: ClearRedeemOutput {
            redeem_output: ClearTransactionOutput::new(
                public_key,
                MonetaryValue::try_from(90u64).unwrap(),
                1,
                ESigPayload::new_with_ueta(),
            ),
            change_output: Some(ClearTransactionOutput::new(
                public_key,
                MonetaryValue::try_from(10u64).unwrap(),
                2,
                ESigPayload::new_with_ueta(),
            )),
        },
        correlation_id: CorrelationId::default(),
        extra: vec![],
    };
    let correlation_id = clear_redeem_request.correlation_id;
    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request);

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then
    assert_matches!(result, Err(FailureReason::InputLessThanRedeemAmount));
}

#[test]
fn clear_redeem_request__errors_when_clear_redeem_request_is_constructed_with_incorrect_change_output(
) {
    // Given a maliciously constructed redeem request incorrect change output
    let csprng = &mut OsRng::default();
    let public_key = PublicKey::from("public key");
    let clear_redeem_request = ClearRedeemRequestTransaction {
        input: vec![ClearTransactionOutput::new(
            public_key,
            MonetaryValue::try_from(100u64).unwrap(),
            0,
            ESigPayload::new_with_ueta(),
        )],
        output: ClearRedeemOutput {
            redeem_output: ClearTransactionOutput::new(
                public_key,
                MonetaryValue::try_from(50u64).unwrap(),
                1,
                ESigPayload::new_with_ueta(),
            ),
            change_output: Some(ClearTransactionOutput::new(
                public_key,
                MonetaryValue::try_from(100u64).unwrap(),
                2,
                ESigPayload::new_with_ueta(),
            )),
        },
        correlation_id: CorrelationId::default(),
        extra: vec![],
    };
    let correlation_id = clear_redeem_request.correlation_id;
    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request);

    // When
    let result = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request);

    // Then
    assert_matches!(
        result,
        Err(FailureReason::TxoChangeOutputHasUnexpectedAmount)
    );
}

#[test]
fn clear_redeem_request__errors_when_input_txos_are_reused() {
    // Given two redeem requests that consume the same input txo
    let csprng = &mut OsRng::default();
    let public_key = PublicKey::from("public key");
    let input = ClearTransactionOutput::new(
        public_key,
        MonetaryValue::try_from(100u64).unwrap(),
        0,
        ESigPayload::new_with_ueta(),
    );
    let clear_redeem_request_1 = ClearRedeemRequestTransaction {
        input: vec![input],
        output: ClearRedeemOutput {
            redeem_output: ClearTransactionOutput::new(
                public_key,
                MonetaryValue::try_from(50u64).unwrap(),
                1,
                ESigPayload::new_with_ueta(),
            ),
            change_output: Some(ClearTransactionOutput::new(
                public_key,
                MonetaryValue::try_from(50u64).unwrap(),
                2,
                ESigPayload::new_with_ueta(),
            )),
        },
        correlation_id: csprng.gen(),
        extra: vec![],
    };
    let clear_redeem_request_2 = ClearRedeemRequestTransaction {
        input: vec![input],
        output: ClearRedeemOutput {
            redeem_output: ClearTransactionOutput::new(
                public_key,
                MonetaryValue::try_from(50u64).unwrap(),
                3,
                ESigPayload::new_with_ueta(),
            ),
            change_output: Some(ClearTransactionOutput::new(
                public_key,
                MonetaryValue::try_from(50u64).unwrap(),
                4,
                ESigPayload::new_with_ueta(),
            )),
        },
        correlation_id: csprng.gen(),
        extra: vec![],
    };

    let ctx = TestContext::new().valid_for_clear_redeem(&clear_redeem_request_1);

    // When they are both submitted
    let result_1 = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request_1);
    let result_2 = ctx
        .redeem_request_processor()
        .clear_redeem_request(clear_redeem_request_2);

    // Then the first is successful and the second errors that it is not redeemable
    assert!(result_1.is_ok());
    assert_matches!(result_2, Err(FailureReason::ClearTxoNotRedeemable));
}

mod clear_redeem_fulfillment {
    #![allow(non_snake_case)]

    use crate::{
        chain_logic::redeems::tests::TestContext, errors::FailureReason,
        transactions::clear_redeem::TestClearRedeemRequestBuilder, ClearRedeemRequestRecord,
        ClearTransactionOutputRepository, CorrelationId, RedeemFulfillmentHandler,
        RedeemFulfillmentTransaction, RedeemRequestRecord, RedeemRequestRepository,
    };
    use assert_matches::assert_matches;
    use rand::rngs::OsRng;

    #[test]
    fn clear_redeem_fulfillment__happy_path() {
        // Given a redeem request
        let csprng = OsRng::default();
        let req = TestClearRedeemRequestBuilder::default()
            .build(csprng)
            .unwrap();
        let correlation_id = req.correlation_id;
        let as_record = ClearRedeemRequestRecord::from(req.clone());
        // And a test context with the request recorded in storage
        let ctx = TestContext::new();
        ctx.redeem_request_repo
            .store_redeem_request(correlation_id, RedeemRequestRecord::Clear(as_record));
        // And a confirmation for the redeem
        let confirm_txn = RedeemFulfillmentTransaction {
            correlation_id: req.correlation_id,
        };

        // When
        let result = ctx
            .redeem_request_processor()
            .redeem_fulfillment(confirm_txn);

        // Then the fulfillment succeeds
        assert!(result.is_ok());
        // And the redeem request record is removed
        assert_eq!(
            ctx.redeem_request_repo
                .get_unfulfilled_redeem_request(correlation_id),
            Option::None
        );
        // And redeem output is unusable
        assert!(!ctx.clear_txo_repo.contains(&req.output.redeem_output));
    }

    #[test]
    fn clear_redeem_fulfillment__errors_when_attempting_to_fulfill_non_existent_redeem() {
        // Given a test context with no unfulfilled redeem requests
        let ctx = TestContext::new();
        // And a confirmation for non-existant redeem
        let confirm_txn = RedeemFulfillmentTransaction {
            correlation_id: CorrelationId::default(),
        };

        // When
        let result = ctx
            .redeem_request_processor()
            .redeem_fulfillment(confirm_txn);

        // Then
        assert_matches!(result, Err(FailureReason::NoMatchingRedeemRequestToFulfill));
    }
}

#[cfg(test)]
mod clear_redeem_cancellation {
    #![allow(non_snake_case)]

    use crate::{
        chain_logic::redeems::tests::TestContext, errors::FailureReason,
        transactions::clear_redeem::TestClearRedeemRequestBuilder, ClearRedeemRequestRecord,
        ClearTransactionOutputRepository, CorrelationId, RedeemCancellationHandler,
        RedeemCancellationReason, RedeemCancellationTransaction, RedeemRequestRecord,
        RedeemRequestRepository, TotalIssuanceRepository,
    };
    use assert_matches::assert_matches;
    use rand::rngs::OsRng;

    #[test]
    fn clear_redeem_cancellation__happy_path() {
        // Given a redeem request
        let csprng = OsRng::default();
        let req = TestClearRedeemRequestBuilder::default()
            .build(csprng)
            .unwrap();
        let correlation_id = req.correlation_id;
        let as_record = ClearRedeemRequestRecord::from(req.clone());
        // And a test context with the request recorded in storage
        let ctx = TestContext::new();
        ctx.redeem_request_repo
            .store_redeem_request(correlation_id, RedeemRequestRecord::Clear(as_record));
        // And a cancellation request for the redeem
        let cancel_txn = RedeemCancellationTransaction {
            correlation_id,
            reason: RedeemCancellationReason::InvalidData,
        };

        // When
        let result = ctx
            .redeem_request_processor()
            .redeem_cancellation(cancel_txn);

        // Then the cancellation succeeds
        assert!(result.is_ok());
        // And the redeem request record is removed from storage
        assert_eq!(
            ctx.redeem_request_processor()
                .redeem_request_repo
                .get_unfulfilled_redeem_request(correlation_id),
            None
        );
        // And the total redeem amount is not altered
        assert_eq!(ctx.total_issuance_repo.get_total_redeemed(), 0);
        // And the redeem output is usable by the owner
        assert!(ctx.clear_txo_repo.contains(&req.output.redeem_output))
    }

    #[test]
    fn clear_redeem_cancellation__errors_when_attempting_to_cancel_nonexistent_redeem() {
        // Given a test context with no unfulfilled redeem requests
        let ctx = TestContext::new();
        // And a cancellation for a non-existent redeem
        let cancel_tx = RedeemCancellationTransaction {
            correlation_id: CorrelationId::default(),
            reason: RedeemCancellationReason::InvalidData,
        };

        // When
        let result = ctx
            .redeem_request_processor()
            .redeem_cancellation(cancel_tx);

        // Then
        assert_matches!(result, Err(FailureReason::NoMatchingRedeemRequestToCancel));
    }

    #[test]
    fn clear_redeem_cancellation__reduces_redeemed_claims() {
        // Given a redeem request
        let starting_amount = 100;
        let redeem_amount = 10;
        let expected_amount = 100;
        let csprng = OsRng::default();
        let req = TestClearRedeemRequestBuilder::default()
            .with_redeem_amount(redeem_amount)
            .build(csprng)
            .unwrap();
        let correlation_id = req.correlation_id;
        let as_record = ClearRedeemRequestRecord::from(req);
        // And a test context with the request recorded a given number of issued claims
        let ctx = TestContext::new().with_claims_in_network(starting_amount);
        ctx.redeem_request_repo
            .store_redeem_request(correlation_id, RedeemRequestRecord::Clear(as_record));
        // And a cancellation for the redeem
        let cancel_txn = RedeemCancellationTransaction {
            correlation_id,
            reason: RedeemCancellationReason::InvalidData,
        };

        // When
        let result = ctx
            .redeem_request_processor()
            .redeem_cancellation(cancel_txn);

        // Then the cancellation succeeds
        assert!(result.is_ok());
        // And the total claims is restored to the original amount
        assert_eq!(ctx.total_issuance_repo.get_total_claims(), expected_amount);
    }
}
