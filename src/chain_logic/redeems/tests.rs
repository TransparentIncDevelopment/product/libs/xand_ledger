mod clear_redeems;
mod redeems;

use crate::test::fakes::fake_clear_transaction_data_repository::FakeClearTransactionDataRepository;
use crate::test::fakes::fake_validator_repository::FakeValidatorRepository;
use crate::{
    crypto::DefaultCryptoImpl,
    redeems::RedeemRequestRecord,
    test::fakes::{
        FakeCorrelationIdRepository, FakeIdentityTagRepo, FakeKeyImageRepo, FakeMemberRepository,
        FakeRedeemRequestRepository, FakeTotalIssuanceRepository, FakeTransactionDataRepository,
        FakeTrustMetadata,
    },
    transactions::RedeemRequestBuildResponse,
    ClearRedeemRequestTransaction, ClearTransactionOutput, ConfidentialRedeemRequestRecord,
    CorrelationId, IUtxo, IdentityTag, KeyImage, PublicInputSet, PublicKey, RedeemRequestProcessor,
    RedeemRequestRepository, RedeemRequestTransaction, TotalIssuanceRepository,
    TransactionDataRepository, TransactionOutput, TxoStatus,
};
use std::marker::PhantomData;

pub struct TestContext {
    clear_txo_repo: FakeClearTransactionDataRepository,
    txo_repo: FakeTransactionDataRepository,
    key_image_repo: FakeKeyImageRepo,
    identity_tag_repo: FakeIdentityTagRepo,
    member_repo: FakeMemberRepository,
    trust_metadata: FakeTrustMetadata,
    correlation_id_repo: FakeCorrelationIdRepository,
    redeem_request_repo: FakeRedeemRequestRepository,
    total_issuance_repo: FakeTotalIssuanceRepository,
    validator_repo: FakeValidatorRepository,
}

impl TestContext {
    pub fn new() -> Self {
        Self {
            clear_txo_repo: FakeClearTransactionDataRepository::new(),
            correlation_id_repo: FakeCorrelationIdRepository::new(),
            identity_tag_repo: FakeIdentityTagRepo::new(),
            txo_repo: FakeTransactionDataRepository::new(),
            trust_metadata: FakeTrustMetadata::new(),
            member_repo: FakeMemberRepository::new(),
            key_image_repo: FakeKeyImageRepo::new(),
            redeem_request_repo: FakeRedeemRequestRepository::new(),
            total_issuance_repo: FakeTotalIssuanceRepository::new(),
            validator_repo: FakeValidatorRepository::new(),
        }
    }

    fn valid_for_confidential_redeem(
        self,
        redeem_request_data: &RedeemRequestBuildResponse,
    ) -> Self {
        let identity_tags =
            get_identity_tags_from_redeem_request(&redeem_request_data.redeem_request);
        let txos = get_input_txos_from_redeem_request(&redeem_request_data.redeem_request);
        self.with_identity_tags(&identity_tags)
            .with_txos(&txos)
            .with_trust(redeem_request_data.trust_pub_key)
    }

    fn valid_for_clear_redeem(
        self,
        redeem_request: &ClearRedeemRequestTransaction<ClearTransactionOutput>,
    ) -> Self {
        let clear_txos = redeem_request.input.clone();
        self.with_clear_txos(&clear_txos)
            .with_validators(&[redeem_request.output.redeem_output.public_key()])
    }

    fn with_claims_in_network(self, amount: u64) -> Self {
        self.total_issuance_repo
            .add_to_total_cash_confirmations(amount);
        self
    }

    fn with_trust(self, trust: PublicKey) -> Self {
        Self {
            trust_metadata: FakeTrustMetadata::from_trust_key(trust),
            ..self
        }
    }

    fn with_identity_tags(self, tags: &[IdentityTag]) -> Self {
        Self {
            identity_tag_repo: FakeIdentityTagRepo::seeded(tags),
            ..self
        }
    }

    fn with_members(self, tags: &[IdentityTag]) -> Self {
        Self {
            member_repo: FakeMemberRepository::new().with_members(tags.to_vec()),
            ..self
        }
    }
    pub fn with_confidential_redeem_request(
        &self,
        correlation_id: CorrelationId,
        record: ConfidentialRedeemRequestRecord,
    ) {
        self.redeem_request_repo.store_redeem_request(
            correlation_id,
            RedeemRequestRecord::Confidential(Box::new(record)),
        );
    }

    fn with_txos(self, txos: &[TransactionOutput]) -> Self {
        Self {
            txo_repo: FakeTransactionDataRepository::seeded(txos, TxoStatus::Confirmed, 0),
            ..self
        }
    }

    fn with_clear_txos(self, clear_txos: &[ClearTransactionOutput]) -> Self {
        Self {
            clear_txo_repo: FakeClearTransactionDataRepository::seeded(clear_txos),
            ..self
        }
    }

    fn with_txos_status(self, txos: &[TransactionOutput], status: TxoStatus) -> Self {
        Self {
            txo_repo: FakeTransactionDataRepository::seeded(txos, status, 0),
            ..self
        }
    }

    fn with_key_images(self, key_images: &[KeyImage]) -> Self {
        Self {
            key_image_repo: FakeKeyImageRepo::seeded(key_images),
            ..self
        }
    }

    fn with_validators(self, validators: &[PublicKey]) -> Self {
        Self {
            validator_repo: FakeValidatorRepository::new().with_validators(validators),
            ..self
        }
    }

    pub fn redeem_request_processor(
        &self,
    ) -> RedeemRequestProcessor<
        impl Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTransactionOutput>)>,
        DefaultCryptoImpl,
        u64,
        FakeClearTransactionDataRepository,
    > {
        RedeemRequestProcessor {
            correlation_id_repo: &self.correlation_id_repo,
            identity_tag_repo: &self.identity_tag_repo,
            clear_txo_repo: &self.clear_txo_repo,
            txo_repo: &self.txo_repo,
            trust_metadata: &self.trust_metadata,
            member_repo: &self.member_repo,
            key_image_repo: &self.key_image_repo,
            redeem_request_repo: &self.redeem_request_repo,
            total_issuance_repo: &self.total_issuance_repo,
            validator_repo: &self.validator_repo,
            _crypto: PhantomData,
        }
    }

    pub fn total_redeem_amount(&self) -> u64 {
        self.total_issuance_repo.get_total_redeemed()
    }

    pub fn transaction_status(&self, output: &TransactionOutput) -> TxoStatus {
        self.txo_repo.lookup_status(output).0
    }
}

fn get_identity_tags_from_redeem_request(
    transaction: &RedeemRequestTransaction,
) -> Vec<IdentityTag> {
    transaction
        .core_transaction
        .input
        .iter()
        .map(|PublicInputSet { identity_input, .. }| identity_input)
        .copied()
        .collect()
}

fn get_input_txos_from_redeem_request(
    transaction: &RedeemRequestTransaction,
) -> Vec<TransactionOutput> {
    transaction
        .core_transaction
        .input
        .iter()
        .flat_map(|PublicInputSet { txos, .. }| txos.components.clone())
        .collect()
}

fn get_input_txos_from_clear_redeem_request(
    transaction: &ClearRedeemRequestTransaction<ClearTransactionOutput>,
) -> Vec<ClearTransactionOutput> {
    transaction.input.clone()
}
