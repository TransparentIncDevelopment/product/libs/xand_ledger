use crate::{
    crypto::LedgerCrypto,
    errors::FailureReason,
    esig_payload::transaction_output_contains_valid_ueta,
    transactions::{verify_send_claims_transaction, PublicInputSet},
    IdentityTagRepository, KeyImageRepository, MemberRepository, SendClaimsHandler,
    SendClaimsTransaction, TransactionDataRepository, TransactionOutput, TransactionResult,
    TxoStatus,
};
use core::marker::PhantomData;

#[cfg(test)]
pub(crate) mod tests;

#[derive(derive_more::Constructor)]
pub struct SendClaimsProcessor<'a, C, BlockNumber: PartialOrd> {
    txo_repo: &'a dyn TransactionDataRepository<BlockNumber>,
    key_image_repo: &'a dyn KeyImageRepository,
    identity_tag_repo: &'a dyn IdentityTagRepository,
    member_repo: &'a dyn MemberRepository<BlockNumber>,
    _crypto: PhantomData<C>,
}

/// [SendClaimsTransaction] which has been validated by [SendClaimsProcessor].
struct ValidSendClaimsTransaction(SendClaimsTransaction);

impl<'a, C: LedgerCrypto, BlockNumber: PartialOrd> SendClaimsProcessor<'a, C, BlockNumber> {
    fn validate_send_claims_transaction(
        &self,
        transaction: SendClaimsTransaction,
    ) -> Result<ValidSendClaimsTransaction, FailureReason> {
        // Verify against the state of the chain
        verify_send_claims_input_txos_state(self.txo_repo, &transaction)?;
        verify_send_claims_identity_tags_exist(
            self.identity_tag_repo,
            self.member_repo,
            &transaction,
        )?;
        verify_key_images_do_not_already_exist(self.key_image_repo, &transaction)?;
        verify_send_claims_output_txos_state(self.txo_repo, &transaction)?;

        verify_proofs_are_valid::<C, BlockNumber>(self.member_repo, &transaction)?;

        Ok(ValidSendClaimsTransaction(transaction))
    }

    fn apply_send_claims_state_changes(&self, transaction: ValidSendClaimsTransaction) {
        save_identity_output(self.identity_tag_repo, &transaction.0);
        save_key_images(self.key_image_repo, &transaction.0);
        save_txos(self.txo_repo, &transaction.0);
    }
}

impl<'a, C: LedgerCrypto, BlockNumber: PartialOrd> SendClaimsHandler
    for SendClaimsProcessor<'a, C, BlockNumber>
{
    fn send_claims(&self, transaction: SendClaimsTransaction) -> TransactionResult {
        let valid_send_claims_transaction = self.validate_send_claims_transaction(transaction)?;
        self.apply_send_claims_state_changes(valid_send_claims_transaction);
        Ok(())
    }
}

fn save_key_images(image_repository: &dyn KeyImageRepository, transaction: &SendClaimsTransaction) {
    transaction
        .core_transaction
        .key_images
        .iter()
        .for_each(|image| image_repository.insert_key_image(*image))
}

fn save_identity_output(
    identity_repo: &dyn IdentityTagRepository,
    transaction: &SendClaimsTransaction,
) {
    identity_repo.store_id_tag(transaction.core_transaction.output_identity);
}

fn save_txos<BlockNumber: PartialOrd>(
    txo_repository: &dyn TransactionDataRepository<BlockNumber>,
    transaction: &SendClaimsTransaction,
) {
    transaction
        .core_transaction
        .output
        .iter()
        .map(|output| output.txo)
        .for_each(|txo| txo_repository.set_output_status(txo, TxoStatus::Confirmed));
}

fn verify_send_claims_input_txos_state<BlockNumber: PartialOrd>(
    txo_repo: &dyn TransactionDataRepository<BlockNumber>,
    transaction: &SendClaimsTransaction,
) -> TransactionResult {
    transaction
        .core_transaction
        .input
        .iter()
        .flat_map(|set| &set.txos.components)
        .try_for_each(|txo| {
            verify_txo_is_ueta_compliant(txo)?;
            verify_txo_is_spendable(txo_repo, txo)
        })
}

fn verify_txo_is_ueta_compliant(txo: &TransactionOutput) -> TransactionResult {
    (transaction_output_contains_valid_ueta(txo))
        .then_some(())
        .ok_or(FailureReason::TxoNotUETACompliant)
}

/// Send-claims-specific validation of a spendable transaction output
fn verify_txo_is_spendable<BlockNumber: PartialOrd>(
    txo_repo: &dyn TransactionDataRepository<BlockNumber>,
    txo: &TransactionOutput,
) -> TransactionResult {
    (txo_repo.lookup_status(txo).0 == TxoStatus::Confirmed)
        .then_some(())
        .ok_or(FailureReason::TxoNotSpendable(*txo))
}

fn verify_send_claims_identity_tags_exist<BlockNumber>(
    identity_tag_repo: &dyn IdentityTagRepository,
    member_repo: &dyn MemberRepository<BlockNumber>,
    transaction: &SendClaimsTransaction,
) -> TransactionResult {
    transaction
        .core_transaction
        .input
        .iter()
        .all(|PublicInputSet { identity_input, .. }| {
            identity_tag_repo.exists(identity_input) || member_repo.is_valid_member(identity_input)
        })
        .then_some(())
        .ok_or(FailureReason::UnknownIdentity)
}

fn verify_key_images_do_not_already_exist(
    key_image_repo: &dyn KeyImageRepository,
    transaction: &SendClaimsTransaction,
) -> TransactionResult {
    let new_key_images = &transaction.core_transaction.key_images;
    new_key_images
        .iter()
        .all(|image| !key_image_repo.is_used(image))
        .then_some(())
        .ok_or(FailureReason::KeyImageAlreadySpent)
}

fn verify_send_claims_output_txos_state<BlockNumber: PartialOrd>(
    txo_repo: &dyn TransactionDataRepository<BlockNumber>,
    transaction: &SendClaimsTransaction,
) -> TransactionResult {
    transaction
        .core_transaction
        .output
        .iter()
        .try_for_each(|output| {
            verify_txo_is_new(txo_repo, &output.txo)?;
            verify_txo_is_ueta_compliant(&output.txo)
        })
}

fn verify_txo_is_new<BlockNumber: PartialOrd>(
    txo_repo: &dyn TransactionDataRepository<BlockNumber>,
    txo: &TransactionOutput,
) -> TransactionResult {
    (txo_repo.lookup_status(txo).0 == TxoStatus::Nonexistent)
        .then_some(())
        .ok_or(FailureReason::OutputTxoAlreadyExists)
}

fn verify_proofs_are_valid<T: LedgerCrypto, BlockNumber>(
    member_repo: &dyn MemberRepository<BlockNumber>,
    txn: &SendClaimsTransaction,
) -> Result<(), FailureReason> {
    let banned_members = member_repo
        .get_banned_members()
        .into_iter()
        .map(|(p, _)| p)
        .collect();
    verify_send_claims_transaction::<T>(txn, banned_members)?;
    Ok(())
}
