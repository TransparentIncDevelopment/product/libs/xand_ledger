use crate::{
    chain_logic::SendClaimsProcessor,
    crypto::DefaultCryptoImpl,
    test::{
        arb_rng,
        fakes::{
            FakeCsprng, FakeIdentityTagRepo, FakeKeyImageRepo, FakeMemberRepository,
            FakeTransactionDataRepository,
        },
    },
    transactions::{PublicInputSet, TestSendClaimsBuilder, TransactionValidationError},
    FailureReason, IdentityTag, KeyImage, OneTimeKey, SendClaimsHandler, SendClaimsOutput,
    SendClaimsTransaction, TransactionOutput, TxoStatus,
};
use core::marker::PhantomData;
use curve25519_dalek::scalar::Scalar;
use proptest::prelude::*;
use rand::rngs::OsRng;
use zkplmt::bulletproofs::BulletRangeProof;

struct TestContext {
    identity_tag_repo: FakeIdentityTagRepo,
    txo_repo: FakeTransactionDataRepository,
    key_image_repo: FakeKeyImageRepo,
    member_repo: FakeMemberRepository,
}

impl TestContext {
    fn new() -> Self {
        Self {
            identity_tag_repo: FakeIdentityTagRepo::new(),
            txo_repo: FakeTransactionDataRepository::new(),
            key_image_repo: FakeKeyImageRepo::new(),
            member_repo: FakeMemberRepository::new(),
        }
    }

    fn with_identity_tags(self, tags: &[IdentityTag]) -> Self {
        Self {
            identity_tag_repo: FakeIdentityTagRepo::seeded(tags),
            ..self
        }
    }

    fn with_members(self, tags: &[IdentityTag]) -> Self {
        Self {
            member_repo: FakeMemberRepository::new().with_members(tags.to_vec()),
            ..self
        }
    }

    fn with_txos(self, txos: &[TransactionOutput]) -> Self {
        Self {
            txo_repo: FakeTransactionDataRepository::seeded(txos, TxoStatus::Confirmed, 0),
            ..self
        }
    }

    fn with_txos_status(self, txos: &[TransactionOutput], status: TxoStatus) -> Self {
        Self {
            txo_repo: FakeTransactionDataRepository::seeded(txos, status, 0),
            ..self
        }
    }

    fn with_key_images(self, key_images: &[KeyImage]) -> Self {
        Self {
            key_image_repo: FakeKeyImageRepo::seeded(key_images),
            ..self
        }
    }

    fn send_claims_processor(&self) -> SendClaimsProcessor<DefaultCryptoImpl, u64> {
        SendClaimsProcessor {
            txo_repo: &self.txo_repo,
            key_image_repo: &self.key_image_repo,
            identity_tag_repo: &self.identity_tag_repo,
            member_repo: &self.member_repo,
            _crypto: PhantomData::default(),
        }
    }
}

fn rng() -> OsRng {
    OsRng::default()
}

fn get_input_txos_from_send_claims(transaction: &SendClaimsTransaction) -> Vec<TransactionOutput> {
    transaction
        .core_transaction
        .input
        .iter()
        .flat_map(|PublicInputSet { txos, .. }| txos.components.clone())
        .collect()
}

fn get_identity_tags_from_send_claims(transaction: &SendClaimsTransaction) -> Vec<IdentityTag> {
    transaction
        .core_transaction
        .input
        .iter()
        .map(|PublicInputSet { identity_input, .. }| identity_input)
        .copied()
        .collect()
}

fn generate_valid_test_context(transaction: &SendClaimsTransaction) -> TestContext {
    let txos = get_input_txos_from_send_claims(transaction);
    let identity_tags = get_identity_tags_from_send_claims(transaction);
    TestContext::new()
        .with_identity_tags(&identity_tags)
        .with_txos(txos.as_slice())
}

fn arb_bullet_proof(rng: &mut FakeCsprng) -> BulletRangeProof {
    TestSendClaimsBuilder::default()
        .build(rng)
        .core_transaction
        .range_proof
}
// TODO: increase the number of proptest case runs after optimizing test overhead
//       see ADO 6573 (https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6573)

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_happy_path(
        rng in arb_rng()
    ) {
        let transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&transaction);
        ctx.send_claims_processor().send_claims(transaction).unwrap();
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_if_txo_inputs_do_not_exist_in_repository(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let identity_tags = get_identity_tags_from_send_claims(&send_claims_transaction);
        // Notice `txos` aren't included in the `TestContext`.
        let ctx = TestContext::new().with_identity_tags(&identity_tags);
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(result, Err(FailureReason::TxoNotSpendable(_))))
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_if_txo_inputs_exist_but_not_confirmed_in_repository(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let txos = get_input_txos_from_send_claims(&send_claims_transaction);
        let identity_tags = get_identity_tags_from_send_claims(&send_claims_transaction);
        // Notice `my_spendable_txos` are included in the `TestContext`, but as `Pending`.
        let ctx = TestContext::new()
            .with_identity_tags(&identity_tags)
            .with_txos_status(&txos, TxoStatus::Pending);
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(result, Err(FailureReason::TxoNotSpendable(_))))
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_if_identity_tags_do_not_exist_in_repository(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let txos = get_input_txos_from_send_claims(&send_claims_transaction);
        let identity_tags = get_identity_tags_from_send_claims(&send_claims_transaction);
        let ctx = TestContext::new().with_txos(txos.as_slice());
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(result, Err(FailureReason::UnknownIdentity)))
    }
}

mod esig_payload {
    use super::*;
    use crate::{
        esig_payload::{errors::ESigError, ESigPayload, UETA_TEXT},
        test_helpers::empty_esig_payload,
    };

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn send_claims_fails_without_ueta_payload_on_input(
            mut rng in arb_rng()
        ) {
            // Given a send claims transaction with a set of input TXOs that contains a TXO with a non-UETA compliant payload
            let send_claims_transaction = TestSendClaimsBuilder::default().build(&mut rng);
            // When
            let res = TransactionOutput::with_custom_esig_payload(
                Default::default(), OneTimeKey::random(&mut rng), empty_esig_payload()
            ).unwrap_err();

            // Then
            assert!(matches!(res, ESigError::InvalidStringContent));

        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        /// Type safety of `ESigPayload` obviates the need for an unhappy path test.
        /// In the future with other e-signature schemes, we will need to test unhappy paths.
        #[allow(unused_must_use)]
        #[test]
        fn successfully_constructed_send_claims_has_valid_ueta_output_in_txos(
                rng in arb_rng()
        ){
            // Given a new TXO with a UETA compliant payload
            // When we construct the send claims transaction
            let send_claims_transaction = TestSendClaimsBuilder::default().build(rng);

            // Then its txos have the correct payload
            send_claims_transaction
            .core_transaction
            .output
            .into_iter()
            .map(|txo| {
                assert!(matches!(
                    txo.txo.esig_payload(),
                    ESigPayload::Ueta(_)
                ));
                assert_eq!(
                    txo.txo.esig_payload().ueta_text(),
                    Some(UETA_TEXT.to_string())
                );
            });
        }
    }
}

#[test]
fn send_claims_succeeds_with_valid_member_address_as_true_source() {
    let csprng = &mut OsRng::default();
    let private_key = Scalar::random(csprng);
    let identity_source = IdentityTag::from_key_with_generator_base(private_key.into());
    let send_claims_transaction = TestSendClaimsBuilder::default()
        .with_identity_source(identity_source)
        .with_private_key(private_key)
        .build(csprng);
    let txos = get_input_txos_from_send_claims(&send_claims_transaction);
    let identity_tags: Vec<IdentityTag> =
        get_identity_tags_from_send_claims(&send_claims_transaction)
            .into_iter()
            .filter(|id| id != &identity_source)
            .collect();

    let ctx = TestContext::new()
        .with_identity_tags(&identity_tags)
        .with_members(&vec![identity_source])
        .with_txos(txos.as_slice());
    let result = ctx
        .send_claims_processor()
        .send_claims(send_claims_transaction);
    assert!(
        matches!(result, Ok(_)),
        "Expected success but got error {:?}",
        result
    );
}

#[test]
fn send_claims_fails_with_invalid_member_address() {
    let csprng = &mut OsRng::default();
    let private_key = Scalar::random(csprng);
    let identity_source = IdentityTag::from_key_with_generator_base(private_key.into());
    let send_claims_transaction = TestSendClaimsBuilder::default()
        .with_identity_source(identity_source)
        .build(csprng);
    let txos = get_input_txos_from_send_claims(&send_claims_transaction);
    let identity_tags: Vec<IdentityTag> =
        get_identity_tags_from_send_claims(&send_claims_transaction)
            .into_iter()
            .filter(|id| id != &identity_source)
            .collect();

    let ctx = TestContext::new()
        .with_identity_tags(&identity_tags)
        .with_txos(txos.as_slice());
    let result = ctx
        .send_claims_processor()
        .send_claims(send_claims_transaction);
    assert!(
        matches!(result, Err(FailureReason::UnknownIdentity)),
        "Expected UnknownIdentity but got {:?}",
        result
    );
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_if_key_images_already_exist_in_repository(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction)
            .with_key_images(send_claims_transaction.core_transaction.key_images.as_slice());

        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(result, Err(FailureReason::KeyImageAlreadySpent)))
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_new_identity_tag_output_added_to_repository(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        ctx.send_claims_processor().send_claims(send_claims_transaction.clone()).unwrap();

        assert!(ctx
            .identity_tag_repo
            .all_recorded_tags()
            .contains(&send_claims_transaction.core_transaction.output_identity));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_key_images_added_to_repository(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        ctx.send_claims_processor().send_claims(send_claims_transaction.clone()).unwrap();

        send_claims_transaction
            .core_transaction
            .key_images
            .iter()
            .for_each(|image| {
                assert!(
                    ctx.key_image_repo.current_key_images().contains(image),
                    "Could not find {:?}",
                    image
                )
            });
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_output_txos_added_to_repository(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        ctx.send_claims_processor().send_claims(send_claims_transaction.clone()).unwrap();

        send_claims_transaction
            .core_transaction
            .output
            .iter()
            .for_each(|SendClaimsOutput { txo, .. }| {
                assert_eq!(
                    *ctx.txo_repo.current_txos().get(txo).unwrap(),
                    (TxoStatus::Confirmed, Some(0)),
                    "Could not find {:?}",
                    txo
                )
            });
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_if_output_txos_already_exist_in_repository(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let mut txos = get_input_txos_from_send_claims(&send_claims_transaction);
        // Adding output txo to txo repo
        txos.push(send_claims_transaction.core_transaction.output[0].txo);
        let identity_tags = get_identity_tags_from_send_claims(&send_claims_transaction);
        let ctx = TestContext::new()
            .with_identity_tags(&identity_tags)
            .with_txos(&txos);
        assert_eq!(
            ctx.send_claims_processor().send_claims(send_claims_transaction).err(),
            Some(FailureReason::OutputTxoAlreadyExists)
        );
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]
    #[test]
    #[ignore = "blocked on verify_zkplmt's susceptibility to panics"]
    fn send_claims_fails_with_empty_input_proof(
        rng in arb_rng()
    ) {
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        // alter the proof of the send claims transaction after it was constructed
        send_claims_transaction.pi.corrupt_proof();

        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(TransactionValidationError::InvalidSignature))
        ));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_with_corrupted_input_proof(
        rng in arb_rng()
    ) {
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        // corrupt the proof of the send claims transaction after it was constructed
        send_claims_transaction.pi.corrupt_proof();
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(TransactionValidationError::InvalidSignature))
        ));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_with_irrelevant_valid_input_proof(
        mut rng in arb_rng()
    ) {
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(&mut rng);
        let dummy_send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        // use an irrelevant, valid proof in the send_claims_transaction
        send_claims_transaction.pi = dummy_send_claims_transaction.pi;
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(TransactionValidationError::InvalidSignature))
        ));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_with_corrupted_bullet_proof(
        rng in arb_rng()
    ) {
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        // corrupt the bullet proof of the send claims transaction after it was constructed
        send_claims_transaction.core_transaction.range_proof.corrupt_proof();
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(TransactionValidationError::InvalidBulletProof))
        ));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_with_irrelevant_valid_bullet_proof(
        mut rng in arb_rng(),
    ) {
        let dummy_range_proof = arb_bullet_proof(&mut rng);
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);

        // Replace bullet proof with proof from other send claims transaction
        send_claims_transaction.core_transaction.range_proof = dummy_range_proof;

        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(TransactionValidationError::RangeProofCommitmentMismatch)))
        );
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_with_corrupted_output_proof(
        rng in arb_rng()
    ) {
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        send_claims_transaction.core_transaction.alpha.corrupt_proof();
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(TransactionValidationError::InvalidAlpha))
        ));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_with_irrelevant_valid_output_proof(
        mut rng in arb_rng()
    ) {
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(&mut rng);
        let dummy_send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        // use an irrelevant, valid proof in the send claims transaction
        send_claims_transaction.core_transaction.alpha = dummy_send_claims_transaction.core_transaction.alpha;
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(TransactionValidationError::InvalidAlpha))
        ));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_with_corrupted_encrypted_sender_proof(
        rng in arb_rng()
    ) {
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        send_claims_transaction.core_transaction.encrypted_sender.corrupt_proof();
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(TransactionValidationError::InvalidEncryption(_)))
        ));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]

    #[test]
    fn send_claims_fails_with_irrelevant_valid_encrypted_sender_proof(
        mut rng in arb_rng()
    ) {
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(&mut rng);
        let dummy_send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        send_claims_transaction.core_transaction.encrypted_sender = dummy_send_claims_transaction.core_transaction.encrypted_sender;
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(matches!(
            result,
            Err(FailureReason::TransactionCouldNotBeVerified(TransactionValidationError::InvalidEncryption(_)))
        ));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]
    #[test]
    #[ignore = "TestBuilder does not currently support testing this case, refine it to \
    address potential adversarial scenarios"]
    fn send_claims_fails_with_no_outputs(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default().with_outputs(vec![]).build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(result.is_err());
    }
}

#[cfg(any(test, feature = "test-helpers"))]
proptest! {
    #![proptest_config(ProptestConfig::with_cases(2))]
    #[test]
    #[ignore = "This should fail but unexpectedly works. Need to review bullet proof."]
    fn send_claims_fails_with_zero_value(
        rng in arb_rng()
    ) {
        let send_claims_transaction = TestSendClaimsBuilder::default()
            .with_inputs(vec![0])
            .with_outputs(vec![0])
            .build(rng);
        let ctx = generate_valid_test_context(&send_claims_transaction);
        let result = ctx.send_claims_processor().send_claims(send_claims_transaction);
        assert!(result.is_err());
    }
}
