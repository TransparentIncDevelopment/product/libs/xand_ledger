// Avoid clippy warning in RedeemRequestProcessor constructor hidden behind derive_more::Constructor macro
#![allow(clippy::too_many_arguments)]

mod exiting_redeems;
#[cfg(test)]
pub mod tests;

use crate::{
    crypto::LedgerCrypto,
    esig_payload::{ESigPayload, UETA_TEXT},
    model::{clear_txo::IUtxo, redeems::RedeemRequestRecord},
    redeems::ClearRedeemRequestRecord,
    transactions::verify_redeem_request_transaction,
    ClearRedeemRequestHandler, ClearRedeemRequestTransaction, ClearTransactionOutputRepository,
    ConfidentialRedeemRequestRecord, CorrelationId, CorrelationIdRepository, FailureReason,
    IdentityTagRepository, KeyImageRepository, MemberRepository, PublicInputSet,
    RedeemCancellationHandler, RedeemCancellationTransaction, RedeemFulfillmentHandler,
    RedeemFulfillmentTransaction, RedeemRequestHandler, RedeemRequestRepository,
    RedeemRequestTransaction, TotalIssuanceRepository, TransactionDataRepository,
    TransactionOutput, TransactionResult, TrustMetadata, TxoStatus, ValidatorRepository,
};
use alloc::{boxed::Box, string::ToString, vec::Vec};
use core::marker::PhantomData;

pub use exiting_redeems::ExitingRedeemRequestProcessor;

#[derive(derive_more::Constructor)]
pub struct RedeemRequestProcessor<
    'a,
    RedeemRequestRecordIter,
    C,
    BlockNumber: PartialOrd,
    ClearTxoStore: ClearTransactionOutputRepository,
> {
    clear_txo_repo: &'a ClearTxoStore,
    txo_repo: &'a dyn TransactionDataRepository<BlockNumber>,
    key_image_repo: &'a dyn KeyImageRepository,
    identity_tag_repo: &'a dyn IdentityTagRepository,
    member_repo: &'a dyn MemberRepository<BlockNumber>,
    trust_metadata: &'a dyn TrustMetadata,
    correlation_id_repo: &'a dyn CorrelationIdRepository,
    redeem_request_repo:
        &'a dyn RedeemRequestRepository<Utxo = ClearTxoStore::Utxo, Iter = RedeemRequestRecordIter>,
    total_issuance_repo: &'a dyn TotalIssuanceRepository,
    validator_repo:
        &'a dyn ValidatorRepository<PublicKey = <ClearTxoStore::Utxo as IUtxo>::PublicKey>,
    _crypto: PhantomData<C>,
}

impl<'a, RedeemRequestRecordIter, C, BlockNumber, ClearTxoStore>
    RedeemRequestProcessor<'a, RedeemRequestRecordIter, C, BlockNumber, ClearTxoStore>
where
    RedeemRequestRecordIter:
        Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTxoStore::Utxo>)>,
    BlockNumber: PartialOrd,
    ClearTxoStore: ClearTransactionOutputRepository,
{
    fn verify_redeem_request_identity_tags_exist(
        &self,
        transaction: &RedeemRequestTransaction,
    ) -> TransactionResult {
        transaction
            .core_transaction
            .input
            .iter()
            .all(|PublicInputSet { identity_input, .. }| {
                self.identity_tag_repo.exists(identity_input)
                    || self.member_repo.is_valid_member(identity_input)
            })
            .then_some(())
            .ok_or(FailureReason::UnknownIdentity)
    }

    fn verify_confidential_redeem_request_input_txos_state(
        &self,
        transaction: &RedeemRequestTransaction,
    ) -> TransactionResult {
        transaction
            .core_transaction
            .input
            .iter()
            .flat_map(|set| &set.txos.components)
            .try_for_each(|txo| self.verify_txo_is_spendable(txo))
    }

    /// Redeem-specific validation of a spendable transaction output
    fn verify_txo_is_spendable(&self, txo: &TransactionOutput) -> TransactionResult {
        (self.txo_repo.lookup_status(txo).0 == TxoStatus::Confirmed)
            .then_some(())
            .ok_or(FailureReason::TxoNotSpendable(*txo))
    }

    fn verify_key_images_do_not_already_exist(
        &self,
        transaction: &RedeemRequestTransaction,
    ) -> TransactionResult {
        let new_key_images = &transaction.core_transaction.key_images;
        new_key_images
            .iter()
            .all(|image| !self.key_image_repo.is_used(image))
            .then_some(())
            .ok_or(FailureReason::KeyImageAlreadySpent)
    }

    fn verify_confidential_txos_are_new(
        &self,
        transaction: &RedeemRequestTransaction,
    ) -> TransactionResult {
        let change_txo_is_new = self
            .txo_repo
            .lookup_status(&transaction.core_transaction.output.change_output.txo)
            .0
            == TxoStatus::Nonexistent;
        let redeem_txo_is_new = self
            .txo_repo
            .lookup_status(
                &transaction
                    .core_transaction
                    .output
                    .redeem_output
                    .transaction_output,
            )
            .0
            == TxoStatus::Nonexistent;

        if change_txo_is_new && redeem_txo_is_new {
            Ok(())
        } else {
            Err(FailureReason::OutputTxoAlreadyExists)
        }
    }

    fn verify_proofs_are_valid<T: LedgerCrypto>(
        &self,
        txn: &RedeemRequestTransaction,
    ) -> Result<(), FailureReason> {
        let trust_key = self
            .trust_metadata
            .get_trust_key()
            .ok_or(FailureReason::InvalidTrustAccountId)?;
        let banned_members: Vec<_> = self
            .member_repo
            .get_banned_members()
            .into_iter()
            .map(|(p, _)| p)
            .collect();
        verify_redeem_request_transaction::<T>(txn, &trust_key, &banned_members)?;
        Ok(())
    }

    fn verify_correlation_id_has_not_been_used(
        &self,
        correlation_id: CorrelationId,
    ) -> Result<(), FailureReason> {
        if self.correlation_id_repo.is_used(correlation_id).is_some() {
            return Err(FailureReason::CorrelationIdCannotBeReused);
        }
        Ok(())
    }

    fn verify_confidential_redeem_request_does_not_already_exist(
        &self,
        txn: &RedeemRequestTransaction,
    ) -> Result<(), FailureReason> {
        if self
            .redeem_request_repo
            .get_unfulfilled_redeem_request(txn.core_transaction.correlation_id)
            .is_some()
        {
            return Err(FailureReason::RedeemRequestIdAlreadyExists);
        }
        Ok(())
    }

    fn save_key_images(&self, transaction: &RedeemRequestTransaction) {
        transaction
            .core_transaction
            .key_images
            .iter()
            .for_each(|image| self.key_image_repo.insert_key_image(*image))
    }

    fn save_confidential_change_output(&self, transaction: &RedeemRequestTransaction) {
        let txo = transaction.core_transaction.output.change_output.txo;
        self.txo_repo.set_output_status(txo, TxoStatus::Confirmed)
    }

    fn save_identity_output(&self, transaction: &RedeemRequestTransaction) {
        self.identity_tag_repo
            .store_id_tag(transaction.core_transaction.output_identity);
    }

    fn save_correlation_id(&self, correlation_id: CorrelationId) {
        self.correlation_id_repo.store_id(correlation_id);
    }

    fn save_confidential_redeem_request_record(&self, txn: RedeemRequestTransaction) {
        self.total_issuance_repo
            .add_to_total_redeemed(txn.core_transaction.output.redeem_output.value);
        self.redeem_request_repo.store_redeem_request(
            txn.core_transaction.correlation_id,
            RedeemRequestRecord::Confidential(Box::new(ConfidentialRedeemRequestRecord::from(txn))),
        );
    }

    fn verify_clear_redeem_request_input_txos_redeemable(
        &self,
        transaction: &ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) -> TransactionResult {
        transaction.input.iter().try_for_each(|clear_txo| {
            (self.clear_txo_repo.contains(clear_txo))
                .then_some(())
                .ok_or(FailureReason::ClearTxoNotRedeemable)
        })
    }

    fn verify_output_clear_txos_are_new(
        &self,
        transaction: &ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) -> TransactionResult {
        if !self.change_clear_txo_exists(transaction.output.change_output)
            && !self.redeem_clear_txo_exists(transaction.output.redeem_output)
        {
            Ok(())
        } else {
            Err(FailureReason::OutputTxoAlreadyExists)
        }
    }

    fn change_clear_txo_exists(&self, change_output: Option<ClearTxoStore::Utxo>) -> bool {
        if let Some(txo) = change_output {
            self.clear_txo_repo.contains(&txo)
        } else {
            false
        }
    }

    fn redeem_clear_txo_exists(&self, redeem_output: ClearTxoStore::Utxo) -> bool {
        self.clear_txo_repo.contains(&redeem_output)
    }

    fn clear_verify_correlation_id_has_not_been_used(
        &self,
        txn: &ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) -> TransactionResult {
        if self
            .correlation_id_repo
            .is_used(txn.correlation_id)
            .is_some()
        {
            return Err(FailureReason::CorrelationIdCannotBeReused);
        }
        Ok(())
    }

    fn verify_clear_redeem_request_does_not_already_exist(
        &self,
        txn: &ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) -> TransactionResult {
        if self
            .redeem_request_repo
            .get_unfulfilled_redeem_request(txn.correlation_id)
            .is_some()
        {
            return Err(FailureReason::RedeemRequestIdAlreadyExists);
        }
        Ok(())
    }

    fn verify_clear_txos_ueta_compliant(
        &self,
        txn: &ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) -> TransactionResult {
        if !txn
            .input
            .iter()
            .all(|utxo| self.utxo_contains_valid_ueta(utxo))
            || !self.utxo_contains_valid_ueta(&txn.output.redeem_output)
            || (txn.output.change_output.is_some()
                && !self.utxo_contains_valid_ueta(&txn.output.change_output.unwrap()))
        {
            return Err(FailureReason::TxoNotUETACompliant);
        }
        Ok(())
    }

    fn utxo_contains_valid_ueta(&self, utxo: &ClearTxoStore::Utxo) -> bool {
        matches!(utxo.esig_payload(), ESigPayload::Ueta(val) if val.to_string() == UETA_TEXT)
    }

    fn verify_input_sum_is_sufficient_for_clear_redeem(
        &self,
        txn: &ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) -> TransactionResult {
        let inputs_sum: u64 = txn.input.iter().map(|txo| txo.value().value()).sum();
        let change_amount = inputs_sum
            .checked_sub(txn.output.redeem_output.value().value())
            .ok_or(FailureReason::InputLessThanRedeemAmount)?;
        let actual_change_amount = if let Some(change_output) = txn.output.change_output {
            change_output.value().value()
        } else {
            0
        };
        if change_amount != actual_change_amount {
            return Err(FailureReason::TxoChangeOutputHasUnexpectedAmount);
        }
        Ok(())
    }

    fn verify_clear_txos_belong_to_redeeming_validator(
        &self,
        txn: &ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) -> TransactionResult {
        let public_key = txn.output.redeem_output.public_key();
        if !self.validator_repo.is_validator(&public_key) {
            return Err(FailureReason::UnknownIdentity);
        }
        let change_output_public_key_matches = if let Some(change_output) = txn.output.change_output
        {
            change_output.public_key() == public_key
        } else {
            true
        };
        if !change_output_public_key_matches
            || !txn
                .input
                .iter()
                .all(|clear_txo| public_key == clear_txo.public_key())
        {
            return Err(FailureReason::ClearTxoDoesNotBelongToRedeemingValidator);
        }
        Ok(())
    }

    fn remove_input_clear_txos(
        &self,
        transaction: &ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) {
        for txo in &transaction.input {
            self.clear_txo_repo.remove(*txo);
        }
    }

    fn save_clear_change_output(
        &self,
        transaction: &ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) {
        let txo = transaction.output.change_output;
        if let Some(change_output) = transaction.output.change_output {
            self.clear_txo_repo.add(change_output)
        }
    }

    fn save_clear_redeem_request_record(
        &self,
        txn: ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) where
        RedeemRequestRecordIter:
            Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTxoStore::Utxo>)>,
    {
        self.total_issuance_repo
            .add_to_total_redeemed(txn.output.redeem_output.value().value());
        self.redeem_request_repo.store_redeem_request(
            txn.correlation_id,
            RedeemRequestRecord::Clear(ClearRedeemRequestRecord::from(txn)),
        )
    }
}

impl<RI, C, BlockNumber, ClearTxoStore> RedeemRequestHandler
    for RedeemRequestProcessor<'_, RI, C, BlockNumber, ClearTxoStore>
where
    RI: Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTxoStore::Utxo>)>,
    C: LedgerCrypto,
    BlockNumber: PartialOrd,
    ClearTxoStore: ClearTransactionOutputRepository,
{
    fn redeem_request(&self, transaction: RedeemRequestTransaction) -> TransactionResult {
        self.verify_redeem_request_identity_tags_exist(&transaction)?;
        self.verify_confidential_redeem_request_input_txos_state(&transaction)?;
        self.verify_key_images_do_not_already_exist(&transaction)?;
        self.verify_confidential_txos_are_new(&transaction)?;
        self.verify_proofs_are_valid::<C>(&transaction)?;
        self.verify_confidential_redeem_request_does_not_already_exist(&transaction)?;
        self.verify_correlation_id_has_not_been_used(transaction.core_transaction.correlation_id)?;

        //Transaction is now confirmed to be valid. Apply changes
        self.save_key_images(&transaction);
        self.save_confidential_change_output(&transaction);
        self.save_identity_output(&transaction);
        self.save_correlation_id(transaction.core_transaction.correlation_id);
        self.save_confidential_redeem_request_record(transaction);
        Ok(())
    }
}

impl<RI, C, BlockNumber, ClearTxoStore: ClearTransactionOutputRepository>
    ClearRedeemRequestHandler<ClearTxoStore::Utxo>
    for RedeemRequestProcessor<'_, RI, C, BlockNumber, ClearTxoStore>
where
    RI: Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTxoStore::Utxo>)>,
    C: LedgerCrypto,
    BlockNumber: PartialOrd,
    ClearTxoStore: ClearTransactionOutputRepository,
{
    fn clear_redeem_request(
        &self,
        transaction: ClearRedeemRequestTransaction<ClearTxoStore::Utxo>,
    ) -> TransactionResult {
        self.verify_clear_redeem_request_does_not_already_exist(&transaction)?;
        self.verify_clear_redeem_request_input_txos_redeemable(&transaction)?;
        self.verify_output_clear_txos_are_new(&transaction)?;
        self.verify_correlation_id_has_not_been_used(transaction.correlation_id)?;
        self.verify_clear_txos_ueta_compliant(&transaction)?;
        self.verify_input_sum_is_sufficient_for_clear_redeem(&transaction)?;
        self.verify_clear_txos_belong_to_redeeming_validator(&transaction)?;
        // TODO: verify that validator redeem request transaction doesn't exceed a certain size (maybe
        //  via constraining the number of input txos?) https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7307

        // Transaction is now confirmed to be valid. Apply changes
        self.remove_input_clear_txos(&transaction);
        self.save_clear_change_output(&transaction);
        self.save_correlation_id(transaction.correlation_id);
        self.save_clear_redeem_request_record(transaction);
        Ok(())
    }
}

impl<RI, C, BlockNumber, ClearTxoStore> RedeemFulfillmentHandler
    for RedeemRequestProcessor<'_, RI, C, BlockNumber, ClearTxoStore>
where
    RI: Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTxoStore::Utxo>)>,
    BlockNumber: PartialOrd,
    ClearTxoStore: ClearTransactionOutputRepository,
{
    fn redeem_fulfillment(&self, fulfillment: RedeemFulfillmentTransaction) -> TransactionResult {
        // check that there is a matching redeem request record
        let maybe_redeem = self
            .redeem_request_repo
            .get_unfulfilled_redeem_request(fulfillment.correlation_id);
        match maybe_redeem {
            None => Err(FailureReason::NoMatchingRedeemRequestToFulfill),
            Some(pending_redeem) => {
                // Delete the redeem request record
                self.redeem_request_repo
                    .remove_redeem_request(fulfillment.correlation_id);
                Ok(())
            }
        }
    }
}

impl<RI, C, BlockNumber, ClearTxoStore> RedeemCancellationHandler
    for RedeemRequestProcessor<'_, RI, C, BlockNumber, ClearTxoStore>
where
    RI: Iterator<Item = (CorrelationId, RedeemRequestRecord<ClearTxoStore::Utxo>)>,
    BlockNumber: PartialOrd,
    ClearTxoStore: ClearTransactionOutputRepository,
{
    fn redeem_cancellation(
        &self,
        cancellation: RedeemCancellationTransaction,
    ) -> TransactionResult {
        match self
            .redeem_request_repo
            .get_unfulfilled_redeem_request(cancellation.correlation_id)
        {
            None => Err(FailureReason::NoMatchingRedeemRequestToCancel),
            Some(record) => {
                // Decrease the total redeem amount
                self.total_issuance_repo
                    .subtract_from_total_redeemed(record.amount());
                match record {
                    // Make confidential redeem output spendable by redeem requester
                    RedeemRequestRecord::Confidential(r) => self.txo_repo.set_output_status(
                        r.redeem_output.transaction_output,
                        TxoStatus::Confirmed,
                    ),
                    // Make clear redeem output redeemable by redeem requester
                    RedeemRequestRecord::Clear(r) => self.clear_txo_repo.add(r.redeem_output),
                }
                // Delete the redeem request
                self.redeem_request_repo
                    .remove_redeem_request(cancellation.correlation_id);
                Ok(())
            }
        }
    }
}
