#[cfg(any(test, feature = "test-helpers"))]
use crate::crypto::get_G;
#[cfg(any(test, feature = "test-helpers"))]
use crate::test_helpers::random_private_key;
use crate::{
    clear_txo::IUtxo, esig_payload::ESigPayload, ClearRedeemOutput, ClearRedeemRequestTransaction,
    ClearTransactionOutput, CorrelationId, MonetaryValue, PublicKey, TransactionBuilderErrors,
};
use alloc::vec::Vec;
use core::convert::TryFrom;
use rand::RngCore;
#[cfg(any(test, feature = "test-helpers"))]
use rand::{CryptoRng, Rng};

pub fn construct_clear_redeem_request_transaction<R: RngCore>(
    issuer: PublicKey,
    input_txos: Vec<ClearTransactionOutput>,
    redeem_amount: u64,
    correlation_id: CorrelationId,
    extra: Vec<u8>,
    rng: &mut R,
) -> Result<ClearRedeemRequestTransaction<ClearTransactionOutput>, TransactionBuilderErrors> {
    let output = construct_redeem_output(
        issuer,
        input_txos.clone(),
        redeem_amount,
        correlation_id,
        rng,
    )?;

    Ok(ClearRedeemRequestTransaction {
        input: input_txos,
        output,
        correlation_id,
        extra,
    })
}

fn construct_redeem_output<R: RngCore>(
    issuer: PublicKey,
    inputs: Vec<ClearTransactionOutput>,
    redeem_amount: u64,
    correlation_id: CorrelationId,
    rng: &mut R,
) -> Result<ClearRedeemOutput<ClearTransactionOutput>, TransactionBuilderErrors> {
    let change_amount = calculate_change_amount(&inputs, redeem_amount)?;

    let change_output = if change_amount > 0 {
        Some(ClearTransactionOutput::new(
            issuer,
            MonetaryValue::try_from(change_amount)?,
            rng.next_u64(),
            ESigPayload::new_with_ueta(),
        ))
    } else {
        None
    };

    let redeem_output = ClearTransactionOutput::new(
        issuer,
        MonetaryValue::try_from(redeem_amount)?,
        rng.next_u64(),
        ESigPayload::new_with_ueta(),
    );

    Ok(ClearRedeemOutput {
        redeem_output,
        change_output,
    })
}

fn calculate_change_amount(
    inputs: &[ClearTransactionOutput],
    redeem_amount: u64,
) -> Result<u64, TransactionBuilderErrors> {
    let inputs_sum: u64 = inputs.iter().map(|txo| txo.value().value()).sum();
    inputs_sum
        .checked_sub(redeem_amount)
        .ok_or(TransactionBuilderErrors::InsufficientRedeemRequestInput)
}

/// Builder pattern for clear redeem request transactions
#[cfg(any(test, feature = "test-helpers"))]
#[derive(Clone)]
pub struct TestClearRedeemRequestBuilder {
    issuer: Option<PublicKey>,
    input_values: Vec<u64>,
    redeem_amount: u64,
    correlation_id: Option<CorrelationId>,
    extra: Vec<u8>,
}

#[cfg(any(test, feature = "test-helpers"))]
pub struct TestClearRedeemRequestInputs {
    pub issuer: PublicKey,
    pub input_txos: Vec<ClearTransactionOutput>,
    pub redeem_amount: u64,
    pub correlation_id: CorrelationId,
    pub extra: Vec<u8>,
}

#[cfg(any(test, feature = "test-helpers"))]
impl Default for TestClearRedeemRequestBuilder {
    fn default() -> Self {
        TestClearRedeemRequestBuilder {
            issuer: None,
            input_values: vec![5, 5, 5],
            redeem_amount: 11,
            correlation_id: None,
            extra: vec![],
        }
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl TestClearRedeemRequestBuilder {
    /// Specify public key of transaction creator
    pub fn with_issuer(mut self, public_key: PublicKey) -> Self {
        self.issuer = Some(public_key);
        self
    }

    pub fn with_redeem_amount(mut self, amount: u64) -> Self {
        self.redeem_amount = amount;
        self
    }

    /// Specify values for transaction
    pub fn input_values(mut self, values: Vec<u64>) -> Self {
        self.input_values = values;
        self
    }

    fn generate_new_clear_txos<R: RngCore>(
        public_key: PublicKey,
        input_values: Vec<u64>,
        rng: &mut R,
    ) -> Vec<ClearTransactionOutput> {
        input_values
            .iter()
            .map(|val| {
                ClearTransactionOutput::new(
                    public_key,
                    MonetaryValue::try_from(*val).unwrap(),
                    rng.next_u64(),
                    ESigPayload::new_with_ueta(),
                )
            })
            .collect()
    }

    fn clear_redeem_request_inputs<R: RngCore + CryptoRng>(
        self,
        mut rng: R,
    ) -> TestClearRedeemRequestInputs {
        let public_key = self.issuer.unwrap_or_else(|| {
            let private_key = random_private_key(&mut rng);
            (private_key * get_G()).into()
        });

        let input_txos = Self::generate_new_clear_txos(public_key, self.input_values, &mut rng);

        let correlation_id = self.correlation_id.unwrap_or_else(|| rng.gen());
        TestClearRedeemRequestInputs {
            issuer: public_key,
            input_txos,
            redeem_amount: self.redeem_amount,
            correlation_id,
            extra: self.extra,
        }
    }

    pub fn build<R: RngCore + CryptoRng>(
        self,
        mut csprng: R,
    ) -> Result<ClearRedeemRequestTransaction<ClearTransactionOutput>, TransactionBuilderErrors>
    {
        let inputs = self.clear_redeem_request_inputs(&mut csprng);
        construct_clear_redeem_request_transaction(
            inputs.issuer,
            inputs.input_txos,
            inputs.redeem_amount,
            inputs.correlation_id,
            inputs.extra,
            &mut csprng,
        )
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use crate::{
        clear_txo::IUtxo,
        esig_payload::{ESigPayload, UETA_TEXT},
        test::arb_rng,
        transactions::clear_redeem::TestClearRedeemRequestBuilder,
        PublicKey, TransactionBuilderErrors,
    };
    use assert_matches::assert_matches;
    use proptest::prelude::*;

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn construct_clear_redeem_request_transaction__succeeds_with_valid_inputs(rng in arb_rng()) {
            // Given a specified public key and a test builder for constructing a redeem request
            let expected_public_key = PublicKey::from("my public key");
            let builder = TestClearRedeemRequestBuilder::default()
                .with_issuer(expected_public_key);

            // When a redeem request is constructed
            let res = builder.build(rng);

            // Then it is constructed successfully with utxo outputs with the redeeming public key
            let transaction = res.unwrap();
            assert!(transaction.output.change_output.unwrap().public_key() == expected_public_key);
            assert!(transaction.output.redeem_output.public_key() == expected_public_key);
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn construct_clear_redeem_request_transaction__errors_with_inputs_that_do_not_exceed_the_request(rng in arb_rng()) {
            // Given a test builder for constructing a redeem request with a value greater than the sum of the input values.
            let builder = TestClearRedeemRequestBuilder::default()
                .with_redeem_amount(10)
                .input_values(vec![1, 1]);

            // When the request is constructed
            let res = builder.build(rng);

            // Then an InsufficientRedeemRequestInput error is thrown
            let error = res.unwrap_err();
            assert_matches!(error, TransactionBuilderErrors::InsufficientRedeemRequestInput);
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn construct_clear_redeem_request_transaction__creates_txos_with_ueta_payload(rng in arb_rng()) {
            // Given a test builder for constructing a redeem request
            let builder = TestClearRedeemRequestBuilder::default();

            // When the request is constructed
            let res = builder.build(rng);

            // Then it is constructed successfully with utxo outputs with the valid ueta payload
            let transaction = res.unwrap();
            assert_matches!(transaction.output.change_output.unwrap().esig_payload(), ESigPayload::Ueta(message) if message.to_string() == UETA_TEXT);
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn construct_clear_redeem_request_transaction__can_create_request_for_exact_sum_of_inputs_value(rng in arb_rng()) {
            // Given a test builder for constructing a redeem request that requests for a value of the exact sum of the input values
            let builder = TestClearRedeemRequestBuilder::default().with_redeem_amount(15).input_values(vec![5, 5, 5]);

            // When the request is constructed
            let res = builder.build(rng);

            // Then it is constructed successfully
            let transaction = res.unwrap();
            assert!(transaction.output.change_output.is_none());
        }
    }
}
