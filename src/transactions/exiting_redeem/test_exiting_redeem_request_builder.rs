use crate::{
    construct_exiting_redeem_request_transaction, generate_fake_input_txos,
    generate_spendable_utxos,
    test_helpers::random_private_key,
    transactions::exiting_redeem::{
        construct_core_exiting_redeem_request, exiting_redeem_request_shuffle_inputs,
        UnsignedExitingRedeemRequest,
    },
    CorrelationId, EncryptionKey, ExitingRedeemRequestTransaction, IdentityTag,
    PrivateExitingRedeemRequestInputs, PrivateKey, PublicKey, TransactionInputSet,
};
use alloc::vec::Vec;
use curve25519_dalek::scalar::Scalar;
use rand::{CryptoRng, Rng, RngCore};

/// Builder pattern for exiting redeem request transactions
#[derive(Clone)]
pub struct TestExitingRedeemRequestBuilder {
    pub input_identity: Option<IdentityTag>,
    pub input_values: Vec<u64>,
    pub redeem_amount: u64,
    pub private_key: Option<Scalar>,
    pub encryption_key: Option<EncryptionKey>,
    pub correlation_id: Option<CorrelationId>,
    pub trust_pub_key: PublicKey,
    pub banned_members: Option<Vec<PublicKey>>,
    pub extra: Vec<u8>,
}

#[cfg(any(test, feature = "test-helpers"))]
impl Default for TestExitingRedeemRequestBuilder {
    fn default() -> Self {
        TestExitingRedeemRequestBuilder {
            input_identity: None,
            input_values: vec![5, 5, 5],
            redeem_amount: 15,
            private_key: None,
            encryption_key: None,
            correlation_id: None,
            trust_pub_key: Default::default(),
            banned_members: None,
            extra: vec![],
        }
    }
}

#[cfg(any(test, feature = "test-helpers"))]
pub struct TestExitingRedeemRequestBuildResponse {
    pub redeem_request: ExitingRedeemRequestTransaction,
    pub trust_pub_key: PublicKey,
}

#[cfg(any(test, feature = "test-helpers"))]
pub struct TestExitingRedeemRequestInputs {
    pub private_inputs: PrivateExitingRedeemRequestInputs,
    pub redeem_amount: u64,
    pub trust_pub_key: PublicKey,
    pub own_pub_key: PublicKey,
    pub correlation_id: CorrelationId,
    pub decoy_txos: Vec<TransactionInputSet>,
    pub extra: Vec<u8>,
}

#[cfg(any(test, feature = "test-helpers"))]
impl TestExitingRedeemRequestBuilder {
    /// Specify private key of transaction creator
    pub fn with_private_key(mut self, private_key: PrivateKey) -> Self {
        self.private_key = Some(private_key);
        self
    }

    /// Specify the trust public key
    pub fn trust_public_key(mut self, trust_public_key: PublicKey) -> Self {
        self.trust_pub_key = trust_public_key;
        self
    }

    pub fn with_redeem_amount(mut self, amount: u64) -> Self {
        self.redeem_amount = amount;
        self
    }

    pub fn with_input_values(mut self, values: Vec<u64>) -> Self {
        self.input_values = values;
        self
    }

    fn exiting_redeem_request_inputs<R>(self, mut rng: R) -> TestExitingRedeemRequestInputs
    where
        R: RngCore + CryptoRng,
    {
        let private_key = self
            .private_key
            .unwrap_or_else(|| random_private_key(&mut rng));

        let my_identity_tag = self
            .input_identity
            .unwrap_or_else(|| IdentityTag::from_key_with_rand_base(private_key.into(), &mut rng));
        let my_spendable_txos =
            generate_spendable_utxos(my_identity_tag, self.input_values, &mut rng);
        let decoy_txos = generate_fake_input_txos(4, my_spendable_txos.len(), &mut rng)
            .into_iter()
            .map(|i| i.txos)
            .collect();

        let correlation_id = self.correlation_id.unwrap_or_else(|| rng.gen());
        let private_inputs = PrivateExitingRedeemRequestInputs {
            spends: my_spendable_txos,
            private_key,
        };
        let banned_members = self.banned_members.unwrap_or_default();
        let encryption_key = if let Some(key) = self.encryption_key {
            key
        } else {
            EncryptionKey::random(&mut rng)
        };
        TestExitingRedeemRequestInputs {
            private_inputs,
            redeem_amount: self.redeem_amount,
            trust_pub_key: self.trust_pub_key,
            own_pub_key: PublicKey::from(private_key),
            correlation_id,
            decoy_txos,
            extra: self.extra,
        }
    }

    pub fn build_unsigned<R>(self, mut rng: R) -> UnsignedExitingRedeemRequest
    where
        R: RngCore + CryptoRng,
    {
        let inputs = self.exiting_redeem_request_inputs(&mut rng);

        let shuffled_inputs = exiting_redeem_request_shuffle_inputs(
            inputs.decoy_txos,
            &inputs.private_inputs.spends,
            &mut rng,
        );

        let core_transaction = construct_core_exiting_redeem_request(
            inputs.private_inputs.private_key,
            inputs.redeem_amount,
            &inputs.private_inputs.spends,
            &shuffled_inputs,
            inputs.trust_pub_key,
            inputs.own_pub_key,
            inputs.correlation_id,
            inputs.extra,
            &mut rng,
        )
        .unwrap();

        UnsignedExitingRedeemRequest {
            secret_index: shuffled_inputs.s_index,
            private_key: inputs.private_inputs.private_key,
            core_details: core_transaction,
            trust_pub_key: inputs.trust_pub_key,
        }
    }

    pub fn build<R>(self, mut csprng: R) -> TestExitingRedeemRequestBuildResponse
    where
        R: RngCore + CryptoRng,
    {
        let inputs = self.exiting_redeem_request_inputs(&mut csprng);
        let redeem_request = construct_exiting_redeem_request_transaction(
            inputs.private_inputs,
            inputs.redeem_amount,
            inputs.trust_pub_key,
            inputs.own_pub_key,
            inputs.correlation_id,
            inputs.decoy_txos,
            inputs.extra,
            &mut csprng,
        )
        .unwrap();
        TestExitingRedeemRequestBuildResponse {
            redeem_request,
            trust_pub_key: inputs.trust_pub_key,
        }
    }
}
