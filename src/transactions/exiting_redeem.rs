use crate::{
    crypto::{get_G, DefaultCryptoImpl, LedgerCrypto},
    transactions::{
        common::verification::verify_commitment, create_commitments, key_images,
        output_blinding_factors,
    },
    CoreExitingRedeemRequestTransaction, CorrelationId, ExitingRedeemOutput,
    ExitingRedeemRequestTransaction, KeyImage, OneTimeKey, OpenedTransactionOutput,
    PrivateExitingRedeemRequestInputs, PrivateKey, PublicKey, SerializableForSigning,
    TransactionBuilderErrors, TransactionInputSet, TransactionOutput, TransactionValidationError,
};
use alloc::vec::Vec;
use curve25519_dalek::{ristretto::RistrettoPoint, scalar::Scalar};
use rand::{CryptoRng, RngCore};
use zkplmt::{
    core::{create_zkplmt, shuffle},
    models::{CurveVector, Proof, VectorTuple},
};

#[cfg(any(test, feature = "test-helpers"))]
pub mod test_exiting_redeem_request_builder;

use crate::esig_payload::ESigPayload;
#[cfg(any(test, feature = "test-helpers"))]
pub use test_exiting_redeem_request_builder::*;

#[allow(clippy::too_many_arguments)]
pub fn construct_exiting_redeem_request_transaction<R>(
    private_inputs: PrivateExitingRedeemRequestInputs,
    redeem_amount: u64,
    trust_pub_key: PublicKey,
    own_pub_key: PublicKey,
    correlation_id: CorrelationId,
    masking_inputs: Vec<TransactionInputSet>,
    extra: Vec<u8>,
    rng: &mut R,
) -> Result<ExitingRedeemRequestTransaction, TransactionBuilderErrors>
where
    R: RngCore + CryptoRng,
{
    let shuffled_inputs =
        exiting_redeem_request_shuffle_inputs(masking_inputs, &private_inputs.spends, rng);

    let CoreTransactionDetails {
        core_transaction, ..
    } = construct_core_exiting_redeem_request(
        private_inputs.private_key,
        redeem_amount,
        &private_inputs.spends,
        &shuffled_inputs,
        trust_pub_key,
        own_pub_key,
        correlation_id,
        extra,
        rng,
    )?;

    let tuples = extract_tuples::<DefaultCryptoImpl>(&core_transaction);
    let buf = core_transaction.signable_bytes();
    let pi = create_zkplmt(
        &buf,
        &tuples,
        shuffled_inputs.s_index,
        private_inputs.private_key,
        rng,
    )?;
    Ok(ExitingRedeemRequestTransaction {
        core_transaction,
        pi,
    })
}

pub(crate) fn exiting_redeem_request_shuffle_inputs<R: RngCore + CryptoRng>(
    masking_inputs: Vec<TransactionInputSet>,
    spends: &[OpenedTransactionOutput],
    rng: &mut R,
) -> ExitingRedeemRequestShuffledInputs {
    // First item in the inputs is the "real" one we can spend.
    let mut inputs = core::iter::once(TransactionInputSet {
        components: spends.iter().map(|x| x.transaction_output).collect(),
    })
    .chain(masking_inputs.into_iter().map(|x| TransactionInputSet {
        components: x.components,
    }))
    .collect::<Vec<_>>();
    let s_index = shuffle(&mut inputs, rng);

    ExitingRedeemRequestShuffledInputs { inputs, s_index }
}

fn tx_inputs_to_set_of_hash_key_sets<T: LedgerCrypto>(
    tx_inputs: &[TransactionInputSet],
) -> Vec<Vec<RistrettoPoint>> {
    tx_inputs
        .iter()
        .map(|input| T::txn_input_keys_to_hash_curve_points(&input.components))
        .collect()
}

fn extract_tuples<T: LedgerCrypto>(
    core_transaction: &CoreExitingRedeemRequestTransaction,
) -> Vec<VectorTuple> {
    let H = tx_inputs_to_set_of_hash_key_sets::<T>(&core_transaction.input);
    let key_images = &core_transaction.key_images;
    let output = &core_transaction.output;
    let output_sum = output.redeem_output.transaction_output.commitment();

    core_transaction
        .input
        .iter()
        .zip(&H)
        .map(|(input, hashes)| {
            let mut Y_j = exiting_redeem_request_components_to_vector_tuples(
                ExitingRedeemRequestComponentInputs {
                    Z: core_transaction.Z,
                    key_images,
                    sum_output: *output_sum,
                    input: &input.components,
                    hashes,
                    sender_pub_key: &core_transaction.sender,
                },
            );
            Y_j.values.push(
                core_transaction
                    .output
                    .redeem_output
                    .transaction_output
                    .public_key()
                    .into(),
            );
            Y_j
        })
        .collect::<Vec<_>>()
}

pub(crate) struct ExitingRedeemRequestComponentInputs<'a> {
    pub(crate) Z: RistrettoPoint,
    pub(crate) key_images: &'a [KeyImage],
    pub(crate) sum_output: RistrettoPoint,
    pub(crate) input: &'a [TransactionOutput],
    pub(crate) hashes: &'a [RistrettoPoint],
    pub(crate) sender_pub_key: &'a PublicKey,
}

// TODO: Needs a better name. I just dumbly extracted this to dedupe code. Parameters also need
//   better names where doable - or to be turned into a struct instead of a bunch of params.
pub(crate) fn exiting_redeem_request_components_to_vector_tuples(
    component_inputs: ExitingRedeemRequestComponentInputs,
) -> VectorTuple {
    let components = &component_inputs.input;
    let mut curve_vectors: Vec<CurveVector> = components
        .iter()
        .map(|c| CurveVector {
            x: c.public_key().x(),
            y: c.public_key().y(),
        })
        .collect();
    let mut hash_vectors = component_inputs
        .hashes
        .iter()
        .zip(component_inputs.key_images.iter())
        .map(|(hash, I)| CurveVector {
            x: *hash,
            y: (*I).into(),
        })
        .collect();
    curve_vectors.append(&mut hash_vectors);

    let sum_input = component_inputs
        .input
        .iter()
        .map(|c| c.commitment())
        .fold(RistrettoPoint::default(), |X, Y| X + Y);
    let sum_diff = sum_input - component_inputs.sum_output;

    curve_vectors.push(CurveVector {
        x: component_inputs.Z,
        y: sum_diff,
    });

    curve_vectors.push(CurveVector {
        x: get_G(),
        y: *component_inputs.sender_pub_key.as_ref(),
    });
    VectorTuple {
        values: curve_vectors,
    }
}

pub struct CoreTransactionDetails {
    core_transaction: CoreExitingRedeemRequestTransaction,
    output_blinding_factors: Vec<Scalar>,
}

pub(crate) struct ExitingRedeemRequestShuffledInputs {
    pub(crate) inputs: Vec<TransactionInputSet>,
    pub(crate) s_index: usize,
}

#[cfg(any(test, feature = "test-helpers"))]
pub struct UnsignedExitingRedeemRequest {
    pub secret_index: usize,
    pub private_key: PrivateKey,
    pub core_details: CoreTransactionDetails,
    pub trust_pub_key: PublicKey,
}

#[cfg(any(test, feature = "test-helpers"))]
impl UnsignedExitingRedeemRequest {
    pub fn sign<R>(self, mut rng: R) -> ExitingRedeemRequestTransaction
    where
        R: RngCore + CryptoRng,
    {
        let tuples = extract_tuples::<DefaultCryptoImpl>(&self.core_details.core_transaction);
        let buf = &self.core_details.core_transaction.signable_bytes();
        let pi =
            create_zkplmt(buf, &tuples, self.secret_index, self.private_key, &mut rng).unwrap();
        ExitingRedeemRequestTransaction {
            core_transaction: self.core_details.core_transaction,
            pi,
        }
    }
}

#[allow(clippy::too_many_arguments)]
fn construct_core_exiting_redeem_request<R>(
    private_key: PrivateKey,
    redeem_amount: u64,
    private_input_spends: &[OpenedTransactionOutput],
    tx_inputs: &ExitingRedeemRequestShuffledInputs,
    trust_pub_key: PublicKey,
    own_pub_key: PublicKey,
    correlation_id: CorrelationId,
    extra: Vec<u8>,
    rng: &mut R,
) -> Result<CoreTransactionDetails, TransactionBuilderErrors>
where
    R: RngCore + CryptoRng,
{
    let G = get_G();
    let inputs_sum: u64 = private_input_spends.iter().map(|txo| txo.value).sum();
    let output_values = [redeem_amount];
    let z = Scalar::random(rng);
    let Z = z * G;
    let output_blinding_factors =
        output_blinding_factors(1, private_key, private_input_spends, z, rng)?;
    let output_comms = create_commitments(&output_blinding_factors, &output_values)?;
    let signer_public_key = PublicKey::from(private_key);
    let redeem_public_key = OneTimeKey::from_public_key_with_randomness(signer_public_key, rng);
    let alpha = exiting_redeem_request_compute_alpha(z, Z, rng);
    let H = tx_inputs_to_set_of_hash_key_sets::<DefaultCryptoImpl>(&tx_inputs.inputs);
    let key_images = key_images(private_key, &H[tx_inputs.s_index]);

    Ok(CoreTransactionDetails {
        core_transaction: CoreExitingRedeemRequestTransaction {
            sender: PublicKey::from(private_key),
            input: tx_inputs.inputs.clone(),
            output: ExitingRedeemOutput {
                redeem_output: OpenedTransactionOutput {
                    transaction_output: TransactionOutput::new(
                        output_comms[0],
                        redeem_public_key,
                        ESigPayload::new_with_ueta(),
                    ),
                    blinding_factor: output_blinding_factors[0],
                    value: redeem_amount,
                },
            },
            correlation_id,
            key_images,
            Z,
            alpha,
            extra,
        },
        output_blinding_factors,
    })
}

pub fn exiting_redeem_request_compute_alpha<R: RngCore + CryptoRng>(
    z: Scalar,
    Z: RistrettoPoint,
    rng: &mut R,
) -> Proof {
    let values = vec![CurveVector { x: get_G(), y: Z }];

    let alpha_tuple = VectorTuple { values };
    create_zkplmt(&[], &[alpha_tuple], 0, z, rng).expect("This should never be reached")
}

pub fn verify_exiting_redeem_request_transaction<T: LedgerCrypto>(
    transaction: &ExitingRedeemRequestTransaction,
) -> Result<(), TransactionValidationError> {
    verify_input_txo_set_length(transaction)?;
    verify_alpha_proof::<T>(transaction)?;
    verify_txo_opened_data_is_correct(transaction)?;

    let tuples = extract_tuples::<T>(&transaction.core_transaction);
    let buf = transaction.core_transaction.signable_bytes();
    if T::verify_zkplmt(&buf, &tuples, &transaction.pi).is_err() {
        Err(TransactionValidationError::InvalidSignature)
    } else {
        Ok(())
    }
}

fn verify_txo_opened_data_is_correct(
    transaction: &ExitingRedeemRequestTransaction,
) -> Result<(), TransactionValidationError> {
    let opened_output = &transaction.core_transaction.output.redeem_output;
    verify_commitment(
        opened_output.blinding_factor,
        opened_output.value,
        opened_output.transaction_output.commitment(),
    )?;
    Ok(())
}

fn verify_input_txo_set_length(
    transaction: &ExitingRedeemRequestTransaction,
) -> Result<(), TransactionValidationError> {
    (!transaction.core_transaction.input.is_empty())
        .then_some(())
        .ok_or(TransactionValidationError::InvalidInputSet)
}

fn verify_alpha_proof<T: LedgerCrypto>(
    transaction: &ExitingRedeemRequestTransaction,
) -> Result<(), TransactionValidationError> {
    crate::transactions::common::verification::verify_alpha_proof::<T>(
        transaction.core_transaction.Z,
        &[],
        &[],
        &transaction.core_transaction.alpha,
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::crypto::{get_L, DefaultCryptoImpl};
    use rand::rngs::OsRng;

    #[test]
    fn exiting_redeem_happy_path() {
        let mut rng = OsRng::default();
        let builder = TestExitingRedeemRequestBuilder::default();
        let trust_key = builder.trust_pub_key;
        let private_key = PrivateKey::random(&mut rng);
        let own_key = PublicKey::from(private_key);
        let redeem_request = builder
            .with_private_key(private_key)
            .build(rng)
            .redeem_request;
        let result =
            verify_exiting_redeem_request_transaction::<DefaultCryptoImpl>(&redeem_request);
        assert!(result.is_ok())
    }

    #[test]
    fn redeem_more_than_input_fails() {
        let mut rng = OsRng::default();
        let builder = TestExitingRedeemRequestBuilder::default();
        let trust_key = builder.trust_pub_key;
        let private_key = PrivateKey::random(&mut rng);
        let own_key = PublicKey::from(private_key);
        let mut unsigned_redeem_request = builder.with_private_key(private_key).build_unsigned(rng);

        // preserve blinding factor used in original zero-value proof
        let original = unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output;
        // construct valid redeem output commitment higher than the inputs
        let value: u64 = 100_000_000;
        unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output = OpenedTransactionOutput {
            transaction_output: TransactionOutput::new(
                original.blinding_factor * get_G() + Scalar::from(value) * get_L(),
                *original.transaction_output.public_key(),
                ESigPayload::new_with_ueta(),
            ),
            blinding_factor: original.blinding_factor,
            value,
        };

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);
        let result =
            verify_exiting_redeem_request_transaction::<DefaultCryptoImpl>(&redeem_request);
        // Verify (Z, sum_diff) is no longer parallel to p
        assert!(
            matches!(result, Err(TransactionValidationError::InvalidSignature)),
            "Expected InvalidSignature but got {:?}",
            result
        );
    }

    // This test attempts to transfer the redeem output to a different owner in the case of a canceled
    // redeem request. (e.g. using redeem to send claims)
    // To verify this malicious proof, modify extract_tuples to exclude the redeem public key
    #[test]
    fn redeem_output_must_be_owned_by_issuer() {
        let mut rng = OsRng::default();
        let builder = TestExitingRedeemRequestBuilder::default();
        let mut unsigned_redeem_request = builder.build_unsigned(rng);

        let txo = TransactionOutput::new(
            *unsigned_redeem_request
                .core_details
                .core_transaction
                .output
                .redeem_output
                .transaction_output
                .commitment(),
            OneTimeKey::random(&mut rng),
            ESigPayload::new_with_ueta(),
        );

        unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output
            .transaction_output = txo;

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);
        let result =
            verify_exiting_redeem_request_transaction::<DefaultCryptoImpl>(&redeem_request);
        // Verify (A_r, B_r) is no longer parallel to p
        assert!(
            matches!(result, Err(TransactionValidationError::InvalidSignature)),
            "Expected InvalidSignature but got {:?}",
            result
        );
    }

    // This test attempts to redeem more than the committed amount.
    // While the proofs work against the commitment of the redeemed amount,
    // the trust will redeem an amount based on the opened redeem amount.
    // This test ensures that the inputs for the redeem request commitment
    // can't deviate from the values used in the proofs.
    //
    // To see this attack work, disable `verify_txo_opened_data_is_correct`
    #[test]
    fn opened_redeem_amount_cannot_be_different_than_commitment() {
        let rng = OsRng::default();
        let builder = TestExitingRedeemRequestBuilder::default();
        let mut unsigned_redeem_request = builder.build_unsigned(rng);
        // modify opened burn commitment inputs
        unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output
            .value = 100_000_000;

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);

        // Verify commitment mismatch error
        assert!(matches!(
            verify_exiting_redeem_request_transaction::<DefaultCryptoImpl>(&redeem_request,),
            Err(TransactionValidationError::OpenedCommitmentMismatch)
        ))
    }

    #[test]
    fn redeem_with_wrong_sender_key_fails() {
        let mut rng = OsRng::default();
        let builder = TestExitingRedeemRequestBuilder::default();
        let mut unsigned_redeem_request = builder.build_unsigned(rng);
        // modify opened burn commitment inputs
        let wrong_private_key = PrivateKey::random(&mut rng);
        let wrong_pub_key = PublicKey::from(wrong_private_key);

        unsigned_redeem_request.core_details.core_transaction.sender = wrong_pub_key;

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);

        let result =
            verify_exiting_redeem_request_transaction::<DefaultCryptoImpl>(&redeem_request)
                .unwrap_err();

        assert!(matches!(
            result,
            TransactionValidationError::InvalidSignature
        ));
    }
}
