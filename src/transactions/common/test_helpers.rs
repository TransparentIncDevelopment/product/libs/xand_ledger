use crate::{
    crypto::{get_G, get_L},
    esig_payload::ESigPayload,
    get_random_curve_point, IdentityTag, OneTimeKey, OpenedTransactionOutput, PrivateKey,
    PublicInputSet, PublicKey, Scalar, TransactionInputSet, TransactionOutput,
};
use alloc::vec::Vec;
use rand::{CryptoRng, RngCore};

pub const TEST_METADATA_BYTES: [u8; 2] = [0, 255];

pub fn generate_fake_input_txos<R: RngCore + CryptoRng>(
    num_input_sets: usize,
    num_input_txos: usize,
    csprng: &mut R,
) -> Vec<PublicInputSet> {
    (1..num_input_sets)
        .map(|_| {
            // It's all made up!
            let outputs_used_for_this_set: Vec<_> = (0..num_input_txos)
                .map(|_| {
                    let public_key = OneTimeKey::random(csprng);
                    TransactionOutput::new(
                        get_random_curve_point(csprng),
                        public_key,
                        ESigPayload::new_with_ueta(),
                    )
                })
                .collect();
            PublicInputSet {
                txos: TransactionInputSet::new(outputs_used_for_this_set),
                // Output address generation is described in the paper.
                identity_input: IdentityTag::random(csprng),
            }
        })
        .collect()
}

/// Given a private key and some amount values, generate UTXOs that key could spend worth the
/// provided amounts, to be used as inputs to a transaction.
pub fn generate_spendable_utxos<R: RngCore + CryptoRng>(
    our_identity_tag: IdentityTag,
    real_input_amounts: Vec<u64>,
    csprng: &mut R,
) -> Vec<OpenedTransactionOutput> {
    let G = get_G();
    let L = get_L();

    real_input_amounts
        .iter()
        .map(|input_value| {
            let randomness_for_input = Scalar::random(csprng);
            let pub_key = OneTimeKey::from_id_with_randomness(our_identity_tag, csprng);

            OpenedTransactionOutput {
                transaction_output: TransactionOutput::new(
                    randomness_for_input * G + Scalar::from(*input_value) * L,
                    pub_key,
                    ESigPayload::new_with_ueta(),
                ),
                blinding_factor: randomness_for_input,
                value: *input_value,
            }
        })
        .collect()
}

pub(crate) fn random_private_key<R>(rng: &mut R) -> PrivateKey
where
    R: RngCore + CryptoRng,
{
    Scalar::random(rng)
}

pub(crate) fn random_public_key<R>(rng: &mut R) -> PublicKey
where
    R: RngCore + CryptoRng,
{
    PublicKey::from(random_private_key(rng))
}
