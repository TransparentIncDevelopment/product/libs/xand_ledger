use super::*;
use crate::{
    crypto::{get_G, get_L, LedgerCrypto},
    esig_payload::ESigPayload,
    transactions::common::verification::{verify_commitment, verify_trust_sender_encryption},
    verifiable_encryption::VerifiableEncryptionOfCurvePoint,
    CorrelationId, IdentityTag, SerializableForSigning, VectorTuple,
};
#[cfg(any(test, feature = "test-helpers"))]
use core::iter::repeat_with;

/// All the (nonrandom) inputs required to construct a
/// [CreateRequestTransaction](struct.CreateRequestTransaction.html)
#[derive(Debug, PartialEq, Eq)]
pub struct CreateRequestTxnInputs {
    /// The identity tag which proves the keyholder is a verified participant
    pub identity_source: IdentityTag,
    /// A series of values that will be used to create TXOs if the created transaction is successful
    pub values: Vec<u64>,
    /// Other IdentityTags in the system not belonging to the keyholder that will mask the real tag
    pub masking_identity_sources: Vec<IdentityTag>,
    /// The correlation id that will be used to identify the associated cash transfer at a Xand-Enabled Bank
    pub correlation_id: CorrelationId,
    /// The private key belonging to the transaction creator
    pub private_key: PrivateKey,
    /// Permanent public key of the trust
    pub trust_pub_key: PublicKey,
    /// Encrypted Metadata (e.g. bank account details)
    pub extra: Vec<u8>,
}

/// Verifies a create request and its proofs. Requires a list of banned members is included. The list of
/// banned members must be the same length and same order as the Qs included on the transaction.
pub fn verify_create_request_transaction<T: LedgerCrypto>(
    transaction: &CreateRequestTransaction,
    trust_key: &PublicKey,
    banned_members: Vec<PublicKey>,
) -> Result<(), TransactionValidationError> {
    verify_txo_opened_data_is_correct(transaction)?;
    verify_sender_encryption(transaction, trust_key)?;
    verify_alpha_proof::<T>(transaction, banned_members)?;
    verify_sender_is_not_banned(transaction)?;
    verify_signature_proof::<T>(transaction)?;
    Ok(())
}

fn verify_signature_proof<T: LedgerCrypto>(
    transaction: &CreateRequestTransaction,
) -> Result<(), TransactionValidationError> {
    let tuples = extract_tuples_from_create_request(&transaction.core_transaction);
    let pi = &transaction.pi;
    let message = transaction.core_transaction.signable_bytes();
    if T::verify_zkplmt(&message, &tuples, pi).is_ok() {
        Ok(())
    } else {
        Err(TransactionValidationError::InvalidSignature)
    }
}

fn verify_sender_encryption(
    transaction: &CreateRequestTransaction,
    trust_key: &PublicKey,
) -> Result<(), TransactionValidationError> {
    verify_trust_sender_encryption(&transaction.core_transaction.encrypted_sender, trust_key)
}

fn verify_txo_opened_data_is_correct(
    transaction: &CreateRequestTransaction,
) -> Result<(), TransactionValidationError> {
    let G = get_G();
    let L = get_L();
    for opened_output in transaction.core_transaction.outputs.iter() {
        verify_commitment(
            opened_output.blinding_factor,
            opened_output.value,
            opened_output.transaction_output.commitment(),
        )?;
    }
    Ok(())
}

fn verify_alpha_proof<T: LedgerCrypto>(
    transaction: &CreateRequestTransaction,
    banned_members: Vec<PublicKey>,
) -> Result<(), TransactionValidationError> {
    crate::transactions::common::verification::verify_alpha_proof::<T>(
        transaction.core_transaction.Z,
        &banned_members,
        &transaction.core_transaction.Qs,
        &transaction.core_transaction.alpha,
    )
}

fn verify_sender_is_not_banned(
    transaction: &CreateRequestTransaction,
) -> Result<(), TransactionValidationError> {
    let _Z_ = transaction.core_transaction._Z_;
    transaction
        .core_transaction
        .Qs
        .iter()
        .try_for_each(|Q_e| do_not_match(_Z_, *Q_e))
}

fn do_not_match(_Z_: RistrettoPoint, Q: RistrettoPoint) -> Result<(), TransactionValidationError> {
    if _Z_ == Q {
        Err(TransactionValidationError::ConstructedByBannedMember)
    } else {
        Ok(())
    }
}

/// Given a create request transaction, extract the zkplmt tuples from it, so that they may be passed
/// into `verify_zkplmt` for verification.
fn extract_tuples_from_create_request(
    create_request_transaction: &CoreCreateRequestTransaction,
) -> Vec<VectorTuple> {
    let identity_target = &create_request_transaction.identity_output;
    let output_pubkeys: Vec<CurveVector> = create_request_transaction
        .outputs
        .iter()
        .map(|otxo| otxo.transaction_output.public_key().0)
        .collect();

    let encrypted_sender: VerifiableEncryptionOfCurvePoint =
        create_request_transaction.encrypted_sender.clone().into();

    let tuples: Vec<_> = create_request_transaction
        .identity_sources
        .iter()
        .map(|identity| {
            let mut tuple_values = output_pubkeys.clone();
            tuple_values.append(&mut vec![
                CurveVector {
                    x: create_request_transaction.Z,
                    y: create_request_transaction._Z_,
                },
                identity.0,
                identity_target.0,
                CurveVector {
                    x: encrypted_sender.Y,
                    y: encrypted_sender.P_,
                },
            ]);
            VectorTuple {
                values: tuple_values,
            }
        })
        .collect();
    tuples
}

/// Builds a create request
pub fn construct_create_request_transaction<R>(
    inputs: &CreateRequestTxnInputs,
    banned_members: Vec<PublicKey>,
    csprng: &mut R,
) -> Result<CreateRequestTransaction, TransactionBuilderErrors>
where
    R: RngCore + CryptoRng,
{
    if !check_ownership(inputs.identity_source, inputs.private_key) {
        return Err(TransactionBuilderErrors::IdentitySourceDoesNotMatchPrivateKey);
    }

    // We want everyone to use a ring signature size of 5, but we allow for less masking sources
    // in case there aren't enough other participants on the network.
    // See also, 'mixin level', Zero to Monero 2nd ed, Page 30, footnote 5
    if inputs.masking_identity_sources.len() > 4 {
        return Err(TransactionBuilderErrors::MaxMaskingIdentitySourcesExceeded);
    }

    if inputs.values.is_empty() || inputs.values.iter().any(|value| *value == u64::MIN) {
        return Err(TransactionBuilderErrors::ZeroValueInvalid);
    }

    unchecked_construct_create_request_transaction(inputs, banned_members, csprng)
}

fn unchecked_construct_create_request_transaction<R>(
    inputs: &CreateRequestTxnInputs,
    banned_members: Vec<PublicKey>,
    csprng: &mut R,
) -> Result<CreateRequestTransaction, TransactionBuilderErrors>
where
    R: RngCore + CryptoRng,
{
    let G = get_G();
    let mut identity_sources = vec![&inputs.identity_source];
    identity_sources.extend(inputs.masking_identity_sources.iter());

    let signer_public_key = PublicKey::from(inputs.private_key);
    let identity_encryption = VerifiableEncryptionOfCurvePoint::create(
        &signer_public_key.into(),
        &[IdentityTag::from_key_with_generator_base(inputs.trust_pub_key).into()],
        &G,
        csprng,
    )?;

    let opened_transaction_outputs = build_opened_outputs(inputs, csprng);

    let identity_target = IdentityTag::from_key_with_rand_base(signer_public_key, csprng);

    let mut indexes: Vec<usize> = (0..identity_sources.len()).collect();
    let true_input_index = shuffle(&mut indexes, csprng);

    // let shuffle_tuple: Vec<VectorTuple> = indexes.iter().map(|j| tuples[*j].clone()).collect();
    let shuffle_identity: Vec<IdentityTag> =
        indexes.into_iter().map(|j| *identity_sources[j]).collect();

    let z = Scalar::random(csprng);
    let Z = z * G;
    let _Z_ = inputs.private_key * Z;
    let mut values = vec![CurveVector { x: G, y: Z }];

    let (Ps_Qs, Qs): (Vec<CurveVector>, Vec<RistrettoPoint>) = banned_members
        .into_iter()
        .map(Into::<RistrettoPoint>::into)
        .map(|P_e| {
            let Q_e = z * P_e;
            (CurveVector { x: P_e, y: Q_e }, Q_e)
        })
        .unzip();

    values.extend(Ps_Qs);

    let alpha_tuple = VectorTuple { values };

    let alpha =
        create_zkplmt(&[], &[alpha_tuple], 0, z, csprng).expect("This should never be reached");

    let core_transaction = CoreCreateRequestTransaction {
        identity_sources: shuffle_identity,
        outputs: opened_transaction_outputs,
        identity_output: identity_target,
        correlation_id: inputs.correlation_id,
        encrypted_sender: identity_encryption.into(),
        Z,
        _Z_,
        alpha,
        Qs,
        extra: inputs.extra.clone(),
    };

    let tuples = extract_tuples_from_create_request(&core_transaction);
    let message = core_transaction.signable_bytes();
    let pi = create_zkplmt(
        &message,
        &tuples,
        true_input_index,
        inputs.private_key,
        csprng,
    )?;
    verify_zkplmt(&message, &tuples, &pi)
        .map_err(|_| TransactionBuilderErrors::ProofConstructionFailure)?;
    Ok(CreateRequestTransaction {
        core_transaction,
        pi,
    })
}

fn build_opened_outputs<R>(
    inputs: &CreateRequestTxnInputs,
    csprng: &mut R,
) -> Vec<OpenedTransactionOutput>
where
    R: RngCore + CryptoRng,
{
    let G = get_G();
    let L = get_L();
    let blinding_factor = Scalar::random(csprng);
    let opened_transaction_outputs: Vec<_> = inputs
        .values
        .iter()
        .map(|value| {
            let value_scalar = Scalar::from(*value);
            let commitment = blinding_factor * G + value_scalar * L;
            let public_key = OneTimeKey::from_id_with_randomness(inputs.identity_source, csprng);
            OpenedTransactionOutput {
                transaction_output: TransactionOutput::new(
                    commitment,
                    public_key,
                    ESigPayload::new_with_ueta(),
                ),
                blinding_factor,
                value: *value,
            }
        })
        .collect();
    opened_transaction_outputs
}

/// Builder pattern for create request transactions
#[cfg(any(test, feature = "test-helpers"))]
#[derive(Default)]
pub struct TestCreateRequestBuilder {
    identity_source: Option<IdentityTag>,
    values: Option<Vec<u64>>,
    private_key: Option<Scalar>,
    correlation_id: Option<CorrelationId>,
    trust_public_key: Option<PublicKey>,
    extra: Option<Vec<u8>>,
    masking_identity_sources: Option<Vec<IdentityTag>>,
    banned_members: Option<Vec<PublicKey>>,
}

pub struct CreateRequestResponse {
    pub create_request: CreateRequestTransaction,
    pub trust_pub_key: PublicKey,
}

#[cfg(any(test, feature = "test-helpers"))]
impl TestCreateRequestBuilder {
    /// Specify identity source for transaction
    pub fn identity_source(mut self, identity_source: IdentityTag) -> Self {
        self.identity_source = Some(identity_source);
        self
    }

    /// Specify values for transaction
    pub fn values(mut self, values: Vec<u64>) -> Self {
        self.values = Some(values);
        self
    }

    /// Specify private key of transaction creator
    pub fn private_key(mut self, private_key: Scalar) -> Self {
        self.private_key = Some(private_key);
        self
    }

    /// Specify association id for cash transfer
    pub fn correlation_id(mut self, correlation_id: CorrelationId) -> Self {
        self.correlation_id = Some(correlation_id);
        self
    }

    /// Include extra metadata with transaction
    pub fn extra(mut self, extra: Vec<u8>) -> Self {
        self.extra = Some(extra);
        self
    }

    /// Specify set of decoy identity tags
    pub fn masking_identity_sources(mut self, masking_identity_sources: Vec<IdentityTag>) -> Self {
        self.masking_identity_sources = Some(masking_identity_sources);
        self
    }

    /// Specify the trust public key
    pub fn trust_public_key(mut self, trust_public_key: PublicKey) -> Self {
        self.trust_public_key = Some(trust_public_key);
        self
    }

    pub fn banned_members(self, banned_members: Vec<PublicKey>) -> Self {
        Self {
            banned_members: Some(banned_members),
            ..self
        }
    }

    /// Consume builder to construct transaction
    pub fn build<R: rand::Rng + CryptoRng>(self, csprng: &mut R) -> CreateRequestResponse {
        // restrict to some integer 0 < x <= 16bits in total size
        let default_values: Vec<u64> = repeat_with(|| csprng.gen_range(1u16, u16::MAX))
            .take(5)
            .map(Into::into)
            .collect();

        let values = self.values.unwrap_or(default_values);
        let masking_identity_sources = self
            .masking_identity_sources
            .unwrap_or_else(|| default_masking_sources(csprng));
        let correlation_id = self.correlation_id.unwrap_or_else(|| csprng.gen());
        let private_key = self.private_key.unwrap_or_else(|| Scalar::random(csprng));
        let extra = self.extra.unwrap_or_default();

        let default_identity_source =
            IdentityTag::from_key_with_rand_base(private_key.into(), csprng);
        let identity_source = self.identity_source.unwrap_or(default_identity_source);

        let trust_pub_key: PublicKey = self.trust_public_key.unwrap_or_default();

        let banned_members = self.banned_members.unwrap_or_default();

        let inputs = CreateRequestTxnInputs {
            identity_source,
            values,
            masking_identity_sources,
            correlation_id,
            private_key,
            trust_pub_key,
            extra,
        };

        let create_request =
            unchecked_construct_create_request_transaction(&inputs, banned_members, csprng)
                .expect("Transaction builder produced invalid transaction unexpectedly");
        CreateRequestResponse {
            create_request,
            trust_pub_key,
        }
    }
}

#[cfg(any(test, feature = "test-helpers"))]
fn default_masking_sources<R: rand::Rng + CryptoRng>(csprng: &mut R) -> Vec<IdentityTag> {
    core::iter::repeat_with(|| IdentityTag::random(csprng))
        .take(4)
        .collect()
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use crate::{
        crypto::DefaultCryptoImpl,
        test::{arb_rng, banned_members, insert_index},
        test_helpers::random_private_key,
    };
    use proptest::prelude::*;
    use rand::rngs::OsRng;

    #[test]
    fn test_construct_create_request_transaction() {
        let mut csprng: OsRng = OsRng::default();
        let transaction_response = TestCreateRequestBuilder::default().build(&mut csprng);
        assert!(verify_create_request_transaction::<DefaultCryptoImpl>(
            &transaction_response.create_request,
            &transaction_response.trust_pub_key,
            Vec::new()
        )
        .is_ok());
    }

    #[test]
    fn construct_create_request_transaction__mismatch_between_identity_source_and_private_key_returns_error(
    ) {
        let mut csprng: OsRng = OsRng::default();

        let trust_pub_key: PublicKey = get_random_curve_point(&mut csprng).into();
        let inputs = CreateRequestTxnInputs {
            identity_source: IdentityTag::random(&mut csprng),
            values: vec![],
            masking_identity_sources: vec![],
            correlation_id: CorrelationId::default(),
            private_key: Scalar::random(&mut csprng),
            trust_pub_key,
            extra: vec![],
        };

        let banned_members = Vec::new();

        let result = construct_create_request_transaction(&inputs, banned_members, &mut csprng);
        // assert error
        assert!(matches!(
            result,
            Err(TransactionBuilderErrors::IdentitySourceDoesNotMatchPrivateKey)
        ));
    }

    #[test]
    fn construct_create_request_transaction__masking_identity_sources_is_greater_than_four_returns_error(
    ) {
        let mut csprng: OsRng = OsRng::default();
        let private_key = Scalar::random(&mut csprng);

        let trust_pub_key: PublicKey = get_random_curve_point(&mut csprng).into();
        let inputs = CreateRequestTxnInputs {
            identity_source: IdentityTag::from_key_with_generator_base(private_key.into()),
            values: vec![],
            masking_identity_sources: vec![
                IdentityTag::random(&mut csprng),
                IdentityTag::random(&mut csprng),
                IdentityTag::random(&mut csprng),
                IdentityTag::random(&mut csprng),
                IdentityTag::random(&mut csprng),
            ],
            correlation_id: CorrelationId::default(),
            private_key,
            trust_pub_key,
            extra: vec![],
        };

        let banned_members = Vec::new();

        let result = construct_create_request_transaction(&inputs, banned_members, &mut csprng);
        // assert error
        assert!(matches!(
            result,
            Err(TransactionBuilderErrors::MaxMaskingIdentitySourcesExceeded)
        ));
    }

    #[test]
    fn construct_create_request_transaction__zero_values_returns_error() {
        let mut csprng: OsRng = OsRng::default();
        let trust_pub_key: PublicKey = get_random_curve_point(&mut csprng).into();
        let private_key = Scalar::random(&mut csprng);
        let inputs = CreateRequestTxnInputs {
            identity_source: IdentityTag::from_key_with_generator_base(private_key.into()),
            values: vec![0],
            masking_identity_sources: vec![],
            correlation_id: CorrelationId::default(),
            private_key,
            trust_pub_key,
            extra: vec![],
        };

        let banned_members = Vec::new();

        let result = construct_create_request_transaction(&inputs, banned_members, &mut csprng);
        // assert error
        assert!(matches!(
            result,
            Err(TransactionBuilderErrors::ZeroValueInvalid)
        ));
    }

    #[test]
    fn construct_create_request_transaction__empty_values_returns_error() {
        let mut csprng: OsRng = OsRng::default();
        let private_key = Scalar::random(&mut csprng);

        let trust_pub_key: PublicKey = get_random_curve_point(&mut csprng).into();
        let inputs = CreateRequestTxnInputs {
            identity_source: IdentityTag::from_key_with_generator_base(private_key.into()),
            values: vec![],
            masking_identity_sources: vec![],
            correlation_id: CorrelationId::default(),
            private_key,
            trust_pub_key,
            extra: vec![],
        };

        let banned_members = Vec::new();

        let result = construct_create_request_transaction(&inputs, banned_members, &mut csprng);
        // assert error
        assert!(matches!(
            result,
            Err(TransactionBuilderErrors::ZeroValueInvalid)
        ));
    }

    #[test]
    fn create_request_transaction_generates_new_id_tag() {
        let mut rng = OsRng::default();
        let private_key = Scalar::random(&mut rng);
        let id_tag = IdentityTag::from_key_with_generator_base(private_key.into());
        let transaction = TestCreateRequestBuilder::default()
            .private_key(private_key)
            .identity_source(id_tag)
            .build(&mut rng)
            .create_request;
        assert_ne!(id_tag, transaction.core_transaction.identity_output);
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn can_submit_create_request_as_non_banned_member(
            banned_members in banned_members(0..10),
            mut rng in arb_rng()
        ) {
            let transaction_response = TestCreateRequestBuilder::default()
                .banned_members(banned_members.clone())
                .build(&mut rng);

            assert!(verify_create_request_transaction::<DefaultCryptoImpl>(
                &transaction_response.create_request,
                &transaction_response.trust_pub_key,
                banned_members,
            )
            .is_ok());
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn cannot_submit_create_request_as_banned_member(
            mut banned_members in banned_members(0..10),
            mut rng in arb_rng()
        ) {
            let own_private_key = random_private_key(&mut rng);
            let own_public_key = (own_private_key * get_G()).into();
            let insert_index = insert_index(&mut rng, banned_members.len());
            banned_members.insert(insert_index, own_public_key);

            let transaction_response = TestCreateRequestBuilder::default()
                .banned_members(banned_members.clone())
                .private_key(own_private_key)
                .build(&mut rng);
            assert!(matches!(
                verify_create_request_transaction::<DefaultCryptoImpl>(
                    &transaction_response.create_request,
                    &transaction_response.trust_pub_key,
                    banned_members
                ),
                Err(TransactionValidationError::ConstructedByBannedMember)
            ));
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn cannot_verify_create_request_with_wrong_banned_member(
            mut banned_members in banned_members(1..10),
            mut rng in arb_rng()
        ) {
            let transaction_response = TestCreateRequestBuilder::default()
                .banned_members(banned_members.clone())
                .build(&mut rng);

            // Replace one banned member
            let some_private_key = random_private_key(&mut rng);
            let some_public_key = (some_private_key * get_G()).into();
            let replace_index = insert_index(rng, banned_members.len());
            banned_members[replace_index] = some_public_key;

            assert!(matches!(verify_create_request_transaction::<DefaultCryptoImpl>(
                &transaction_response.create_request,
                &transaction_response.trust_pub_key,
                banned_members,
            ), Err(TransactionValidationError::InvalidAlpha))
            );
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn banned_member_list_cannot_be_different_length_than_Qs(
            mut banned_members in banned_members(0..10),
            mut rng in arb_rng()
        ) {
            let transaction_response = TestCreateRequestBuilder::default()
                .banned_members(banned_members.clone())
                .build(&mut rng);

            // Add one banned member
            let some_private_key = random_private_key(&mut rng);
            let some_public_key = (some_private_key * get_G()).into();
            let replace_index = insert_index(rng, banned_members.len());
            banned_members.push(some_public_key);

            assert!(matches!(verify_create_request_transaction::<DefaultCryptoImpl>(
                &transaction_response.create_request,
                &transaction_response.trust_pub_key,
                banned_members,
            ), Err(TransactionValidationError::BannedMemberListDifferentLengths))
            );
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn cannot_use_fake_randomness_values(
            banned_members in banned_members(0..10),
            mut rng in arb_rng()
        ) {
            let mut transaction_response = TestCreateRequestBuilder::default()
                .banned_members(banned_members.clone())
                .build(&mut rng);

            let fake_Z_ = Scalar::random(&mut rng) * Scalar::random(&mut rng) * get_G();

            transaction_response.create_request.core_transaction._Z_ = fake_Z_;

            assert!(matches!(
                verify_create_request_transaction::<DefaultCryptoImpl>(
                    &transaction_response.create_request,
                    &transaction_response.trust_pub_key,
                    banned_members
                ),
                Err(TransactionValidationError::InvalidSignature)
            ));
        }
    }
}
