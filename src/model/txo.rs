use crate::{esig_payload::ESigPayload, OneTimeKey};
use alloc::vec::Vec;
use curve25519_dalek::ristretto::RistrettoPoint;
use rand::{CryptoRng, RngCore};
use serde::{Deserialize, Serialize};

/// A transaction output, or TXO, represents a commitment to some value, and information about who
/// owns the right to spend that value (claims).
#[derive(Clone, Copy, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct TransactionOutput {
    /// The commitment is the representation of the value in the form of a Pedersen Commitment
    commitment: RistrettoPoint,
    /// The temporary public key of the owner of this TransactionOutput
    public_key: OneTimeKey,
    esig_payload: ESigPayload,
}

impl TransactionOutput {
    pub fn new(
        commitment: RistrettoPoint,
        public_key: OneTimeKey,
        esig_payload: ESigPayload,
    ) -> Self {
        Self {
            commitment,
            public_key,
            esig_payload,
        }
    }

    pub fn commitment(&self) -> &RistrettoPoint {
        &self.commitment
    }

    pub fn public_key(&self) -> &OneTimeKey {
        &self.public_key
    }

    pub fn esig_payload(&self) -> &ESigPayload {
        &self.esig_payload
    }

    /// Generate a random transaction output
    pub fn random<R>(csprng: &mut R) -> Self
    where
        R: RngCore + CryptoRng,
    {
        Self::new(
            RistrettoPoint::random(csprng),
            OneTimeKey::random(csprng),
            ESigPayload::new_with_ueta(),
        )
    }

    /// The SHA256 of the one-time key
    pub fn to_hash(self) -> [u8; 32] {
        self.public_key.0.to_hash()
    }

    #[cfg(test)]
    /// Creates an `TransactionOutput` with a valid paylod.
    /// Note: We currently only implement e-signatures for UETA networks.
    /// For custom payloads, use `with_custom_esig_payload()` instead.
    pub fn with_compliant_esig_payload(commitment: RistrettoPoint, public_key: OneTimeKey) -> Self {
        Self {
            commitment,
            public_key,
            esig_payload: ESigPayload::new_with_ueta(),
        }
    }

    #[cfg(test)]
    /// Creates an `TransactionOutput` with a custom payload.
    /// Note: Any payload that is not a valid UETA payload will not be accepted by the network
    /// as we currently only implement e-signatures for UETA networks.
    /// For valid payloads, use `with_compliant_esig_payload()` instead.
    pub fn with_custom_esig_payload(
        commitment: RistrettoPoint,
        public_key: OneTimeKey,
        esig_text: &'static str,
    ) -> Result<Self, crate::esig_payload::errors::ESigError> {
        Ok(Self {
            commitment,
            public_key,
            esig_payload: ESigPayload::new(esig_text)?,
        })
    }
}

/// A collection of TransactionOutput(s). The are used in a transaction as a collection of inputs
/// that are summed up to make the total value of the transaction. A transaction includes multiple
/// `TransactionInputSet`s, only one of which is the real `TransactionInputSet` being spent.
#[derive(Clone, Default, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct TransactionInputSet {
    /// The list of TXOs being used in this input set
    pub components: Vec<TransactionOutput>,
}

impl TransactionInputSet {
    /// Construct a new input set consisting of the provided TXOs
    pub fn new(components: Vec<TransactionOutput>) -> TransactionInputSet {
        TransactionInputSet { components }
    }
}
