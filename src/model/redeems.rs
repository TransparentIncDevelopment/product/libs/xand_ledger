use crate::{CorrelationId, IUtxo};
use alloc::boxed::Box;
use serde::{Deserialize, Serialize};

mod clear_redeem;
mod exiting_redeem;
mod redeem;

pub use clear_redeem::*;
pub use exiting_redeem::*;
pub use redeem::*;

/// Provides access to and storage of info in the ledger about pending unfulfilled redeem requests in the form
/// of [RedeemRequestRecord](struct.RedeemRequestRecord.html)s
pub trait RedeemRequestRepository {
    type Utxo: IUtxo;
    /// The type of iterator the implementor uses
    type Iter: Iterator<Item = (CorrelationId, RedeemRequestRecord<Self::Utxo>)>;

    /// Fetches the redeem request record for the correlation id if one exists
    fn get_unfulfilled_redeem_request(
        &self,
        correlation_id: CorrelationId,
    ) -> Option<RedeemRequestRecord<Self::Utxo>>;
    /// Stores a new unfulfilled redeem request record under the provided correlation id
    fn store_redeem_request(
        &self,
        correlation_id: CorrelationId,
        record: RedeemRequestRecord<Self::Utxo>,
    );
    /// Remove an unfulfilled redeem request record from storage - either because of fulfillment or cancellation.
    fn remove_redeem_request(&self, correlation_id: CorrelationId);
    /// Returns an iterator over all outstanding unfulfilled redeem requests
    fn all_unfulfilled_redeem_requests(&self) -> Self::Iter;
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Clone)]
pub enum RedeemRequestRecord<Utxo: IUtxo> {
    Confidential(Box<ConfidentialRedeemRequestRecord>),
    Clear(ClearRedeemRequestRecord<Utxo>),
}

impl<Utxo: IUtxo> RedeemRequestRecord<Utxo> {
    pub fn amount(&self) -> u64 {
        match self {
            RedeemRequestRecord::Confidential(r) => r.amount,
            RedeemRequestRecord::Clear(r) => r.amount.value(),
        }
    }
}
