/// Trait for managing the storage of validator data
pub trait ValidatorRepository {
    type PublicKey;

    /// Return true if public key is associated with a validator on the network
    fn is_validator(&self, public_key: &Self::PublicKey) -> bool;
}
