use crate::{IdentityTag, PublicKey};
use alloc::vec::Vec;

/// State of banned member
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum BannedState<BlockNumber> {
    /// Began exiting on block number
    Exiting(BlockNumber),
    /// Fully exited
    Exited,
}

/// Trait for managing the storage of member data
pub trait MemberRepository<BlockNumber> {
    /// Return true if id is associated with a member
    fn is_valid_member(&self, id: &IdentityTag) -> bool;
    /// Return list of all banned members
    fn get_banned_members(&self) -> Vec<(PublicKey, BannedState<BlockNumber>)>;
}
