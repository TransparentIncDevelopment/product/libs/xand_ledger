use crate::{
    ConfidentialRedeemRequestRecord, CorrelationId, KeyImage, OpenedTransactionOutput, PrivateKey,
    PublicKey, RedeemSigner, TransactionInputSet, TransactionResult,
};
use alloc::vec::Vec;
use curve25519_dalek::ristretto::RistrettoPoint;
use serde::{Deserialize, Serialize};
use zkplmt::models::Proof;

/// Encapsulates all the details of a [`ExitingRedeemRequestTransaction`] other than the proof with signature.
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[allow(non_snake_case)]
pub struct CoreExitingRedeemRequestTransaction {
    pub sender: PublicKey,

    /// A list of lists of transaction outputs where only one of the lists of outputs is the real
    // one being used.
    pub input: Vec<TransactionInputSet>,

    /// Change output and redeem commitment
    pub output: ExitingRedeemOutput,

    /// The correlation id matching this request and its fulfillment
    pub correlation_id: CorrelationId,

    /// key-images are nullifiers or the TransactionOutput(s). There is only a unique key-image for
    /// every TransactionOutput, but the key-image cannot be efficiently matched with the
    /// corresponding TransactionOutput by any polynomial time probabilistic algorithm with a
    /// non-negligible probability without using the private key. If a Transaction contains a
    /// key-image that has already been used, then the corresponding TransactionOutput has been
    /// spent already, which means that the transaction is invalid.
    pub key_images: Vec<KeyImage>,

    /// Z is used to store the randomness required to obfuscate which of the input sums equals the
    /// output sum. Without this randomness, all anonymity will be lost.
    pub Z: RistrettoPoint,

    /// A proof that Z is a Pedersen commitment to zero. It also implies that pZ is also a
    /// commitment to zero where p is any scalar.
    pub alpha: Proof,

    /// Private information
    pub extra: Vec<u8>,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct ExitingRedeemOutput {
    pub redeem_output: OpenedTransactionOutput,
}

/// An exiting redeem request transaction
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct ExitingRedeemRequestTransaction {
    /// The underlying redeem request transaction which is signed for (part of) the proof
    pub core_transaction: CoreExitingRedeemRequestTransaction,
    /// Works as both a signature and the proof that the signer is a verified participant.
    pub pi: Proof,
}

/// Inputs for a participant issuing an exiting redeem request
#[derive(Debug, Deserialize, Serialize)]
pub struct PrivateExitingRedeemRequestInputs {
    pub spends: Vec<OpenedTransactionOutput>,
    pub private_key: PrivateKey,
}

pub trait ExitingRedeemRequestHandler {
    fn exiting_redeem_request(
        &self,
        transaction: ExitingRedeemRequestTransaction,
    ) -> TransactionResult;
}

impl From<ExitingRedeemRequestTransaction> for ConfidentialRedeemRequestRecord {
    fn from(req: ExitingRedeemRequestTransaction) -> Self {
        ConfidentialRedeemRequestRecord {
            amount: req.core_transaction.output.redeem_output.value,
            redeem_output: req.core_transaction.output.redeem_output,
            account_data: req.core_transaction.extra,
            signer: RedeemSigner::PublicKey(req.core_transaction.sender),
        }
    }
}
