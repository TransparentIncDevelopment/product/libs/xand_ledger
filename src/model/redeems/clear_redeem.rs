use crate::{clear_txo::IUtxo, CorrelationId, EncryptedBlob, MonetaryValue, TransactionResult};
use alloc::vec::Vec;
use serde::{Deserialize, Serialize};

/// A record of a unfulfilled clear redeem request a user does not submit this directly, rather it
/// is created and stored as a result of processing a clear redeem request transaction.
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Clone)]
pub struct ClearRedeemRequestRecord<Utxo: IUtxo> {
    /// Clear Txo that is being redeemed. In the case of cancellation this will become valid again
    pub redeem_output: Utxo,
    /// Encrypted data for the trust concerning where to send cash funds
    pub account_data: EncryptedBlob,
    /// The total amount to be redeemed
    pub amount: MonetaryValue,
}

impl<Utxo: IUtxo> From<ClearRedeemRequestTransaction<Utxo>> for ClearRedeemRequestRecord<Utxo> {
    fn from(req: ClearRedeemRequestTransaction<Utxo>) -> Self {
        ClearRedeemRequestRecord {
            amount: req.output.redeem_output.value(),
            redeem_output: req.output.redeem_output,
            account_data: req.extra,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct ClearRedeemOutput<Utxo: IUtxo> {
    pub redeem_output: Utxo,
    pub change_output: Option<Utxo>,
}

/// A clear (non-confidential) redeem request transaction
#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct ClearRedeemRequestTransaction<Utxo: IUtxo> {
    /// Input clear utxos to be redeemed
    pub(crate) input: Vec<Utxo>,
    /// Change output and output intended for the redeem
    pub(crate) output: ClearRedeemOutput<Utxo>,
    /// The correlation id matching this request and its fulfillment or cancellation
    pub(crate) correlation_id: CorrelationId,
    /// Private information
    pub(crate) extra: Vec<u8>,
}

impl<Utxo: IUtxo> ClearRedeemRequestTransaction<Utxo> {
    pub fn input(&self) -> Vec<Utxo> {
        self.input.clone()
    }

    pub fn output(&self) -> ClearRedeemOutput<Utxo> {
        self.output.clone()
    }

    pub fn correlation_id(&self) -> CorrelationId {
        self.correlation_id
    }

    pub fn extra_data(&self) -> Vec<u8> {
        self.extra.clone()
    }
}

/// Implementors are capable of processing clear redeem request transactions
pub trait ClearRedeemRequestHandler<Utxo: IUtxo> {
    /// Process and validate an incoming clear redeem request
    fn clear_redeem_request(
        &self,
        transaction: ClearRedeemRequestTransaction<Utxo>,
    ) -> TransactionResult;
}
