use crate::{
    transactions::PublicInputSet, Encrypted, IdentityTag, KeyImage, Proof, TransactionOutput,
    TransactionResult, VerifiableEncryptionOfSignerKey,
};
use alloc::vec::Vec;
use curve25519_dalek::ristretto::RistrettoPoint;
use serde::{Deserialize, Serialize};
use zkplmt::bulletproofs::BulletRangeProof;

/// The CoreTransaction contains all the details of a Transaction except the signature and proof. A
/// serialized version of the CoreTransaction is then signed as part of the main proof. A
/// Transaction contains a CoreTransaction and a proof cum signature.
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[allow(non_snake_case)]
pub struct CoreSendClaimsTransaction {
    /// A list of lists of transaction outputs where only one of the lists of outputs is the real
    /// one being used.
    pub input: Vec<PublicInputSet>,

    /// The set of output UTxOs. All of them are real. The sum of all the TransactionOutput(s) is
    /// the value Of the transaction.
    pub output: Vec<SendClaimsOutput>,

    /// A new identity output key that can be used by the same member who issued this transaction
    /// for subsequent transactions.
    pub output_identity: IdentityTag,

    /// key-images are nullifiers or the TransactionOutput(s). There is only a unique key-image for
    /// every TransactionOutput, but the key-image cannot be efficiently matched with the
    /// corresponding TransactionOutput by any polynomial time probabilistic algorithm with a
    /// non-negligible probability without using the private key. If a Transaction contains a
    /// key-image that has already been used, then the corresponding TransactionOutput has been
    /// spent already, which means that the transaction is invalid.
    pub key_images: Vec<KeyImage>,

    /// `Z` is used to store the randomness required to obfuscate which of the input sums equals the
    /// output sum. Without this randomness, all anonymity will be lost.
    pub Z: RistrettoPoint,

    /// A proof that Z is a Pedersen commitment to zero. It also implies that pZ is also a
    /// commitment to zero where p is any scalar. Also proves that the all the banned member points
    /// `Qs` share randomness with `Z`.
    pub alpha: Proof,

    /// Proof that all the output TransactionOutput(s) contain commitments to values represented by
    /// a maximum number of bits. This is to stop the sum from rolling over to create money out of
    /// nothing.
    pub range_proof: BulletRangeProof,

    /// The encryption of the signer's permanent public key that can be decrypted by
    /// each receiver.
    pub encrypted_sender: VerifiableEncryptionOfSignerKey,

    /// Banned members with randomness. Order must be kept to rebuild the alpha proof.
    pub Qs: Vec<RistrettoPoint>,
}

/// Transfer TxO and associated data
#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize)]
pub struct SendClaimsOutput {
    pub txo: TransactionOutput,
    /// Currently contains commitment inputs encrypted by the issuer for the recipient
    pub encrypted_metadata: Encrypted,
}

/// A send claims transaction representing the transfer of value from one registered member to another
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct SendClaimsTransaction {
    /// Contains all the details of a Transaction other than the proof with signature.
    pub core_transaction: CoreSendClaimsTransaction,

    ///pi is the proof with signature
    pub pi: Proof,
}

/// Implementors are capable of processing send claims transactions
pub trait SendClaimsHandler {
    /// Process and validate an incoming send claims transaction
    fn send_claims(&self, transaction: SendClaimsTransaction) -> TransactionResult;
}
