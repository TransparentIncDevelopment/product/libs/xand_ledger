#![allow(clippy::upper_case_acronyms)]
use crate::{
    CompactEncryptedSigner, CorrelationId, CreateCancellationReason, Dispatcher, IdentityTag,
    Milliseconds, Proof, PublicKey, RistrettoPoint, Scalar, TransactionOutput, TransactionResult,
    VerifiableEncryptionOfSignerKey,
};
use alloc::vec::Vec;
use serde::{Deserialize, Serialize};

/// A record of a confidential pending create request - a user does not submit this directly, rather it
/// is created and stored as a result of processing a create request transaction.
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Clone)]
pub struct CreateRequestRecord {
    /// New outputs that will exist as a result of the create
    pub outputs: Vec<OpenedTransactionOutput>,
    /// The identity tag
    pub identity_output: IdentityTag,
    /// The account data for the transaction. Used by the member to
    /// validate activity and by the trust to transfer cash
    /// from the correct account.
    pub account_data: Vec<u8>,
    /// The time that this request will expire at (millis since epoch)
    pub expires_at: Milliseconds,
    /// Encrypted Sender Identity
    pub encrypted_sender: CompactEncryptedSigner,
}

impl CreateRequestRecord {
    pub(crate) fn from_request(req: CreateRequestTransaction, expires_at: Milliseconds) -> Self {
        CreateRequestRecord {
            outputs: req.core_transaction.outputs,
            identity_output: req.core_transaction.identity_output,
            account_data: req.core_transaction.extra,
            expires_at,
            encrypted_sender: req.core_transaction.encrypted_sender.into(),
        }
    }
}

/// Provides access to and storage of info in the ledger about pending create requests in the form
/// of [CreateRequestRecord](struct.CreateRequestRecord.html)s
#[cfg_attr(test, mockall::automock(type Iter=alloc::vec::IntoIter<(CorrelationId, CreateRequestRecord)>;))]
pub trait CreateRequestRepository {
    /// The type of iterator the implementor uses
    type Iter: Iterator<Item = (CorrelationId, CreateRequestRecord)>;

    /// Fetches the create request record for the correlation id if one exists
    fn get_create_request(&self, correlation_id: CorrelationId) -> Option<CreateRequestRecord>;
    /// Stores a new create request record under the provided correlation id
    fn store_create_request(&self, correlation_id: CorrelationId, record: CreateRequestRecord);
    /// Remove a create request from storage - either because of confirmation or cancellation.
    fn remove_create_request(&self, correlation_id: CorrelationId);
    /// Returns an iterator over all outstanding pending create requests
    fn all_pending_create_requests(&self) -> Self::Iter;
}

/// Provides access to the metadata about the trust.
/// Specifically, it gives access to the public key of the trust.
#[cfg_attr(test, mockall::automock())]
pub trait TrustMetadata {
    /// Returns the public key of the trust
    fn get_trust_key(&self) -> Option<PublicKey>;
}

/// Implementors are capable of processing create request transactions
pub trait CreateRequestHandler {
    /// Process and validate an incoming create request
    fn create_request(&self, create_request: CreateRequestTransaction) -> TransactionResult;
}

/// An `OpenedTransactionOutput` represents a UTXO and the information needed to reveal/open it.
#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize)]
pub struct OpenedTransactionOutput {
    /// The underlying TXO
    pub transaction_output: TransactionOutput,
    /// The randomness used to obscure this output
    pub blinding_factor: Scalar,
    /// The unblinded value of the TXO
    pub value: u64,
}

/// Encapsulates all the fields of the create request transaction except the signature.
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[allow(non_snake_case)]
pub struct CoreCreateRequestTransaction {
    /// This is used as part of the proof that the requester is a verifed member. Only one of
    /// the [`CurveVector`](zkplmt::models::CurveVector)(s) points to a real member.
    pub identity_sources: Vec<IdentityTag>,

    /// The set of output UTxOs requested. They are only available after the trust approves this
    /// transaction. All of them are real. The sum of all the TransactionOutput(s) is the value of
    /// the transaction.
    pub outputs: Vec<OpenedTransactionOutput>,

    /// The new identity tag produced when this transaction is processed, that can be used in
    /// subsequent transactions
    pub identity_output: IdentityTag,

    /// The correlation id for tracking this request and matching it to a cash transfer at a Xand-Enabled Bank
    pub correlation_id: CorrelationId,

    ///The encrypted permanent public key of the sender or signer
    pub encrypted_sender: VerifiableEncryptionOfSignerKey,

    /// `Z` is used to store the randomness required to obfuscate which of the input sums equals the
    /// output sum. Without this randomness, all anonymity will be lost.
    pub Z: RistrettoPoint,

    /// `Z` multiplied by private key of sender
    pub _Z_: RistrettoPoint,

    /// A proof that the Banned Member points `Qs` share randomness with `Z`.
    pub alpha: Proof,

    /// Banned members with randomness z. Order must be kept to rebuild the alpha proof.
    pub Qs: Vec<RistrettoPoint>,

    /// Private information (ex: bank account / routing numbers)
    pub extra: Vec<u8>,
}

/// A create request, as issued by a member
#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct CreateRequestTransaction {
    /// The inner transaction, which is encoded and signed to produce (part of) the attached proof
    pub core_transaction: CoreCreateRequestTransaction,

    /// Works as both a signature and the proof that the signer is a verified participant.
    pub pi: Proof,
}

/// Implementors are capable of processing cash confirmation transactions
pub trait CashConfirmationHandler {
    /// Process and validate a cash confirmation issued by the trust
    fn cash_confirmation(&self, transaction: CashConfirmationTransaction) -> TransactionResult;
}

/// Issued by the trust to confirm a pending create request
#[derive(Deserialize, Serialize, Clone, Debug, derive_more::Constructor, PartialEq, Eq)]
pub struct CashConfirmationTransaction {
    /// The ID of the create request to mark as fulfilled (and the same ID which was present in the
    /// cash transfer)
    pub correlation_id: CorrelationId,
}

/// Implementors are capable of processing create cancellation transactions
pub trait CreateCancellationHandler {
    /// Process and validate a create cancellation issued by the trust
    fn create_cancellation(&self, transaction: CreateCancellationTransaction) -> TransactionResult;

    /// Can be called periodically (ex: on each block) to cancel any pending create requests which have
    /// passed their expiry
    fn cancel_expired_create_requests(&self, dispatcher: &dyn Dispatcher) -> TransactionResult;
}

/// Issued by the trust (or intrinsically) to cancel an outstanding create request
#[derive(Deserialize, Serialize, Clone, Debug, derive_more::Constructor, PartialEq, Eq)]
pub struct CreateCancellationTransaction {
    /// The identifier of the request
    pub correlation_id: CorrelationId,
    /// The reason the request is being cancelled
    pub reason: CreateCancellationReason,
}
