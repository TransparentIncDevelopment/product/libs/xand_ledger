#[cfg(test)]
mod tests;

use crate::{esig_payload::errors::ESigError, OpenedTransactionOutput, TransactionOutput};

use alloc::{
    fmt::{Display, Formatter},
    str::FromStr,
    string::{String, ToString},
};
use core::convert::TryInto;
use serde::{
    de::{Error as SerdeError, Error, Visitor},
    Deserialize, Deserializer, Serialize, Serializer,
};

/// This is the Result type used to support no_std.
pub type Result<T, E = ESigError> = core::result::Result<T, E>;

// Wording derived from UETA payload [PRD](https://transparentfinancialsystems.sharepoint.com/:w:/s/TransparentSystems/EaCBXdLJvfhIlOAsVG8CbdQBYrZVhlbvqjGIc0wKKsdV3g?e=CPcX8u).
pub const UETA_TEXT: &str =
    include_str!("../legal_text/official_text/esig_payload/ueta/ueta_text.txt");

/// A payload containing legal text required on transferable records.
/// ESigPayload` is `non_exhaustive` because it will be expanded as Xand moves to
/// new markets (e.g. European e-signature legislation (eIDAS) might require some specific message).
/// Implementers must decide how to handle `ESigPayload`s they don't understand.
#[non_exhaustive]
#[derive(Clone, Copy, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub enum ESigPayload {
    Ueta(ESigMessage<UETA_SIZE>),
}

/// Exact character length of a valid UETA payload as defined in `UETA_TEXT`.
pub const UETA_SIZE: usize = UETA_TEXT.len();

impl ESigPayload {
    /// Fallible constructor for payloads that are manually built, e.g. by an outside caller.
    pub(crate) fn new(val: &str) -> Result<Self> {
        ESigMessage::new(val)
            .map(Self::Ueta)
            .map_err(|_| ESigError::InvalidStringContent)
    }

    /// Creates a valid ESigPayload::Ueta. Infallible constructor for internal use.
    /// The `UETA_TEXT` is hardcoded and the `ESigMessage` is expecting its exact text, so it
    /// should never panic.
    pub fn new_with_ueta() -> Self {
        Self::new(UETA_TEXT).unwrap() // Should never panic
    }

    pub fn ueta_text(&self) -> Option<String> {
        match self {
            ESigPayload::Ueta(msg) => Some(msg.to_string()),
        }
    }
}

impl FromStr for ESigPayload {
    type Err = ESigError;

    fn from_str(s: &str) -> Result<Self> {
        if s == UETA_TEXT {
            Self::new(UETA_TEXT) // We pass in our const value to avoid lifetimes
        } else {
            Err(ESigError::InvalidStringContent) // We only support valid UETA payloads
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct ESigMessage<const LENGTH: usize>([u8; LENGTH]);

impl<const LENGTH: usize> ESigMessage<{ LENGTH }> {
    pub fn new(val: &str) -> Result<Self> {
        val.parse()
    }
}

impl<const LENGTH: usize> FromStr for ESigMessage<{ LENGTH }> {
    type Err = ESigError;

    fn from_str(s: &str) -> Result<Self> {
        s.as_bytes()
            .try_into()
            .map_err(|_| ESigError::CouldNotParseFromStr)
            .map(ESigMessage)
    }
}

impl<const LENGTH: usize> Display for ESigMessage<{ LENGTH }> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::result::Result<(), core::fmt::Error> {
        let str = core::str::from_utf8(&self.0).unwrap_or("");
        write!(f, "{}", str)
    }
}

impl<const LENGTH: usize> Serialize for ESigMessage<{ LENGTH }> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&format!("{}", self))
    }
}

impl<'de, const LENGTH: usize> Deserialize<'de> for ESigMessage<{ LENGTH }> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct ESigMessageVisitor<const LENGTH: usize> {}

        impl<'de, const LENGTH: usize> Visitor<'de> for ESigMessageVisitor<{ LENGTH }> {
            type Value = ESigMessage<LENGTH>;

            fn expecting(
                &self,
                formatter: &mut Formatter,
            ) -> core::result::Result<(), core::fmt::Error> {
                formatter.write_str("an ESigMessage")
            }

            fn visit_str<E>(self, val: &str) -> Result<Self::Value, E>
            where
                E: Error,
            {
                match val {
                    UETA_TEXT => ESigMessage::from_str(val)
                        .map_err(|e| SerdeError::custom(format!("{:?}", e))),
                    _ => Err(SerdeError::custom(ESigError::InvalidStringContent)),
                }
            }
        }

        let visitor = ESigMessageVisitor::<LENGTH> {};
        deserializer.deserialize_str(visitor)
    }
}
pub fn transaction_output_contains_valid_ueta(output: &TransactionOutput) -> bool {
    matches!(output.esig_payload(), ESigPayload::Ueta(val) if val.to_string() == UETA_TEXT)
}

pub fn opened_transaction_output_contains_valid_ueta(output: &OpenedTransactionOutput) -> bool {
    transaction_output_contains_valid_ueta(&output.transaction_output)
}

pub mod errors {
    use alloc::string::String;
    use serde::{Deserialize, Serialize};

    /// Failures that may occur during E-Signature validation
    #[cfg_attr(feature = "std", derive(thiserror::Error))]
    #[cfg_attr(not(feature = "std"), derive(derive_more::Display))]
    #[derive(Clone, Debug, derive_more::From, Eq, PartialEq, Deserialize, Serialize)]
    pub enum ESigError {
        #[cfg_attr(feature = "std", error("Unable to parse from value: {:?}", .0))]
        CouldNotParseFromInput(String),
        #[cfg_attr(feature = "std", error("Unable to parse from String slice"))]
        CouldNotParseFromStr,
        #[cfg_attr(feature = "std", error("Input string content invalid"))]
        InvalidStringContent,
    }
}
