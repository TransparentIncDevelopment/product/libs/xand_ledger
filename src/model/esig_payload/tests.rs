#![allow(non_snake_case)]
use super::*;
use serde_test::{assert_de_tokens, assert_de_tokens_error, assert_ser_tokens, Token};

const INVALID_NONEMPTY_UETA_STR_VALID_LENGTH: &str =
    include_str!("../../legal_text/official_text/esig_payload/ueta/test_files/invalid_nonempty_ueta_valid_length.txt");
const INVALID_NONEMPTY_UETA_STR_INVALID_LENGTH: &str =
    include_str!("../../legal_text/official_text/esig_payload/ueta/test_files/invalid_nonempty_ueta_invalid_length.txt");

#[test]
fn new__ueta_payload_happy_path() {
    //Given
    let valid_ueta_input = UETA_TEXT;
    // When
    let res = ESigPayload::new(valid_ueta_input);
    // Then
    assert!(matches!(res.clone().unwrap(), ESigPayload::Ueta(_)));
    assert_eq!(Some(valid_ueta_input.to_string()), res.unwrap().ueta_text());
}

#[test]
fn new_with_ueta__ueta_payload_happy_path() {
    //Given
    // When
    let res = ESigPayload::new_with_ueta();
    // Then
    assert!(matches!(res, ESigPayload::Ueta(_)));
    assert_eq!(Some(UETA_TEXT.to_string()), res.ueta_text());
}

#[test]
fn from_str__esig_ueta_happy_path() {
    // Given
    let expected_valid_ueta_payload = UETA_TEXT.to_string();
    // When
    let res = UETA_TEXT.parse::<ESigPayload>().unwrap();
    // Then
    assert!(matches!(res, ESigPayload::Ueta(_)));
    assert_eq!(Some(expected_valid_ueta_payload), res.ueta_text());
}

#[test]
fn from_str__esig_payload_invalid_nonempty_string_valid_length_fails() {
    // Given
    // When
    let res = INVALID_NONEMPTY_UETA_STR_VALID_LENGTH.parse::<ESigPayload>();
    // Then
    assert!(matches!(res.unwrap_err(), ESigError::InvalidStringContent));
}

#[test]
fn from_str__esig_payload_invalid_nonempty_string_invalid_length_fails() {
    // Given
    // When
    let res = INVALID_NONEMPTY_UETA_STR_INVALID_LENGTH.parse::<ESigPayload>();
    // Then
    assert!(matches!(res.unwrap_err(), ESigError::InvalidStringContent));
}

#[test]
fn new__esig_message_ueta_happy_path() {
    //Given
    let valid_input = UETA_TEXT;
    // When
    let res = ESigMessage::<UETA_SIZE>::new(valid_input);
    // Then
    assert_eq!(valid_input.to_string(), res.unwrap().to_string());
}

#[test]
fn new__esig_payload_invalid_nonempty_string_valid_length_fails() {
    // Given
    // When
    let res = ESigPayload::new(INVALID_NONEMPTY_UETA_STR_VALID_LENGTH);
    // Then
    assert!(matches!(res.unwrap_err(), ESigError::InvalidStringContent));
}

#[test]
fn new__ueta_esig_message_invalid_nonempty_string_valid_length_fails() {
    // Given
    // When
    let res = ESigMessage::<UETA_SIZE>::new(INVALID_NONEMPTY_UETA_STR_VALID_LENGTH);
    // Then
    assert!(matches!(res.unwrap_err(), ESigError::CouldNotParseFromStr));
}

#[test]
fn new__ueta_esig_message_invalid_nonempty_string_invalid_length_fails() {
    // Given
    // When
    let res = ESigMessage::<UETA_SIZE>::new(INVALID_NONEMPTY_UETA_STR_INVALID_LENGTH);
    // Then
    assert!(matches!(res.unwrap_err(), ESigError::CouldNotParseFromStr));
}

#[test]
fn new__ueta_esig_message_empty_inner_value_fails() {
    // Given
    // When
    let res = ESigMessage::<UETA_SIZE>::new("");
    // Then
    assert!(matches!(res.unwrap_err(), ESigError::CouldNotParseFromStr));
}

#[test]
fn new_with_ueta__always_works() {
    // Given
    // When
    let ueta_payload = ESigPayload::new_with_ueta();
    // Then
    let maybe_text = ueta_payload.ueta_text();
    assert!(maybe_text.is_some());
}

#[test]
fn esig_message_ueta_round_trip_string_representation_works() {
    //Given
    let valid_input = UETA_TEXT;
    let msg = ESigMessage::from_str(UETA_TEXT).unwrap();
    // When
    let owned_string = format!("{}", msg);
    let esig_type_from_str: ESigMessage<UETA_SIZE> = ESigMessage::from_str(&owned_string).unwrap();
    assert_eq!(msg, esig_type_from_str);
}

#[test]
fn esig_message_ueta_deserializer_works() {
    // Given
    // When
    let msg: ESigMessage<UETA_SIZE> = ESigMessage::from_str(UETA_TEXT).unwrap();
    // Then
    assert_de_tokens(&msg, &[Token::String(UETA_TEXT)]);
}

#[test]
fn esig_message_invalid_ueta_content_deserialize_fails() {
    // Given
    // When
    let bad_serialized_object = &[Token::Str(INVALID_NONEMPTY_UETA_STR_VALID_LENGTH)];
    // Then
    assert_de_tokens_error::<ESigMessage<UETA_SIZE>>(
        bad_serialized_object,
        &format!("{}", ESigError::InvalidStringContent),
    );
}

#[test]
fn esig_message_serializer_works() {
    // Given
    // When
    let msg: ESigMessage<UETA_SIZE> = ESigMessage::from_str(UETA_TEXT).unwrap();
    // Then
    assert_ser_tokens(&msg, &[Token::Str(UETA_TEXT)])
}
