use crate::{PublicKey, ValidatorRepository};
use std::collections::HashSet;

pub(crate) struct FakeValidatorRepository {
    validators: HashSet<PublicKey>,
}

impl FakeValidatorRepository {
    pub fn new() -> Self {
        Self {
            validators: HashSet::new(),
        }
    }

    pub fn with_validators(self, validators: &[PublicKey]) -> Self {
        let mut val_set = HashSet::new();
        val_set.extend(validators.iter());
        Self {
            validators: val_set,
        }
    }
}

impl ValidatorRepository for FakeValidatorRepository {
    type PublicKey = PublicKey;

    fn is_validator(&self, public_key: &Self::PublicKey) -> bool {
        self.validators.contains(public_key)
    }
}
