use super::super::model::{PublicKey, TrustMetadata};
use crate::{
    redeems::RedeemRequestRecord, BannedState, ClearTransactionOutput, CorrelationId,
    CorrelationIdRepository, CorrelationIdUsedBy, CreateRequestRecord, CreateRequestRepository,
    IdentityTag, IdentityTagRepository, KeyImage, KeyImageRepository, MemberRepository,
    RedeemRequestRepository, TotalIssuanceRepository, TransactionDataRepository, TransactionOutput,
    TxoStatus,
};
use alloc::vec::IntoIter;
use core::{
    cell::{Cell, RefCell},
    hash::{Hash, Hasher},
};
use std::collections::{HashMap, HashSet};
use zkplmt::core::get_random_curve_point;
pub use zkplmt_test_helpers::FakeCsprng;

pub mod fake_clear_transaction_data_repository;
pub mod fake_validator_repository;

#[derive(Default)]
pub struct FakeCreateRequestRepository {
    create_request_records: RefCell<HashMap<CorrelationId, CreateRequestRecord>>,
}

impl FakeCreateRequestRepository {
    pub fn new() -> Self {
        FakeCreateRequestRepository::default()
    }
}

impl CreateRequestRepository for FakeCreateRequestRepository {
    type Iter = IntoIter<(CorrelationId, CreateRequestRecord)>;

    fn get_create_request(&self, correlation_id: CorrelationId) -> Option<CreateRequestRecord> {
        self.create_request_records
            .borrow()
            .get(&correlation_id)
            .cloned()
    }

    fn store_create_request(&self, correlation_id: CorrelationId, record: CreateRequestRecord) {
        self.create_request_records
            .borrow_mut()
            .insert(correlation_id, record);
    }

    fn remove_create_request(&self, correlation_id: CorrelationId) {
        self.create_request_records
            .borrow_mut()
            .remove(&correlation_id);
    }

    fn all_pending_create_requests(&self) -> Self::Iter {
        self.create_request_records
            .borrow()
            .iter()
            .map(|(k, v)| (*k, v.clone()))
            .collect::<Vec<_>>()
            .into_iter()
    }
}

#[derive(Default)]
pub struct FakeCorrelationIdRepository {
    correlation_ids: RefCell<HashMap<CorrelationId, CorrelationIdUsedBy>>,
}

impl FakeCorrelationIdRepository {
    pub fn new() -> Self {
        FakeCorrelationIdRepository::default()
    }

    pub fn all_used_ids(&self) -> Vec<CorrelationId> {
        self.correlation_ids.borrow().keys().cloned().collect()
    }
}

impl CorrelationIdRepository for FakeCorrelationIdRepository {
    fn is_used(&self, id: CorrelationId) -> Option<CorrelationIdUsedBy> {
        self.correlation_ids.borrow().get(&id).cloned()
    }

    fn store_id(&self, id: CorrelationId) {
        self.correlation_ids
            .borrow_mut()
            .insert(id, CorrelationIdUsedBy::UsedCorrelationId);
    }
}

#[derive(Default)]
pub struct FakeIdentityTagRepo {
    identity_tags: RefCell<HashSet<IdentityTag>>,
}

impl FakeIdentityTagRepo {
    pub fn new() -> Self {
        FakeIdentityTagRepo::default()
    }

    pub fn seeded(seed: &[IdentityTag]) -> Self {
        FakeIdentityTagRepo {
            identity_tags: RefCell::new(seed.iter().cloned().collect()),
        }
    }

    pub fn all_recorded_tags(&self) -> HashSet<IdentityTag> {
        self.identity_tags.borrow().clone()
    }
}

impl IdentityTagRepository for FakeIdentityTagRepo {
    fn exists(&self, id: &IdentityTag) -> bool {
        self.identity_tags.borrow().contains(id)
    }

    fn store_id_tag(&self, id: IdentityTag) {
        self.identity_tags.borrow_mut().insert(id);
    }
}

pub struct FakeTrustMetadata {
    trust_key: PublicKey,
}

impl FakeTrustMetadata {
    pub fn new() -> FakeTrustMetadata {
        FakeTrustMetadata {
            trust_key: get_random_curve_point(&mut FakeCsprng::from_seed([0u8; 32])).into(),
        }
    }
    pub fn from_trust_key(trust_key: PublicKey) -> Self {
        FakeTrustMetadata { trust_key }
    }
}

impl TrustMetadata for FakeTrustMetadata {
    fn get_trust_key(&self) -> Option<PublicKey> {
        Some(self.trust_key)
    }
}

#[derive(Default)]
pub struct FakeTransactionDataRepository {
    pub transaction_outputs: RefCell<HashMap<TransactionOutput, (TxoStatus, Option<u64>)>>,
}

impl FakeTransactionDataRepository {
    pub fn new() -> Self {
        FakeTransactionDataRepository::default()
    }

    pub fn seeded(seed: &[TransactionOutput], status: TxoStatus, block: u64) -> Self {
        let records = seed
            .iter()
            .cloned()
            .map(|txo| (txo, (status, Some(block))))
            .collect();
        FakeTransactionDataRepository {
            transaction_outputs: RefCell::new(records),
        }
    }

    pub fn current_txos(&self) -> HashMap<TransactionOutput, (TxoStatus, Option<u64>)> {
        self.transaction_outputs.borrow().clone()
    }
}

impl TransactionDataRepository<u64> for FakeTransactionDataRepository {
    fn lookup_status(&self, output: &TransactionOutput) -> (TxoStatus, Option<u64>) {
        self.transaction_outputs
            .borrow()
            .get(output)
            .cloned()
            .unwrap_or((TxoStatus::Nonexistent, None))
    }

    fn set_output_status(&self, output: TransactionOutput, status: TxoStatus) {
        self.transaction_outputs
            .borrow_mut()
            .insert(output, (status, Some(0)));
    }
}

#[derive(Default)]
pub(crate) struct FakeKeyImageRepo {
    key_images: RefCell<HashSet<KeyImage>>,
}

impl FakeKeyImageRepo {
    pub fn new() -> Self {
        FakeKeyImageRepo::default()
    }

    pub fn seeded(key_images: &[KeyImage]) -> Self {
        let records = key_images.iter().cloned().collect();

        FakeKeyImageRepo {
            key_images: RefCell::new(records),
        }
    }

    pub fn current_key_images(&self) -> HashSet<KeyImage> {
        self.key_images.borrow().clone()
    }
}

impl KeyImageRepository for FakeKeyImageRepo {
    fn is_used(&self, image: &KeyImage) -> bool {
        self.key_images.borrow().contains(image)
    }

    fn insert_key_image(&self, image: KeyImage) {
        self.key_images.borrow_mut().insert(image);
    }
}

pub(crate) struct FakeMemberRepository {
    valid_members: Vec<IdentityTag>,
    banned_members: Vec<(PublicKey, BannedState<u64>)>,
}

impl FakeMemberRepository {
    pub fn new() -> Self {
        let valid_members = Vec::new();
        let banned_members = Vec::new();
        Self {
            valid_members,
            banned_members,
        }
    }

    pub fn with_members(self, valid_members: Vec<IdentityTag>) -> Self {
        Self {
            valid_members,
            ..self
        }
    }

    pub fn with_banned(self, banned_members: Vec<(PublicKey, BannedState<u64>)>) -> Self {
        Self {
            banned_members,
            ..self
        }
    }
}

impl MemberRepository<u64> for FakeMemberRepository {
    fn is_valid_member(&self, id: &IdentityTag) -> bool {
        self.valid_members.contains(id)
    }

    fn get_banned_members(&self) -> Vec<(PublicKey, BannedState<u64>)> {
        self.banned_members.clone()
    }
}
#[derive(Default)]
pub struct FakeTotalIssuanceRepository {
    cash_confirmation_total: Cell<u64>,
    redeem_total: Cell<u64>,
}

impl FakeTotalIssuanceRepository {
    pub fn new() -> Self {
        FakeTotalIssuanceRepository::default()
    }
}

impl TotalIssuanceRepository for FakeTotalIssuanceRepository {
    fn add_to_total_cash_confirmations(&self, amount: u64) -> u64 {
        self.cash_confirmation_total
            .set(self.cash_confirmation_total.get() + amount);
        self.cash_confirmation_total.get()
    }

    fn get_total_cash_confirmations(&self) -> u64 {
        self.cash_confirmation_total.get()
    }

    fn add_to_total_redeemed(&self, amount: u64) -> u64 {
        self.redeem_total
            .set(self.redeem_total.get().saturating_add(amount));
        self.redeem_total.get()
    }

    fn subtract_from_total_redeemed(&self, amount: u64) -> u64 {
        self.redeem_total
            .set(self.redeem_total.get().saturating_sub(amount));
        self.redeem_total.get()
    }

    fn get_total_redeemed(&self) -> u64 {
        self.redeem_total.get()
    }
}

#[allow(clippy::derive_hash_xor_eq)]
impl Hash for TransactionOutput {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.public_key().x().compress().as_bytes().hash(state);
        self.public_key().y().compress().as_bytes().hash(state);
        self.commitment().compress().as_bytes().hash(state);
    }
}

#[derive(Default)]
pub struct FakeRedeemRequestRepository {
    redeem_request_records:
        RefCell<HashMap<CorrelationId, RedeemRequestRecord<ClearTransactionOutput>>>,
}

impl FakeRedeemRequestRepository {
    pub fn new() -> Self {
        FakeRedeemRequestRepository::default()
    }
}

impl RedeemRequestRepository for FakeRedeemRequestRepository {
    type Utxo = ClearTransactionOutput;
    type Iter = IntoIter<(CorrelationId, RedeemRequestRecord<ClearTransactionOutput>)>;

    fn get_unfulfilled_redeem_request(
        &self,
        correlation_id: CorrelationId,
    ) -> Option<RedeemRequestRecord<ClearTransactionOutput>> {
        self.redeem_request_records
            .borrow()
            .get(&correlation_id)
            .cloned()
    }

    fn store_redeem_request(
        &self,
        correlation_id: CorrelationId,
        record: RedeemRequestRecord<ClearTransactionOutput>,
    ) {
        self.redeem_request_records
            .borrow_mut()
            .insert(correlation_id, record);
    }

    fn remove_redeem_request(&self, correlation_id: CorrelationId) {
        self.redeem_request_records
            .borrow_mut()
            .remove(&correlation_id);
    }

    fn all_unfulfilled_redeem_requests(&self) -> Self::Iter {
        self.redeem_request_records
            .borrow()
            .iter()
            .map(|(k, v)| (*k, v.clone()))
            .collect::<Vec<_>>()
            .into_iter()
    }
}
